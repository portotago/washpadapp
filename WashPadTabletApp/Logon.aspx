﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Logon.aspx.cs" Inherits="WashPadTabletApp.Logon" %>

<script runat="server">
  void Page_Load(object sender, EventArgs e)
    {
        // set the environment label
        EnvironmentLabel.InnerHtml = ConfigurationManager.AppSettings["EnvironmentLabel"].ToString();
    }
</script>

<html>
<head>
    <title>WashPad Tablet App</title>
    <!-- fix for jQuery 2.x on IE11 -->
    <meta http-equiv="X-UA-Compatible" content="IE=10;" />

    <!-- javascript libraries -->
    <script src="js/jquery.v2.1.4.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui.v1.11.4.min.js" type="text/javascript"></script>
    <script src="js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="js/underscore-min.js" type="text/javascript"></script>

    <!-- style -->
    <link rel="stylesheet" type="text/css" href="css/POL-TabletStyle.css?version=<% Response.Write(typeof(Washpad).Assembly.GetName().Version.ToString());%>" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />

    <!-- mobile icons -->
    <link rel="icon" sizes="128x128" href="images/launch-icon-128.png" />
    <link rel="apple-touch-startup-image" href="images/launch-icon-128.png" />
    <link rel="apple-touch-icon" sizes="128x128" href="images/launch-icon-128.png">

    <!-- mobile web app flags -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-title" content="Port Chalmers Washpad">

</head>

<body>
  <form id="form1" runat="server">
      <div id="main">

    <div id="breadcrumb"><span>Login</span></div>

        <div id="loginform">
            <div class="messagerow" >
                  <asp:Label ID="Msg" ForeColor="red" runat="server" />
            </div>
            <div class="formrow" >
                <div class="formlabel"><span>username</span></div>
                <div class="formfield"><asp:TextBox ID="username" runat="server" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="username" Display="Dynamic"  ErrorMessage="Cannot be empty." runat="server" class="hidden" /></div>
            </div>
            <div class="formrow" >
                <div class="formlabel"><span>password</span></div>
                <div class="formfield"><asp:TextBox ID="password" TextMode="Password" runat="server" /><asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="password" ErrorMessage="Cannot be empty." runat="server" class="hidden" /></div>
            </div>
            <div class="formrow" >
                <div class="formlabel"><span>remember me</span></div>
                <div class="formfield"><asp:CheckBox ID="Persist" runat="server" /></div>
            </div>
            <div style="background:none !important;height:4.2em;">
                <div class="formfield" style="width:100% !important; text-align:center;"><asp:Button ID="Submit1" OnClick="Logon_Click" Text="Login >" runat="server" class=""/></div>
            </div>
        </div><!-- end #repairentry -->

    </div><!-- end #main -->

    <!-- -------------------------------------- -->
    <div id="footer">
        <div id="environmentinfo">
            <div>Washpad App<br /><span id="EnvironmentLabel" runat="server"></span> <span id="AppVersionLabel">v<% Response.Write(typeof(Washpad).Assembly.GetName().Version.ToString()); %></span></div>
            <img src="images/logo.png" alt="POL Logo" />
        </div><!-- end #environmentinfo -->
    </div><!-- end #footer -->
    <div id="StyleStub" runat="server"><% Response.Write("<style>#main{background: #" + ConfigurationManager.AppSettings["BackgroundColorHex"].ToString() + " !important;}</style>");%></div>

  </form>
</body>
</html>
