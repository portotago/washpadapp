﻿// ---------- DEPENDENCIES---------------

// Note: This solution requires log4net
// 1. install the NuGet package manager (Tools>>Extension Manager>>Online Gallery>>NuGet Package Manager
// 2. right-click the project and choose Manage NuGet Packages
// 3. search for log4net and install log4net
// 4. configuration of log4Net within web.config and global.asax.cs

// Note:  The Web.Config file contains connection string which should be manually encrypted post-deploy


using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WashPadTabletApp
{
    public partial class Logon : System.Web.UI.Page
    {

        protected void Logon_Click(object sender, EventArgs e)
        {
            var log = log4net.LogManager.GetLogger(this.GetType());
            string ipAddress = Request.ServerVariables["REMOTE_ADDR"];
            log.Info("Login attempt recieved from " + ipAddress);

            Msg.Text = "_";

            // Encrypt the password string --------------------------------------
            string encryptedPassword = "";
            int i = 1;
            foreach (char c in password.Text)
            {
                int unicode = c;
                char character = (char)(Convert.ToInt32(unicode) + 120 + i);
                encryptedPassword = encryptedPassword + character.ToString();
                i++;
            }

            // Compare user's encrypted password with the encrypted password stored in DepotPRO via web service
            DepotProHHWebService.DepotProHHeldSvcSoapClient DPSoapClient = new DepotProHHWebService.DepotProHHeldSvcSoapClient();
            DataSet dsDepotPROUsers = DPSoapClient.GetUsersDataSet();
            //TODO: handle outage of the web service...


            bool logonSuccess = false; //  guilty intil proven innocent
            foreach (DataTable table in dsDepotPROUsers.Tables)
            {
                foreach (DataRow row in table.Rows)
                {
                    // Loop through all users to find user, case insensitive
                    if (row.Field<string>("USERNAME").Substring(0, username.Text.Length).ToUpper() == username.Text.ToUpper())
                    {
                        // User located, check password, case sensitive
                        log.Info("Username appears to be valid.");
                        if (row.Field<string>("PASSWORD").Substring(0, encryptedPassword.Length) == encryptedPassword)
                        {
                            // Password matches, consider this a successful logon
                            log.Info("Correct passord.");
                            logonSuccess = true;
                        }
                        else { 
                            // Password does not match
                            log.Info("Bad password.");
                            // TODO: manage logon attempt throttling
                        }
                    }
                }
            } // end foreach table

      
        if (logonSuccess)
        {
            log.Info("Login attempt successful.");
            FormsAuthentication.RedirectFromLoginPage(username.Text.ToUpper(), Persist.Checked);
            }
        else
        {
            log.Info("Login attempt failed with bad password.");
            Msg.Text = "Invalid logon. Please try again.";
            }
        } // End logon_Click


        protected void Page_Load(object sender, EventArgs e)
        {
            var log = log4net.LogManager.GetLogger(this.GetType());
            log.Info("Login screen loaded.");


        } // End Page_Load
    }
}