﻿// function to retrieve querystring parameters
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


// --------------------------------------------------------------------------------------------
// --------------------------------------------- CONFIG ---------------------------------------
// --------------------------------------------------------------------------------------------

var containersListURL = "data/getcontainers.aspx";
var ContainerRefreshURL = "data/refreshcontainersfromjade.aspx";
var heatbeatFileURL = "heartbeat/alive.txt";
var heartBeatFreq = 10  // seconds delay in checking connectivity
var statusFlashDurationMS = 1111;
var selected_washpad = "";


// --------------------------------------------------------------------------------------------
// ------------------------------ PRELIMINARY CACHING OF CONTAINER NOS ------------------------
// --------------------------------------------------------------------------------------------

// ------------------ cache containers list --------------------------
var ContainersCache = "";
//var ContainersCache = refreshContainerCache();


// ------------------- initiate heartbeat ----------------------------
setInterval(function () {
    $.ajax({
        url: heatbeatFileURL + '?random=' + Math.random().toString(36).substring(7),
        type: 'HEAD',
        error: function () {
            //file can't be retrieved, assume connection problem
            $.blockUI({
                message: '<h1 style="margin:.5em">Error with connection to POL, check 3G connection and/or VPN.</h1><p style="font-size:16pt;">Retrying in ' + heartBeatFreq + ' seconds...</p>',
                title: "In-house moves app",
                css: {
                    padding: 0,
                    margin: 0,
                    width: '70%',
                    top: '20%',
                    left: '15%',
                    textAlign: 'center',
                    color: '#f00',
                    border: '3px solid #aaa',
                    backgroundColor: '#fff',
                    cursor: 'wait'
                }
            });
        },
        success: function () {
            //file exists
            $.unblockUI();
        }
    });
}, heartBeatFreq * 1000);



// --------------------------------------------------------------------------------------------
// ---------------------------------------- FUNCTIONS -----------------------------------------
// --------------------------------------------------------------------------------------------
function StatusFlash(message) {
    $("#StatusContainer").show();
    $("#StatusBar").html(message);
    $("#StatusBar").hide().fadeIn(500).delay(statusFlashDurationMS).fadeOut(1000).hide(function () {
        $("#StatusContainer").hide();
    });
}  // end StatusFlash


function refreshContainerCache() {
    //console.log("Refreshing cache of containers...");
    var json = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': containersListURL + "?random=" + Math.random().toString(36).substring(7),
        'dataType': "json",
        'success': function (data) {
            console.log("Successfully refreshed containers cache");
            json = data;
        }
    });
    return json;
}


// --------------------------------------------------------------------------------------------
function renderWashpadList (){
    //refresh cache of container numbers in case has been udpated
    ContainersCache = refreshContainerCache();
    // clear out list of containers
    $('#washpads').html("");

    var Wtally = [];
    washpadsHTML = "";
    Wtally[0] = 0;
    Wtally[1] = 0;
    Wtally[2] = 0;
    Wtally[3] = 0;

    //console.log(JSON.stringify(ContainersCache));

    $.each(ContainersCache, function (i, v) {
        // hard-coded iterating through containers at each washpad location to build up tally of containers at each washpad
        if (v.Location == "W1") {
            Wtally[0] = Wtally[0] + 1;
        }
        if (v.Location == "W2") {
            Wtally[1] = Wtally[1] + 1;
        }
        if (v.Location == "W3") {
            Wtally[2] = Wtally[2] + 1;
        }
        if (v.Location == "W4") {
            Wtally[3] = Wtally[3] + 1;
        } 
    });  // end each containersCache

    // build up HTML for washpad lines
    for (var i = 0, len = Wtally.length; i < len; i++) {
        if (Wtally[(i)] > 0 ) {
            washpadsHTML = washpadsHTML + "<div id=\"washpad" + (i + 1) + "\" class=\"washpad hascontainers\">";
            washpadsHTML = washpadsHTML + "<a href=\"containerlist.aspx?washpad=" + (i + 1) + "\"><span>W" + (i + 1) + " (" + Wtally[(i)] + " boxes) ></span></a>";
            washpadsHTML = washpadsHTML + "</div>";
        } else
        {
            washpadsHTML = washpadsHTML + "<div id=\"washpad" + (i + 1) + "\" class=\"washpad empty\">";
            washpadsHTML = washpadsHTML + "<a name=\"\"><span>W" + (i + 1) + " (" + Wtally[(i)] + " boxes)</span></a>";
            washpadsHTML = washpadsHTML + "</div>";
        }

    }

    // render line to page
    $('#washpads').append(washpadsHTML);

    return;

} // end renderWashpadList


// ---------------------------- Add 'clicked' class to any elements that could go slowly...
// note different selector syntax because navigation dynamically generated
$('body').on('click', 'div.washpad a', function () {
    $(this).addClass('clicked');
    // alert('clicked a button');
});



// ----------------------------------------------------------------------------------------------------
// ----------------------------------------- Initialise App -------------------------------------------
// ----------------------------------------------------------------------------------------------------
$(document).ready(function () {

    // Check for conainers in WashDB on init, and then every 30 seconds
    renderWashpadList();
    setInterval(renderWashpadList, 30 * 1000);

    // display state messages
    if (getParameterByName("msgtype") == "Success") {
        StatusFlash(getParameterByName("msgtext"));
    }

});
