﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dashboard2.aspx.cs" Inherits="WashPadTabletApp.dashboard2" %>

<html>
<head>
    <!-- fix for jQuery 2.x on IE11 -->
    <meta http-equiv="X-UA-Compatible" content="IE=10;" />
    <title>WashPad Tablet App</title>

    <!-- javascript libraries -->
    <script src="js/jquery.v2.1.4.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui.v1.11.4.min.js" type="text/javascript"></script>
    <script src="js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="js/underscore-min.js" type="text/javascript"></script>

    <!-- style -->
    <link rel="stylesheet" type="text/css" href="css/POL-TabletStyle.css" />
    <link rel="stylesheet" type="text/css" href="css/POL-Dashboards.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />

    <!-- mobile icons -->
    <link rel="icon" sizes="128x128" href="images/launch-icon-128.png" />
    <link rel="apple-touch-startup-image" href="images/launch-icon-128.png" />
    <link rel="apple-touch-icon" sizes="128x128" href="images/launch-icon-128.png">

    <!-- mobile web app flags -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-title" content="Port Chalmers Washpad">
        
</head>


<body>
    <div id="main">
        <div class="washcontainer">
            <div class="padSummaryTileContainer" id="summarywashpadW1">
                <!-- padSummaryTileContainer populated by jQuery client-side -->
            </div>
            <div id="containerlistW1" class="washpadContainerList"> <!-- this div will be populated by jQuery client-side -->
            </div><!-- end #containerlistW1 -->
        </div>
        <div class="washcontainer">
            <div class="padSummaryTileContainer" id="summarywashpadW2">
                <!-- padSummaryTileContainer populated by jQuery client-side -->
            </div>
            <div id="containerlistW2" class="washpadContainerList"> <!-- this div will be populated by jQuery client-side -->
            </div><!-- end #containerlistW2 -->
        </div>    

        <div class="washcontainer">
            <div class="padSummaryTileContainer" id="summarywashpadW3">
                <!-- padSummaryTileContainer populated by jQuery client-side -->
            </div>
            <div id="containerlistW3" class="washpadContainerList"> <!-- this div will be populated by jQuery client-side -->
            </div><!-- end #containerlistW3 -->
        </div>  
        <div class="washcontainer">
            <div class="padSummaryTileContainer" id="summarywashpadW4">
                <!-- padSummaryTileContainer populated by jQuery client-side -->
            </div>
            <div id="containerlistW4" class="washpadContainerList"> <!-- this div will be populated by jQuery client-side -->
            </div><!-- end #containerlistW4 -->
        </div>

        <div id="ActivityStats">
             <!-- this div will be populated by jQuery client-side -->
        </div>

    </div><!-- end #main -->

</body>

<script src="js/POL-dashboard2.js?version=<% Response.Write(typeof(Washpad).Assembly.GetName().Version.ToString());%>" type="text/javascript"></script>

</html>