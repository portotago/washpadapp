﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace WashPadTabletApp.data
{
    public partial class bgp_remedialresubmission : System.Web.UI.Page
    {


        private bool containerHasPhotos(string containerID)
        {
            DataTable dtContainerPhotos = new DataTable();
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                try
                {
                    _conn.Open();
                    string strSQL = "SELECT * FROM containerPhotos WHERE containerId = '" + containerID + "'";
                    SqlCommand cmd = new SqlCommand(strSQL, _conn);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        dtContainerPhotos.Load(reader);
                    }
                }
                catch (Exception ex)
                {
                    // TODO: handle this
                }
            }

            if (dtContainerPhotos.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        } // end containerHasPhotos

        private bool containerHasRepairs(string containerID)
        {
            DataTable dtContainerRepairs = new DataTable();
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                try
                {
                    _conn.Open();
                    string strSQL = "SELECT * FROM containerRepairs WHERE containerId = '" + containerID + "'";
                    SqlCommand cmd = new SqlCommand(strSQL, _conn);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        dtContainerRepairs.Load(reader);
                    }
                }
                catch (Exception ex)
                {
                    // TODO: handle this
                }
            }

            if (dtContainerRepairs.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        } // end containerHasRepairs

        private void emailNonDepotPROBox(string containerID)
        {
            var log = log4net.LogManager.GetLogger(this.GetType());
            log.Warn("func submitEstimateForContainerToDepotPRO: " + "Container " + containerID + " owner is not set up on DepotPRO - emailing inspection instead of sending to DepotPRO.");

            DataTable dtContainerState = new DataTable();
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                try
                {
                    _conn.Open();
                    string strSQL = "SELECT * FROM containerStates WHERE containerId = '" + containerID + "'";
                    SqlCommand cmd = new SqlCommand(strSQL, _conn);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        dtContainerState.Load(reader);
                    }
                    _conn.Close();
                }
                catch (Exception ex)
                {
                    // TODO: return error
                    log.Error("func submitEstimateForContainerToDepotPRO: " + "Error reading from WashDB containerState: " + ex.Message);
                }
            }

            string emailSubject = dtContainerState.Rows[0]["operatorCode"].ToString() + " container " + containerID + " inspected on washpad";
            string emailBody = "Inspection findings follow:\n\n";

            emailBody = emailBody + "\n----------CONTAINER--------------\n";
            emailBody = emailBody + "Container No.: " + containerID + "\n";
            emailBody = emailBody + "Owner: " + dtContainerState.Rows[0]["operatorCode"].ToString() + "\n";

            emailBody = emailBody + "\n---------SERVICES IDENTIFIED--------\n";
            emailBody = emailBody + (Convert.ToInt32(dtContainerState.Rows[0]["taskInspect"]) > 9 ? "INSPECT\n" : "");
            emailBody = emailBody + (Convert.ToInt32(dtContainerState.Rows[0]["taskWash"]) > 9 ? "WASH\n" : "");
            emailBody = emailBody + (Convert.ToInt32(dtContainerState.Rows[0]["taskBaseWash"]) > 9 ? "BASE WASH\n" : "");
            emailBody = emailBody + (Convert.ToInt32(dtContainerState.Rows[0]["taskRoofWash"]) > 9 ? "ROOF WASH\n" : "");
            emailBody = emailBody + (Convert.ToInt32(dtContainerState.Rows[0]["taskExternalWash"]) > 9 ? "EXTERNAL CLEAN\n" : "");
            emailBody = emailBody + (Convert.ToInt32(dtContainerState.Rows[0]["taskPhoto"]) > 9 ? "PHOTO\n" : "");
            emailBody = emailBody + (Convert.ToInt32(dtContainerState.Rows[0]["taskRepair"]) > 9 ? "REPAIR\n" : "");
            emailBody = emailBody + (Convert.ToInt32(dtContainerState.Rows[0]["taskSteamClean"]) > 9 ? "STEAM CLEAN\n" : "");
            emailBody = emailBody + (Convert.ToInt32(dtContainerState.Rows[0]["taskSteamCleanFQ"]) > 9 ? "STEAM CLEAN FQ\n" : "");

            emailBody = emailBody + "\n---------REPAIRS IDENTIFIED--------\n";

            // repairLines from containerRepairs
            DataTable dtContainerRepairs = new DataTable();
            DataTable dtRepairCodes = new DataTable();
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                try
                {
                    _conn.Open();
                    string strSQL = "SELECT * FROM containerRepairs WHERE containerId = '" + containerID + "'";
                    SqlCommand cmd = new SqlCommand(strSQL, _conn);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        dtContainerRepairs.Load(reader);
                    }
                    _conn.Close();
                }
                catch (Exception ex)
                {
                    log.Error("func submitEstimateForContainerToDepotPRO: " + "Error reading from WashDB containerRepairs: " + ex.Message);
                }
            }
            if (dtContainerRepairs.Rows.Count > 0)   // If there are repair lines, build SQL and insert into cpEstimateLines
            {
                DataView dvContainerRepairs = new DataView(dtContainerRepairs);  // dataview for subsequent filtering

                foreach (DataRowView rowView in dvContainerRepairs)
                {
                    DataRow repairRow = rowView.Row;
                    emailBody = emailBody + "---\n";
                    emailBody = emailBody + "Damage location: " + repairRow["damageLocation"] + "\n";
                    emailBody = emailBody + "Repair Description: " + repairRow["repairDescription"] + "\n";
                    emailBody = emailBody + "Quantity: " + repairRow["quantity"] + "\n";
                }
            }

            emailBody = emailBody + "\n\n------------------------------------\nThis is an automated message sent by the washpad tablet app, do not reply.";

            // Prepare to email
            SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["EmailServer"].ToString(), 25);
            smtpClient.EnableSsl = false;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            // Sent email
            MailMessage mail = new MailMessage();

            string[] multiEmails = ConfigurationManager.AppSettings["EmailRecipients_NonDepotPROBoxes"].ToString().Split(';');
            foreach (string emailAddress in multiEmails)
            {
                mail.To.Add(new MailAddress(emailAddress));
            }

            mail.From = new MailAddress(ConfigurationManager.AppSettings["EmailSender"].ToString(), ConfigurationManager.AppSettings["EmailSenderName"].ToString());
            mail.Body = emailBody;
            mail.Subject = emailSubject;
            try
            {
                smtpClient.Send(mail);
            }
            catch
            {
                // possibly could not reach the mail server - if not caught results in a hard failure
                log.Error("error executing smtpClient.Send");
            }
            mail.Dispose();



        }

        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            StreamReader sr = new StreamReader(strFilePath);
            string[] headers = sr.ReadLine().Split(',');
            DataTable dt = new DataTable();
            foreach (string header in headers)
            {
                dt.Columns.Add(header);
            }
            while (!sr.EndOfStream)
            {
                string[] rows = Regex.Split(sr.ReadLine(), ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                DataRow dr = dt.NewRow();
                for (int i = 0; i < headers.Length; i++)
                {
                    dr[i] = rows[i];
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        private List<string> applyPOLBusinessRules(List<string> servicesIdentified, string containerID)
        {
            // Note that at time of writing 13.3.2017 only two of the business rules are applicable for this app

            var log = log4net.LogManager.GetLogger(this.GetType());
            log.Info("backgroundprocessor.aspx: " + "applying POL Business rules for container: " + containerID);
            Response.Write("<br/>backgroundprocessor.aspx: " + "applying POL Business rules for container: " + containerID);

            log.Debug("backgroundprocessor.aspx: " + "Services for " + containerID + " before business rules: " + string.Join("\t", servicesIdentified.Cast<string>().ToArray()));
            Response.Write("<br/>Services for " + containerID + " before business rules: " + string.Join("\t", servicesIdentified.Cast<string>().ToArray()));

            // Get container details...
            DataTable dtContainerDetails = new DataTable();
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                _conn.Open();
                string strSQL = "SELECT * FROM containerStates WHERE containerID = '" + containerID + "'";
                SqlCommand cmd = new SqlCommand(strSQL, _conn);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    dtContainerDetails.Load(reader);
                }
            }

            if (dtContainerDetails.Rows.Count > 0)
            {
                DataRow containerState = dtContainerDetails.Rows[0];

                // ---------------------------------------------- POL BUSINESS RULE #1 ---------
                // condition: operatorCode=="MSK" && taskDBUpgrade3 == "Yes"
                // strip tasks: Steam.C FQ, STEAM.C, INTSWEEP, WASH
                if (containerState["operatorCode"].ToString() == "MSK" && containerState["taskDBUpgrade3"].ToString() == "DB Upgrade 3")
                {
                    servicesIdentified.RemoveAll(item => item.Contains("'STEAM.C FQ'"));
                    servicesIdentified.RemoveAll(item => item.Contains("'STEAM.C'"));
                    servicesIdentified.RemoveAll(item => item.Contains("'WASH'"));
                }

                // ---------------------------------------------- POL BUSINESS RULE #2 ---------
                // condition: operatorCode=="MSK"
                // strip tasks: Photo
                if (containerState["operatorCode"].ToString() == "MSK")
                {
                    servicesIdentified.RemoveAll(item => item.Contains("'Photo'"));
                }

                // ---------------------------------------------- POL BUSINESS RULE #3 --------- N/A
                // condition: operatorCode=="ANL" && taskDBCertAdmin == "Yes"
                // alter tasks: Replace service "DB Cert Admin" with "DB Upgrade"

                // ---------------------------------------------- POL BUSINESS RULE #4 --------- N/A
                // condition: RecMT=="YES" && RecMode == "SHIP" && IsReefer = 1 
                // strip tasks: ReqInsp (also these that are not handled by the app: IntSweep, EPTI)

                // ---------------------------------------------- POL BUSINESS RULE #5 --------- N/A
                // condition: operatorCode=="MSK", RecMT=="YES" && RecMode == "SHIP"
                // strip tasks: ReqInsp, IntSweep, EPTI, MPI6SI, MPI6 S Reinsp, Tempset, MPE Ex Clean, MPI Int. Insp, MPI Tape)

                // ---------------------------------------------- POL BUSINESS RULE #6 --------- N/A
                // condition: operatorCode=="HSD" 
                // strip tasks: DOORS

                // ---------------------------------------------- POL BUSINESS RULE #7 --------- N/A
                // condition: operatorCode=="HLL" 
                // strip tasks: ReqInsp, MPI6SI, MPI6 S Reinsp

                // ---------------------------------------------- POL BUSINESS RULE #8 --------- N/A
                // condition: PT_lessthan_Rec=="YES" 
                // strip tasks: PTI
            }
            else
            {
                // Log error, we couldn't find the container
                log.Error("backgroundprocessor.aspx: " + "error applying POL Business rules for container: " + containerID + " - could not find it in the database.");
            }

            log.Debug("backgroundprocessor.aspx: " + "Services for " + containerID + " after business rules: " + string.Join("\t", servicesIdentified.Cast<string>().ToArray()));
            Response.Write("<br/>Services for " + containerID + " after business rules: " + string.Join("\t", servicesIdentified.Cast<string>().ToArray()));

            return servicesIdentified;
        } // end applyPOLBusinessRules

        private int compileEstimate(string containerID, string tariffType, DataRow dpContainerRow, DataRow washDbContainerRow)
        {
            var context = HttpContext.Current;
            var log = log4net.LogManager.GetLogger(this.GetType());
            int PK_ESTIMATE = 0;
            string strNewEstimateSQL = "";

            strNewEstimateSQL = strNewEstimateSQL + "INSERT INTO cpEstimate (";
            strNewEstimateSQL = strNewEstimateSQL + "ESTIMATE_NO, "; // use containerStateId for ESTIMATE_NO, gets overridden once loaded to DP
            strNewEstimateSQL = strNewEstimateSQL + "BRANCH_CODE, ";
            strNewEstimateSQL = strNewEstimateSQL + "USERNAME, ";
            strNewEstimateSQL = strNewEstimateSQL + "CONTAINER_NO, ";
            strNewEstimateSQL = strNewEstimateSQL + "OWNER, "; // [2] is the column for owner
            strNewEstimateSQL = strNewEstimateSQL + "ESTIMATE_DATE, ";
            strNewEstimateSQL = strNewEstimateSQL + "NOTES, ";  // [19] is the column for notes... TODO - better lookup!  probably use a dataview
            strNewEstimateSQL = strNewEstimateSQL + "INSPECTED_BY, ";
            strNewEstimateSQL = strNewEstimateSQL + "ISO_CODE, ";
            strNewEstimateSQL = strNewEstimateSQL + "SIZE_CODE, ";
            strNewEstimateSQL = strNewEstimateSQL + "HEIGHT_CODE, ";
            strNewEstimateSQL = strNewEstimateSQL + "TYPE_CODE, ";
            strNewEstimateSQL = strNewEstimateSQL + "UPDATE_BATCH_NO, ";
            strNewEstimateSQL = strNewEstimateSQL + "QUALITY_CODE, ";
            strNewEstimateSQL = strNewEstimateSQL + "LOAD_STATUS, ";
            strNewEstimateSQL = strNewEstimateSQL + "TARIFF_CODE, ";
            strNewEstimateSQL = strNewEstimateSQL + "FINALISE_ENTRY) ";

            strNewEstimateSQL = strNewEstimateSQL + "OUTPUT INSERTED.PK_ESTIMATE ";

            strNewEstimateSQL = strNewEstimateSQL + "VALUES (";
            strNewEstimateSQL = strNewEstimateSQL + "'" + washDbContainerRow[0] + "_" + tariffType + "', "; // use containerStateId for ESTIMATE_NO, gets overridden once loaded to DP, avoids duplicate submissions
            strNewEstimateSQL = strNewEstimateSQL + "'PC', ";
            strNewEstimateSQL = strNewEstimateSQL + "'" + Context.User.Identity.Name + "', ";
            strNewEstimateSQL = strNewEstimateSQL + "'" + containerID + "', ";
            strNewEstimateSQL = strNewEstimateSQL + "'" + washDbContainerRow[3] + "', "; //  [3] is the column for OWNER in WashDB containerStates
            strNewEstimateSQL = strNewEstimateSQL + "'" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', ";
            strNewEstimateSQL = strNewEstimateSQL + "'" + washDbContainerRow[20] + "', ";  // [20] is the column for notes...
            strNewEstimateSQL = strNewEstimateSQL + "'" + washDbContainerRow[26] + "', "; // [26] is the column for UPDATED_BY, used for INSPECTED_BY
            strNewEstimateSQL = strNewEstimateSQL + "'" + dpContainerRow[1] + "', "; // ISO_CODE
            strNewEstimateSQL = strNewEstimateSQL + "'" + dpContainerRow[2] + "', "; // SIZE_CODE
            strNewEstimateSQL = strNewEstimateSQL + "'" + dpContainerRow[3] + "', "; // HEIGHT_CODE
            strNewEstimateSQL = strNewEstimateSQL + "'" + dpContainerRow[4] + "', "; // TYPE_CODE
            strNewEstimateSQL = strNewEstimateSQL + "'0', "; // UPDATE_BATCH_NO - UNKNOWN USAGE
            strNewEstimateSQL = strNewEstimateSQL + "'" + dpContainerRow[6] + "', "; // QUALITY_CODE  
            strNewEstimateSQL = strNewEstimateSQL + "'" + dpContainerRow[7] + "', "; // LOAD_STATUS 
            switch (tariffType)
            {
                case "02":
                    strNewEstimateSQL = strNewEstimateSQL + "'02', "; // 43 TARIFF_CODE FOR MSK REPAIRS
                    break;
                case "43":
                    strNewEstimateSQL = strNewEstimateSQL + "'43', "; // 43 TARIFF_CODE FOR MSK SERVICES
                    break;
                default:
                    // sdfsdf
                    strNewEstimateSQL = strNewEstimateSQL + "'', "; // NULL TARIFF_CODE FOR ALL OTHERS
                    break;
            }

            if (washDbContainerRow[21].ToString() == "complete")   // [20] is the column for quoteStatus
            {
                strNewEstimateSQL = strNewEstimateSQL + "'true')";  // FINALISE_ENTRY conditional on quoteStatus from tablet
            }
            else
            {
                strNewEstimateSQL = strNewEstimateSQL + "'false')";  // FINALISE_ENTRY conditional on quoteStatus from tablet
            }
            log.Debug("func submitEstimateForContainerToDepotPRO: " + "inserting estimate line into WashDB. strNewEstimateSQL: " + strNewEstimateSQL);

            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                try
                {
                    _conn.Open();
                    SqlCommand cmd = new SqlCommand(strNewEstimateSQL, _conn);
                    PK_ESTIMATE = (int)cmd.ExecuteScalar();
                    cmd.Dispose();
                    Response.Write("<br/>SQL INSERT executed. PK_ESTIMATE=" + PK_ESTIMATE);
                }
                catch (Exception ex)
                {
                    // TODO: log/alert/return error
                    Response.Write("<br/>Error with SQL INSERT: " + ex.Message);
                    log.Error("func submitEstimateForContainerToDepotPRO: " + "Error with SQL INSERT: " + ex.Message);
                }
            }
            return PK_ESTIMATE;
        } // end function compileEstimate

        private void compileRepairLinesForEstimate(string containerID, int estimateId, DataRow dpContainerRow)
        {
            var log = log4net.LogManager.GetLogger(this.GetType());

            DataTable dtContainerRepairs = new DataTable();
            DataTable dtRepairCodes = new DataTable();

            // 2a) get repairLines from containerRepairs
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                try
                {
                    _conn.Open();
                    string strSQL = "SELECT * FROM containerRepairs WHERE containerId = '" + containerID + "'";
                    SqlCommand cmd = new SqlCommand(strSQL, _conn);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        dtContainerRepairs.Load(reader);
                    }
                }
                catch (Exception ex)
                {
                    // TODO: return error
                    log.Error("func submitEstimateForContainerToDepotPRO: " + "Error reading from WashDB containerRepairs: " + ex.Message);
                }
            }

            if (dtContainerRepairs.Rows.Count > 0)   // If there are repair lines, build SQL and insert into cpEstimateLines
            {

                // 2b) get repair line detail from CSV
                dtRepairCodes = ConvertCSVtoDataTable(ConfigurationManager.AppSettings["RepairLinesDataFile"].ToString());
                //Response.Write(dataTableToHTML(dtRepairCodes));

                // 2c) derive fully-formed containerEstimateRepairLines rows
                DataView dvRepairCodes = new DataView(dtRepairCodes);
                string SQLInsertRepairLineFragment = "";

                DataView dvContainerRepairs = new DataView(dtContainerRepairs);  // dataview for subsequent filtering
                int lineNo = 0;
                foreach (DataRowView rowView in dvContainerRepairs)
                {
                    lineNo++;
                    DataRow repairRow = rowView.Row;
                    dvRepairCodes.RowFilter = "ID = " + repairRow["repairCode"];   // Filter repair codes by ID, assume only results in 1 row
                    foreach (DataRowView rowV in dvRepairCodes)
                    {
                        // Compile SQL for the repair line. Note the exceptions for HSD, HLL, MSK
                        DataRow rowRepairCode = rowV.Row;
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "(";
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + estimateId + ", ";
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + lineNo + ", ";
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + repairRow["damageLocation"] + "', "; // LOC_CODE
                        if (dpContainerRow[5].ToString() == "HSD")
                        {
                            SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["DAM_HSD"] + "', "; // DAM_CODE
                        }
                        else
                        {
                            SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["DAM"] + "', "; // DAM_CODE
                        }
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["RESP_CODE"] + "', "; // RESP_CODE
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["MAT"] + "', "; // MAT_CODE
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["EQTYPE_CODE"] + "', "; // EQTYPE_CODE
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["COMP"] + "', "; // COMP_CODE
                        if (dpContainerRow[5].ToString() == "HSD")
                        {
                            SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["REP_HSD"] + "', "; // REP_CODE
                        }
                        else
                        {
                            SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["REP"] + "', "; // REP_CODE
                        }
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["CAT_CODE"] + "', "; // CAT_CODE
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["Charge By"] + "', "; // CHARGE_BY
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["UOM"] + "', "; // UOM_CODE
                        //SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["QTY"] + "', "; // QTY  TODO: THIS NEEDS TO COME FROM APP
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + repairRow["quantity"].ToString() + "', ";

                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["LENGTH"] + "', "; // LENGTH
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["HEIGHT"] + "', "; // HEIGHT
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["HOURS"] + "', "; // HOURS
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["MATERIAL"] + "', "; // MATERIAL
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["Notes"] + "', "; // NOTES
                        // TARIFFDET CODES FOR MSK, HLL, BLANK FOR REST:
                        if (dpContainerRow[5].ToString() == "MSK")
                        {
                            SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["TARIFF"]; // LAST LINE WITH NO COMMA , 
                        }
                        else if (dpContainerRow[5].ToString() == "HLL")
                        {
                            SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["TARIFF_HLL"]; // LAST LINE WITH NO COMMA , 
                        }
                        else
                        {
                            SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'"; // LAST LINE WITH NO COMMA , 
                        }
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'), ";
                    }
                }
                SQLInsertRepairLineFragment = SQLInsertRepairLineFragment.Substring(0, SQLInsertRepairLineFragment.Length - 2);  // remove trailing comma
                SQLInsertRepairLineFragment = "INSERT INTO cpEstimateLines (FK_ESTIMATE, LINE_NO, LOC_CODE, DAM_CODE, RESP_CODE, MAT_CODE, EQTYPE_CODE, COMP_CODE, REP_CODE, CAT_CODE, CHARGE_BY,UOM_CODE,QTY,LENGTH,HEIGHT,HOURS,MATERIAL, NOTES,TARIFFDET_CODE) VALUES " + SQLInsertRepairLineFragment;
                Response.Write("SQLInsertRepairLineFragment: " + SQLInsertRepairLineFragment);
                using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
                {
                    try
                    {
                        _conn.Open();
                        SqlCommand cmd = new SqlCommand(SQLInsertRepairLineFragment, _conn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        Response.Write("<br/>SQL REPAIR LINES INSERT executed.");
                    }
                    catch (Exception ex)
                    {
                        // TODO: log/alert/return error
                        Response.Write("<br/>Error with SQL REPAIR LINE INSERT: " + ex.Message);
                    }
                }
            }
            else
            {
                Response.Write("No repair lines for this estimate.");
                log.Info("No repair lines for this estimate.");
            }

        } // end function compileRepairLinesForEstimate

        private string compileServicesForEstimate(string containerID, int estimateID, string estimateType, DataRow washDbContainerRow)
        {
            string strSQLServiceInsertLines = "";
            List<string> strSQLValues = new List<string>();
            List<string> listServiceCreationLines = new List<string>();
            string sqlStatusUpdate = "";

            // ADD each service, including services added on the tablet (10) and services that come through from Jade (23)
            // Note that estimateType accomodates putting ONLY repair service against an 02 eimstate, ONLY non-repair services against 43, ANY against a default estimate

            if (estimateType == "02" || estimateType == "default")
            {
                if (washDbContainerRow["taskRepair"].ToString() == "40" || washDbContainerRow["taskRepair"].ToString() == "10" || washDbContainerRow["taskRepair"].ToString() == "23" || washDbContainerRow["taskRepair"].ToString() == "22" || washDbContainerRow["taskRepair"].ToString() == "30")
                {
                    listServiceCreationLines.Add("(" + estimateID + ",'Repair')");
                    if (washDbContainerRow["taskRepair"].ToString() == "10")
                    {
                        sqlStatusUpdate = sqlStatusUpdate + "taskRepair= '" + 20 + "', ";  // 20 = Task has been submitted to DepotPRO
                    }
                }
            }

            if (estimateType != "02")
            {

                if (washDbContainerRow["taskWash"].ToString() == "40" || washDbContainerRow["taskWash"].ToString() == "10" || washDbContainerRow["taskWash"].ToString() == "23" || washDbContainerRow["taskWash"].ToString() == "22")
                {
                    listServiceCreationLines.Add("(" + estimateID + ",'WASH')");
                    if (washDbContainerRow["taskWash"].ToString() == "10")
                    {
                        sqlStatusUpdate = sqlStatusUpdate + "taskWash= '" + 20 + "', "; // 20 = Task has been submitted to DepotPRO
                    }
                }

                if ((washDbContainerRow["taskPhoto"].ToString() == "10" && containerHasPhotos(containerID)) || washDbContainerRow["taskPhoto"].ToString() == "40" || washDbContainerRow["taskPhoto"].ToString() == "23")
                {
                    listServiceCreationLines.Add("(" + estimateID + ",'Photo')");
                    if (washDbContainerRow["taskPhoto"].ToString() == "10")
                    {
                        sqlStatusUpdate = sqlStatusUpdate + "taskPhoto= '" + 20 + "', ";  // 20 = Task has been submitted to DepotPRO
                    }
                }

                if (washDbContainerRow["taskBaseWash"].ToString() == "40" || washDbContainerRow["taskBaseWash"].ToString() == "10" || washDbContainerRow["taskBaseWash"].ToString() == "23")
                {
                    listServiceCreationLines.Add("(" + estimateID + ",'BASE WASH (pol)')");
                    if (washDbContainerRow["taskBaseWash"].ToString() == "10")
                    {
                        sqlStatusUpdate = sqlStatusUpdate + "taskBaseWash= '" + 20 + "', ";  // 20 = Task has been submitted to DepotPRO
                    }
                }

                if (washDbContainerRow["taskRoofWash"].ToString() == "40" || washDbContainerRow["taskRoofWash"].ToString() == "10" || washDbContainerRow["taskRoofWash"].ToString() == "23")
                {
                    listServiceCreationLines.Add("(" + estimateID + ",'Roof Wash')");
                    if (washDbContainerRow["taskRoofWash"].ToString() == "10")
                    {
                        sqlStatusUpdate = sqlStatusUpdate + "taskRoofWash= '" + 20 + "', ";  // 20 = Task has been submitted to DepotPRO
                    }
                }

                if (washDbContainerRow["taskExternalWash"].ToString() == "40" || washDbContainerRow["taskExternalWash"].ToString() == "10" || washDbContainerRow["taskExternalWash"].ToString() == "23")
                {
                    listServiceCreationLines.Add("(" + estimateID + ",'EXTCLEAN')");
                    if (washDbContainerRow["taskExternalWash"].ToString() == "10")
                    {
                        sqlStatusUpdate = sqlStatusUpdate + "taskExternalWash= '" + 20 + "', ";  // 20 = Task has been submitted to DepotPRO
                    }
                }

                if (washDbContainerRow["taskSteamClean"].ToString() == "40" || washDbContainerRow["taskSteamClean"].ToString() == "10" || washDbContainerRow["taskSteamClean"].ToString() == "23")
                {
                    listServiceCreationLines.Add("(" + estimateID + ",'STEAM.C')");
                    if (washDbContainerRow["taskSteamClean"].ToString() == "10")
                    {
                        sqlStatusUpdate = sqlStatusUpdate + "taskSteamClean= '" + 20 + "', ";  // 20 = Task has been submitted to DepotPRO
                    }
                }

                if (washDbContainerRow["taskSteamCleanFQ"].ToString() == "40" || washDbContainerRow["taskSteamCleanFQ"].ToString() == "10" || washDbContainerRow["taskSteamCleanFQ"].ToString() == "23")
                {
                    listServiceCreationLines.Add("(" + estimateID + ",'STEAM.C FQ')");
                    if (washDbContainerRow["taskSteamCleanFQ"].ToString() == "10")
                    {
                        sqlStatusUpdate = sqlStatusUpdate + "taskSteamCleanFQ= '" + 20 + "', ";  // 20 = Task has been submitted to DepotPRO
                    }
                }
            }

            // Apply POL business rules to supress ToDo tasks that meet critiera
            listServiceCreationLines = applyPOLBusinessRules(listServiceCreationLines, containerID);

            if (listServiceCreationLines.Count > 0)
            {
                strSQLServiceInsertLines = "INSERT INTO cpEstimateServices (FK_ESTIMATE,SERVICE_CODE) VALUES " + string.Join(", ", listServiceCreationLines.ToArray());
                Response.Write("<br><br>strSQLServiceInsertLines = " + strSQLServiceInsertLines);
                using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
                {
                    try
                    {
                        _conn.Open();
                        SqlCommand cmd = new SqlCommand(strSQLServiceInsertLines, _conn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        Response.Write("<br/>SQL SERVICE LINES INSERT executed.<br>");
                    }
                    catch (Exception ex)
                    {
                        // TODO: log/alert/return error
                        Response.Write("<br/>Error with SQL SERVICE LINE INSERT: " + ex.Message);
                    }
                }
            }
            else
            {
                Response.Write("<br><br>no service lines for this estimate.");
            }

            return sqlStatusUpdate;
        } // end function compileServicesForEstimate

        private bool submitEstimateViaWebService(int estimateId)
        {
            var log = log4net.LogManager.GetLogger(this.GetType());

            DataTable dtEstimates1 = new DataTable();
            DataTable dtEstimateLines1 = new DataTable();
            DataTable dtEstimateServices1 = new DataTable();
            DataTable dtEstimatePhotos = new DataTable();
            DataTable dtStocktake = new DataTable();

            // Fill Datatables from DB
            using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                dbConnection.Open();

                SqlCommand cmd = new SqlCommand("SELECT * FROM cpEstimate WHERE PK_ESTIMATE = " + estimateId, dbConnection);
                using (SqlDataAdapter a = new SqlDataAdapter(cmd))
                {
                    a.Fill(dtEstimates1);
                }

                cmd = new SqlCommand(" SELECT cpEstimateLines.* from cpEstimateLines inner join cpEstimate on (cpEstimate.PK_ESTIMATE = cpEstimateLines.FK_ESTIMATE) " + " WHERE cpEstimate.PK_ESTIMATE = " + estimateId, dbConnection);
                using (SqlDataAdapter a = new SqlDataAdapter(cmd))
                {
                    a.Fill(dtEstimateLines1);
                }

                cmd = new SqlCommand(" SELECT cpEstimateServices.* from cpEstimateServices inner join cpEstimate on (cpEstimate.PK_ESTIMATE = cpEstimateServices.FK_ESTIMATE) " + " WHERE (cpEstimate.PK_ESTIMATE = " + estimateId + ") and (SERVICE_CODE <> '') and (SERVICE_CODE is not null)", dbConnection);
                using (SqlDataAdapter a = new SqlDataAdapter(cmd))
                {
                    a.Fill(dtEstimateServices1);
                }

                cmd = new SqlCommand("SELECT cpEstimatePhotos.* from cpEstimatePhotos inner join cpEstimate on (cpEstimate.PK_ESTIMATE = cpEstimatePhotos.FK_ESTIMATE) " + " WHERE (cpEstimate.PK_ESTIMATE = " + estimateId + ")", dbConnection);
                using (SqlDataAdapter a = new SqlDataAdapter(cmd))
                {
                    a.Fill(dtEstimatePhotos);
                }

                cmd = new SqlCommand(" SELECT * from cpStocktake", dbConnection);
                using (SqlDataAdapter a = new SqlDataAdapter(cmd))
                {
                    a.Fill(dtStocktake);
                }
            }

            try
            {
                // Supress estimate auto-finalise if the owner is in configuration list SupressAutoFinalize
                if (ConfigurationManager.AppSettings["SupressAutoFinalize"].ToString().IndexOf(dtEstimates1.Rows[0]["OWNER"].ToString()) >= 0)
                {
                    dtEstimates1.Rows[0]["FINALISE_ENTRY"] = "False";
                    log.Info("func submitEstimateForContainerToDepotPRO: " + "Suppressing auto-finalise estimate for estimate " + estimateId.ToString());
                }
            }

            catch (Exception e)
            {
                Response.Write("func submitEstimateForContainerToDepotPRO: " + "Error submitting estimate to web service for estimate " + estimateId.ToString());
                log.Error("func submitEstimateForContainerToDepotPRO: " + "Error submitting estimate to web service for estimate " + estimateId.ToString());
                log.Error("func submitEstimateForContainerToDepotPRO: " + e.Message);
            }

            // Populate dataSet from dataTables
            DataSet ds = new DataSet();
            ds.Tables.Add(dtEstimates1);
            ds.Tables.Add(dtEstimateLines1);
            ds.Tables.Add(dtEstimateServices1);
            ds.Tables.Add(dtEstimatePhotos);
            ds.Tables.Add(dtStocktake);

            // Pass dataset to web service
            bool success = false;  // guilty until proven innocent
            try
            {
                string mystring = "";
                DepotProHHWebService.DepotProHHeldSvcSoapClient DPSoapClient = new DepotProHHWebService.DepotProHHeldSvcSoapClient();
                success = DPSoapClient.UploadEstimatesVersion(out mystring, ds, "3.11"); // 3.10
                if (success)
                {
                    log.Info("Successful submission of estimate to DepotPRO web service.  Web service reponse: " + mystring);
                    Response.Write("Successful submission of estimate to DepotPRO web service. Web service reponse: " + mystring);
                }
                else
                {
                    log.Error("Failed submission of estimate to DepotPRO web service. Web service reponse: " + mystring);
                    Response.Write("Failed submission of estimate to DepotPRO web service. Web service reponse: " + mystring);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error with web service submission: " + ex.Message);
            }

            return success;
        } // end function submitEstimateViaWebService

        private bool submitEstimateForContainerToDepotPRO(string containerID)
        {
            // Compile estimate and submit to DepotPRO via web service
            // calls out to other functions

            // -----------------------------------------------------------------------------------------------------------
            // -------------------------------------- 0) Init, Validation  -----------------------------
            // -----------------------------------------------------------------------------------------------------------
            var context = HttpContext.Current;
            var log = log4net.LogManager.GetLogger(this.GetType());
            log.Info("func submitEstimateForContainerToDepotPRO: " + "Generating estimate for " + containerID);
            DataTable dtContainerState = new DataTable();
            DataTable dtContainerFromDP = new DataTable();
            DataTable dtCpEstimates = new DataTable();
            //int PK_ESTIMATE = 0;
            string strWashDBStatusUpdateSQL = "UPDATE containerStates SET ";

            // Get container row from WashDB containerStates
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                try
                {
                    _conn.Open();
                    string strSQL = "SELECT * FROM containerStates WHERE containerId = '" + containerID + "'";
                    SqlCommand cmd = new SqlCommand(strSQL, _conn);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        dtContainerState.Load(reader);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("func submitEstimateForContainerToDepotPRO: " + "Error reading from WashDB containerState: " + ex.Message);
                }
            }


            // Check if there's already an estimate for this container.  If so, cancel compilation/submission of the estimate, alert IT
            // Note that the existing estimate will get submitted again
            DataRow containerStateRow = dtContainerState.Rows[0];
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                try
                {
                    _conn.Open();
                    string strSQLCheck = "SELECT * FROM cpEstimate WHERE ESTIMATE_NO IN ('" + dtContainerState.Rows[0]["containerStateId"] + "_','" + dtContainerState.Rows[0]["containerStateId"] + "_02','" + dtContainerState.Rows[0]["containerStateId"] + "_43')";
                    log.Info(strSQLCheck);
                    SqlCommand cmd = new SqlCommand(strSQLCheck, _conn);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        dtCpEstimates.Load(reader);
                    }
                    if (dtCpEstimates.Rows.Count > 0)
                    {
                        // there's already an estimate... crash out
                        log.Warn("func submitEstimateForContainerToDepotPRO: " + "Already generated estimate " + dtContainerState.Rows[0]["containerStateId"] + "_ for this container.  Not proceeding with estimate generation for " + dtContainerState.Rows[0]["containerId"]);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    log.Error("func submitEstimateForContainerToDepotPRO: " + "Error reading from " + ConfigurationManager.AppSettings["DPDBName"].ToString() + " cEstimates: " + ex.Message);
                }
            }


            // Get further container attributes from DepotPRO cpContainer table
            // TODO: SQL METHOD NOT OPTIMAL, ENHANCEMENT TO RETRIEVE INFO FROM WEB SERVICE, WILL NEED SUPPORT FROM GLOBAL
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connDepotPRODB"].ConnectionString))
            {
                try
                {
                    _conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT [CONTAINER_NO],[ISO_CODE],[SIZE_CODE],[HEIGHT_CODE],[TYPE_CODE],[OWNER],[QUALITY_CODE],[LOAD_STATUS] FROM [" + ConfigurationManager.AppSettings["DPDBName"].ToString() + "].[dbo].[cpContainer] WHERE CONTAINER_NO='" + containerID + "' AND RELEASED = 0 ORDER BY REF_NO DESC", _conn);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        dtContainerFromDP.Load(reader);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("func submitEstimateForContainerToDepotPRO: " + "Error reading from " + ConfigurationManager.AppSettings["DPDBName"].ToString() + " cpContainer. Error: " + ex.Message);
                }
            }


            // ----------------------------------------------------------- 
            // Check if this is non-DepotPRO (E.g. PIL) box, if so, send the inspection results to ?____? via email
            log.Warn("func submitEstimateForContainerToDepotPRO: " + "Container " + containerID + " owner is " + dtContainerState.Rows[0]["operatorCode"].ToString());
            if (ConfigurationManager.AppSettings["nonDepotPRO"].ToString().IndexOf(dtContainerState.Rows[0]["operatorCode"].ToString()) >= 0)
            {
                // The box owner is NOT set up in depotPRO, so
                // email to repair
                // then return true - a successful submission

                emailNonDepotPROBox(containerID);

                return true;
            } // --------------------- FUNCTION EXITED HERE FOR NON_DEPOTPRO CONTAINERS AS A SUCCESFUL SUBMISSION



            if (dtContainerFromDP.Rows.Count < 1)
            {
                // No rows returned from DepotPRO, we'll consider this a failed gate-in
                log.Warn("func submitEstimateForContainerToDepotPRO: " + "Container " + containerID + " not found in DepotPRO - handling failed gate-in");
                Response.Write("<br>func submitEstimateForContainerToDepotPRO: " + "Container " + containerID + " not found in DepotPRO, handling failed gate-in.");


                // Increment failed count
                int failedCount = 0;
                using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
                {
                    _conn.Open();
                    SqlCommand cmd = new SqlCommand("UPDATE containerStates SET failedEstimateSubmissionCount = failedEstimateSubmissionCount + 1 OUTPUT INSERTED.failedEstimateSubmissionCount WHERE containerID = '" + containerID + "'", _conn);
                    failedCount = (int)cmd.ExecuteScalar();
                }
                log.Warn("func submitEstimateForContainerToDepotPRO: " + "failedEstimateSubmissionCount for " + containerID + ": " + failedCount.ToString());
                Response.Write("<br>func submitEstimateForContainerToDepotPRO: " + "failedEstimateSubmissionCount for " + containerID + ": " + failedCount.ToString());

                if (failedCount <= int.Parse(ConfigurationManager.AppSettings["maxFailureAlerts"]))
                {
                    log.Warn("func submitEstimateForContainerToDepotPRO: " + "sending email alert regarding failed gate-in for " + containerID);
                    Response.Write("<br>func submitEstimateForContainerToDepotPRO: " + "sending email alert regarding failed gate-in for " + containerID);

                    // Prepare to email
                    SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["EmailServer"].ToString(), 25);
                    smtpClient.EnableSsl = false;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    // Sent email alert
                    MailMessage mail = new MailMessage();

                    string[] multiEmails = ConfigurationManager.AppSettings["EmailRecipients_FailedSubmission"].ToString().Split(';');
                    foreach (string emailAddress in multiEmails)
                    {
                        mail.To.Add(new MailAddress(emailAddress));
                    }

                    mail.From = new MailAddress(ConfigurationManager.AppSettings["EmailSender"].ToString(), ConfigurationManager.AppSettings["EmailSenderName"].ToString());
                    mail.Body = "The Washpad app is unable to submit an estimate for container " + containerID + ". Suspect a failed gate in for this container. Please investiagate asap.\n\n----------------------------\nThis is an automated message, do not reply.";
                    mail.Subject = "Failed gatein - Washpad app unable to submit estimate for " + containerID;
                    try
                    {
                        smtpClient.Send(mail);
                    }
                    catch
                    {
                        // possibly could not reach the mail server - if not caught results in a hard failure
                        log.Error("error executing smtpClient.Send");
                    }
                    mail.Dispose();
                }
                else
                {
                    log.Warn("func submitEstimateForContainerToDepotPRO: " + "not bothering to send an alert for " + containerID + ", number of failures has exceeded annoyance threshold.");
                    Response.Write("<br>func submitEstimateForContainerToDepotPRO: " + "not bothering to send an alert for " + containerID + ", number of failures has exceeded annoyance threshold.");
                }

                // not proceeding with DepotPROsubmission, leave the function
                return false;
            } // end of non-submission
            // --------------------- FUNCTION EXITED HERE FOR CONTAINERS THAT NOT BE FOUND IN DEPOTPRO



            // -----------------------------------------------------------------------------------------------------------
            // ------------------------------------- 1) Compile core estimate(s) --------------------------------------------
            // -----------------------------------------------------------------------------------------------------------
            // Generate the estimate entry(s)
            // Note that MSK gets two estimate resords, the rest get one estimate
            int estimateId_default = 0;
            int estimateId_MSK_02 = 0;
            int estimateId_MSK_43 = 0;
            if (dtContainerFromDP.Rows[0]["OWNER"].ToString() == "MSK")
            {
                estimateId_MSK_02 = compileEstimate(containerID, "02", dtContainerFromDP.Rows[0], dtContainerState.Rows[0]); // Repairs Estimate - note that this is getting compiled even if no repairs, but not submitted
                estimateId_MSK_43 = compileEstimate(containerID, "43", dtContainerFromDP.Rows[0], dtContainerState.Rows[0]); // Services Estimate
            }
            else
            {
                estimateId_default = compileEstimate(containerID, null, dtContainerFromDP.Rows[0], dtContainerState.Rows[0]);
            }
            strWashDBStatusUpdateSQL = strWashDBStatusUpdateSQL + "taskInspect= '" + 40 + "', ";  //i.e. task has been completed and submitted to DepotPRO



            // -----------------------------------------------------------------------------------------------------------
            // ------------------------------ 2) Compile repair lines for estimate ---------------------------------------
            // -----------------------------------------------------------------------------------------------------------
            if (dtContainerFromDP.Rows[0]["OWNER"].ToString() == "MSK")
            {
                // MSK reefer repairs go to estimate with tariff code 02
                compileRepairLinesForEstimate(containerID, estimateId_MSK_02, dtContainerFromDP.Rows[0]);
            }
            else
            {
                // All other boxes to the standard estimate
                compileRepairLinesForEstimate(containerID, estimateId_default, dtContainerFromDP.Rows[0]);
            }



            // -----------------------------------------------------------------------------------------------------------
            // ------------------------------- 3) Compile services for estimate ------------------------------------------
            // -----------------------------------------------------------------------------------------------------------
            log.Info("func submitEstimateForContainerToDepotPRO: " + "compiling services for " + containerID + "...");
            try
            {
                if (dtContainerFromDP.Rows[0]["OWNER"].ToString() == "MSK")
                {
                    // MSK reefers get services against two estimates: repair service goes against 02, all other services (incl photos) against 43
                    strWashDBStatusUpdateSQL = strWashDBStatusUpdateSQL + compileServicesForEstimate(containerID, estimateId_MSK_43, "43", dtContainerState.Rows[0]);  // services estimate
                    strWashDBStatusUpdateSQL = strWashDBStatusUpdateSQL + compileServicesForEstimate(containerID, estimateId_MSK_02, "02", dtContainerState.Rows[0]);  // repairs estimate
                }
                else
                {
                    // All other boxes get all services against the one estimate
                    strWashDBStatusUpdateSQL = strWashDBStatusUpdateSQL + compileServicesForEstimate(containerID, estimateId_default, "default", dtContainerState.Rows[0]);
                }
            }
            catch (Exception e)
            {
                Response.Write("func submitEstimateForContainerToDepotPRO: " + "Error compiling services for " + containerID);
                log.Error("func submitEstimateForContainerToDepotPRO: " + "Error compiling services for " + containerID);
                log.Error("func submitEstimateForContainerToDepotPRO: " + e.Message);
            }


            // -----------------------------------------------------------------------------------------------------------
            // ----------------------------------------- 4) Compile photo lines ------------------------------------------
            // -----------------------------------------------------------------------------------------------------------

            DataTable dtContainerPhotos = new DataTable();
            string SQLInsertPhotoFragment = "";
            List<SqlParameter> cParameters = new List<SqlParameter>();
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                try
                {
                    _conn.Open();
                    string strSQL = "SELECT PHOTO, PHOTORESIZED FROM containerPhotos WHERE containerId = '" + containerID + "'";
                    SqlCommand cmd = new SqlCommand(strSQL, _conn);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        dtContainerPhotos.Load(reader);
                    }
                }
                catch (Exception ex)
                {
                    // TODO: return error
                    log.Error("func submitEstimateForContainerToDepotPRO: " + "Error reading from WashDB containerPhotos: " + ex.Message);
                }
            }

            if (dtContainerPhotos.Rows.Count > 0)   // If there are photo lines, build SQL and insert into cpEstimatePhotos
            {
                Response.Write("<br>" + dtContainerPhotos.Rows.Count + " photos found.");
                DataView dvContainerPhotos = new DataView(dtContainerPhotos);  // dataview for subsequent filtering
                int lineNo = 0;
                foreach (DataRowView rowView in dvContainerPhotos)
                {
                    DataRow photoRow = rowView.Row;
                    byte[] photoBytes = new byte[] { };
                    // if there's a resized photo, use it, else fall back to original photo

                    //byte[] photoBytes = (byte[])photoRow["PHOTORESIZED"];
                    if (photoRow["PHOTORESIZED"] != DBNull.Value)
                    {
                        photoBytes = (byte[])photoRow["PHOTORESIZED"];
                    }
                    else
                    {
                        photoBytes = (byte[])photoRow["PHOTO"];
                    }


                    SQLInsertPhotoFragment = SQLInsertPhotoFragment + "(";

                    if (dtContainerFromDP.Rows[0]["OWNER"].ToString() == "MSK")
                    {
                        // MSK reefers get services against two estimates: repair service goes against 02, all other services (incl photos) against 43
                        SQLInsertPhotoFragment = SQLInsertPhotoFragment + "" + estimateId_MSK_43 + ", "; // ESTIMATE
                    }
                    else
                    {
                        SQLInsertPhotoFragment = SQLInsertPhotoFragment + "" + estimateId_default + ", "; // ESTIMATE
                    }

                    SQLInsertPhotoFragment = SQLInsertPhotoFragment + "@photo" + lineNo.ToString(); // PHOTO - parameterised
                    SQLInsertPhotoFragment = SQLInsertPhotoFragment + "), ";
                    cParameters.Add(new SqlParameter("@photo" + lineNo.ToString(), @photoBytes));
                    lineNo++;
                }

                SQLInsertPhotoFragment = SQLInsertPhotoFragment.Substring(0, SQLInsertPhotoFragment.Length - 2);  // remove trailing comma
                SQLInsertPhotoFragment = "INSERT INTO cpEstimatePhotos (FK_ESTIMATE,PHOTO) VALUES " + SQLInsertPhotoFragment;
                Response.Write("<br>SQLInsertPhotoFragment: " + SQLInsertPhotoFragment);

                using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
                {
                    try
                    {
                        _conn.Open();
                        SqlCommand cmd = new SqlCommand(SQLInsertPhotoFragment, _conn);
                        // add photo data as parameters
                        if (cParameters.Count != 0)
                        {
                            cmd.Parameters.AddRange(cParameters.ToArray());
                        }
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        Response.Write("<br/>SQL PHOTO LINES INSERT executed.");
                    }
                    catch (Exception ex)
                    {
                        // TODO: log/alert/return error
                        log.Error("func submitEstimateForContainerToDepotPRO: " + "Error inserting photos into cpEstimatePhotos: " + ex.Message);
                        Response.Write("<br/>Error with SQL PHOTO LINE INSERT: " + ex.Message);
                    }
                } // End using SqlConnection
            }
            else
            {
                // No photos for this estimate
                Response.Write("<br>No photo lines for this estimate.");
                log.Info("No photo lines for this estimate.");
            }


            // -----------------------------------------------------------------------------------------------------------
            // ------------------------------- 6) Submit estimate via Web service (if gated in) --------------------------
            // -----------------------------------------------------------------------------------------------------------
            bool success = false;
            if (dtContainerFromDP.Rows[0]["OWNER"].ToString() == "MSK")
            {
                // Only submit MSK 02 estimate if there are repairs
                if (containerHasRepairs(containerID))
                {
                    success = submitEstimateViaWebService(estimateId_MSK_02); // MSK repairs
                }
                success = submitEstimateViaWebService(estimateId_MSK_43); // MSK services
            }
            else
            {
                success = submitEstimateViaWebService(estimateId_default); // Other shipping lines combines repairs and services
            }



            // -----------------------------------------------------------------------------------------------------------
            // -------------------------------- 7) Update task statuses in WashDB  ----------------------------------------
            // -----------------------------------------------------------------------------------------------------------
            // the SQL statement has been gradually compiled during previous steps in the function
            //strWashDBStatusUpdateSQL = strWashDBStatusUpdateSQL.Substring(0, strWashDBStatusUpdateSQL.Length - 2);  // remove trailing comma
            // and reset failureCount to 0 when submission is successful...
            strWashDBStatusUpdateSQL = strWashDBStatusUpdateSQL + " failedEstimateSubmissionCount = 0 WHERE containerId = '" + containerID + "'";

            if (success) // only update status in WashDB if the web service submission was successful.
            {
                using (SqlConnection _Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
                {
                    _Conn.Open();
                    SqlCommand cmd = new SqlCommand(strWashDBStatusUpdateSQL, _Conn);
                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            else
            {
                return false;
            }



        } // end submitEstimateForContainerToDepotPRO

        private bool resubmitEstimatesToDepotPro()
        {

            var context = HttpContext.Current;
            var log = log4net.LogManager.GetLogger(this.GetType());
            log.Info("func resubmitEstimatesToDepotPro: " + "Looking for failed submissions to DepotPRO...");

            DataTable dtUnsubmittedEstimates = new DataTable();
            DataView dvUnsubmittedEstimates = new DataView();

            // Get container from WashDB containerStates that have inspections NOT submitted to DepotPRO
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                try
                {
                    _conn.Open();
                    string strSQL = "SELECT * FROM containerStates WHERE taskInspect = '38'";  // submitted to Jade, but not to DepotPRO
                    SqlCommand cmd = new SqlCommand(strSQL, _conn);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        dtUnsubmittedEstimates.Load(reader);
                    }
                    dvUnsubmittedEstimates = new DataView(dtUnsubmittedEstimates);
                }
                catch (Exception ex)
                {
                    // TODO: return error
                    log.Error("func resubmitEstimatesToDepotPro: " + "Error reading from WashDB containerState: " + ex.Message);
                }
            }

            // For each unsubmitted container, attempt to submit the estimate, alert IT admin
            for (int i = 0; i < dvUnsubmittedEstimates.Count; i++)
            {

                Response.Write("FOUND AN UNSUBMITTED ESTIMATE " + dvUnsubmittedEstimates[i]["containerId"]);
                log.Info("func resubmitEstimatesToDepotPro: Found an unsubmitted estimate for " + dvUnsubmittedEstimates[i]["containerId"]);

                // Look for a pre-compiled estimate for this container
                DataTable dtCpEstimates = new DataTable();
                using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
                {
                    try
                    {
                        _conn.Open();
                        string strSQL = "SELECT * from cpEstimate WHERE CONTAINER_NO = '" + dvUnsubmittedEstimates[i]["containerId"] + "'";
                        SqlCommand cmd = new SqlCommand(strSQL, _conn);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            dtCpEstimates.Load(reader);
                        }
                    }
                    catch (Exception ex)
                    {
                        // TODO: return error
                        log.Error("func resubmitEstimatesToDepotPro: " + "Error reading from WashDB containerState: " + ex.Message);
                    }
                }

                if (dtCpEstimates.Rows.Count > 0)
                {
                    // There is a compiled estimate, so get its ID and resubmit it
                    // For each estimate that has been found resubmit it...
                    DataView dvCpEstimates = new DataView(dtCpEstimates);  // dataview for subsequent filtering
                    int lineNo = 0;
                    foreach (DataRowView rowView in dvCpEstimates)
                    {
                        lineNo++;
                        DataRow estimateRow = rowView.Row;
                        log.Info("func resubmitEstimatesToDepotPro: pre-compiled estimate located for " + dvUnsubmittedEstimates[i]["containerId"] + ". PK_ESTIMATE is " + estimateRow["PK_ESTIMATE"]);
                        Response.Write("func resubmitEstimatesToDepotPro: pre-compiled estimate located for " + dvUnsubmittedEstimates[i]["containerId"] + ". PK_ESTIMATE is " + estimateRow["PK_ESTIMATE"]);
                        bool success = submitEstimateViaWebService(Convert.ToInt32(estimateRow["PK_ESTIMATE"]));
                        if (success)
                        {
                            //  update the containerState record, set taskInspect to 40
                            using (SqlConnection _Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
                            {
                                _Conn.Open();
                                SqlCommand cmd = new SqlCommand("UPDATE containerStates SET taskInspect='40' WHERE containerId = '" + dvUnsubmittedEstimates[i]["containerId"] + "'", _Conn);
                                cmd.ExecuteNonQuery();
                            }
                            return true;
                        }
                        else
                        {
                            // resubmit failed so don't update the containerState record
                            // perhaps alert someone...
                            log.Error("func resubmitEstimatesToDepotPro: Failed to resubmit estimate for " + dvUnsubmittedEstimates[i]["containerId"] + ". PK_ESTIMATE is " + estimateRow["PK_ESTIMATE"]);

                        }

                    }
                }
                else
                {
                    // there is no compiled estimate, so run it through estimate compilation/submission
                    log.Info("func resubmitEstimatesToDepotPro: there is no pre-compiled estimate for " + dvUnsubmittedEstimates[i]["containerId"]);
                    Response.Write("func resubmitEstimatesToDepotPro: there is no pre-compiled estimate for " + dvUnsubmittedEstimates[i]["containerId"]);
                    bool success = submitEstimateForContainerToDepotPRO(dvUnsubmittedEstimates[i]["containerId"].ToString());
                    if (success)
                    {
                        //  update the containerState record, set taskInspect to 40
                        using (SqlConnection _Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
                        {
                            _Conn.Open();
                            SqlCommand cmd = new SqlCommand("UPDATE containerStates SET taskInspect='40' WHERE containerId = '" + dvUnsubmittedEstimates[i]["containerId"] + "'", _Conn);
                            cmd.ExecuteNonQuery();
                        }
                        return true;
                    }
                    else
                    {
                        // resubmit failed so don't update the containerState record
                        // perhaps alert someone...
                        log.Error("func resubmitEstimatesToDepotPro: Failed to resubmit estimate for " + dvUnsubmittedEstimates[i]["containerId"] + ".");

                    }
                }

            }


            return false;
        } // end resubmitEstimateToDepotPro

        private void submitTaskCompletionsToJade()
        {
            // ----------------------------------------------------------------------------------------------------
            // ------------------------- Submission to Jade of Completed tasks, and set grade ---------------------
            // ----------------------------------------------------------------------------------------------------
            var log = log4net.LogManager.GetLogger(this.GetType());

            DataTable dtContainersWithTaskCompletions = new DataTable();
            DataView dvContainersWithTaskCompletions = new DataView();
            List<string> listTaskCompletionLines = new List<string>();
            string strSQL = "SELECT * FROM containerStates WHERE taskWash = '30' OR taskBaseWash = '30' OR taskRoofWash = '30' OR taskExternalWash = '30' OR taskPhoto = '30' OR taskRepair = '30' OR taskSteamClean = '30' OR taskSteamCleanFQ = '30'";
            using (SqlConnection _Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                _Conn.Open();
                SqlCommand cmd = new SqlCommand(strSQL, _Conn);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    dtContainersWithTaskCompletions.Load(reader);
                }
                dvContainersWithTaskCompletions = new DataView(dtContainersWithTaskCompletions);
            }


            if (dvContainersWithTaskCompletions.Count > 0)
            {
                log.Info("backgroundprocessor.aspx: " + "Found " + dvContainersWithTaskCompletions.Count.ToString() + " containers with completed tasks to submit.");

                for (int i = 0; i < dvContainersWithTaskCompletions.Count; i++)
                {
                    log.Info("backgroundprocessor.aspx: " + "Processing task completion and status updates for " + dvContainersWithTaskCompletions[i]["containerId"]);

                    // Compile SQL to update status for this container
                    strSQL = "UPDATE containerStates SET ";
                    if (dvContainersWithTaskCompletions[i]["taskWash"].ToString() == "30")
                    {
                        strSQL = strSQL + "taskWash= '" + 40 + "', ";  //i.e. open task has been submitted to source system
                        listTaskCompletionLines.Add(dvContainersWithTaskCompletions[i]["containerId"] + ",ISO Container,WASH,Update,Yes");
                    }
                    if (dvContainersWithTaskCompletions[i]["taskBaseWash"].ToString() == "30")
                    {
                        strSQL = strSQL + "taskBaseWash= '" + 40 + "', ";  //i.e. open task has been submitted to source system
                        listTaskCompletionLines.Add(dvContainersWithTaskCompletions[i]["containerId"] + ",ISO Container,BASE WASH (pol),Update,Yes");
                    }
                    if (dvContainersWithTaskCompletions[i]["taskRoofWash"].ToString() == "30")
                    {
                        strSQL = strSQL + "taskRoofWash= '" + 40 + "', ";  //i.e. open task has been submitted to source system
                        listTaskCompletionLines.Add(dvContainersWithTaskCompletions[i]["containerId"] + ",ISO Container,Roof Wash,Update,Yes");
                    }
                    if (dvContainersWithTaskCompletions[i]["taskExternalWash"].ToString() == "30")
                    {
                        strSQL = strSQL + "taskExternalWash= '" + 40 + "', ";  //i.e. open task has been submitted to source system
                        listTaskCompletionLines.Add(dvContainersWithTaskCompletions[i]["containerId"] + ",ISO Container,EXTCLEAN,Update,Yes");
                    }
                    if (dvContainersWithTaskCompletions[i]["taskSteamClean"].ToString() == "30")
                    {
                        strSQL = strSQL + "taskSteamClean= '" + 40 + "', ";  //i.e. open task has been submitted to source system
                        listTaskCompletionLines.Add(dvContainersWithTaskCompletions[i]["containerId"] + ",ISO Container,STEAM.C,Update,Yes");
                    }
                    if (dvContainersWithTaskCompletions[i]["taskSteamCleanFQ"].ToString() == "30")
                    {
                        strSQL = strSQL + "taskSteamCleanFQ= '" + 40 + "', ";  //i.e. open task has been submitted to source system
                        listTaskCompletionLines.Add(dvContainersWithTaskCompletions[i]["containerId"] + ",ISO Container,STEAM.C FQ,Update,Yes");
                    }
                    if (dvContainersWithTaskCompletions[i]["taskRepair"].ToString() == "30")
                    {
                        strSQL = strSQL + "taskRepair= '" + 40 + "', ";  //i.e. open task has been submitted to source system
                        listTaskCompletionLines.Add(dvContainersWithTaskCompletions[i]["containerId"] + ",ISO Container,Repair,Update,Yes");
                    }
                    if (dvContainersWithTaskCompletions[i]["taskPhoto"].ToString() == "30")
                    {
                        strSQL = strSQL + "taskPhoto= '" + 40 + "', ";  //i.e. goes straight to a completed task, submitted to source system
                        listTaskCompletionLines.Add(dvContainersWithTaskCompletions[i]["containerId"] + ",ISO Container,Photo,Update,Yes");
                    }

                    strSQL = strSQL.Substring(0, strSQL.Length - 2);  // remove trailing comma
                    strSQL = strSQL + " WHERE containerId = '" + dvContainersWithTaskCompletions[i]["containerId"] + "'";
                    log.Debug("backgroundprocessor.aspx: " + "strSQL: " + strSQL);
                    log.Debug("backgroundprocessor.aspx: " + "Number of completed tasks: " + listTaskCompletionLines.Count.ToString());

                    // Prepare to email
                    SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["EmailServer"].ToString(), 25);
                    smtpClient.EnableSsl = false;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                    // Generate CSV file for TODOTASK Completion
                    string CSVPathTodoTaskCompletion = ConfigurationManager.AppSettings["TemporaryFileStore"].ToString() + dvContainersWithTaskCompletions[i]["containerId"] + "_" + "TODOTASKUPDATE_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                    using (StreamWriter sw = File.CreateText(CSVPathTodoTaskCompletion))
                    {
                        sw.WriteLine("WASHPAD_TODOTASKUPDATE");
                        sw.WriteLine("ID,Cargo Type Descr,TASK CODE,ACTION,IS COMPLETE");
                        listTaskCompletionLines.ForEach(delegate(String line)
                        {
                            sw.WriteLine(line);  // Write a line for each task update required in Jade, including completing the Inspection task
                        });
                        log.Info("backgroundprocessor.aspx: " + "CSV File generated: " + CSVPathTodoTaskCompletion);
                    }

                    // Email todo task completion EDI CSV to Jade
                    MailMessage mail = new MailMessage();
                    mail.To.Add(new MailAddress(ConfigurationManager.AppSettings["EmailRecipient_JadeEDI"].ToString()));
                    mail.From = new MailAddress(ConfigurationManager.AppSettings["EmailSender"].ToString(), ConfigurationManager.AppSettings["EmailSenderName"].ToString());
                    mail.Body = "Todo task completion from washpad for " + dvContainersWithTaskCompletions[i]["containerId"];
                    mail.Subject = "Todo task completion from washpad for " + dvContainersWithTaskCompletions[i]["containerId"];
                    mail.Attachments.Add(new Attachment(CSVPathTodoTaskCompletion));
                    try
                    {
                        smtpClient.Send(mail);
                    }
                    catch
                    {
                        // possibly could not reach the mail server - if not caught results in a hard failure
                        log.Error("error executing smtpClient.Send");
                    }
                    mail.Dispose();
                    log.Info("backgroundprocessor.aspx: " + "Email with todo task completion EDI CSV sent to Jade for " + dvContainersWithTaskCompletions[i]["containerId"]);


                    // Generate CSV file to SET GRADE, only if box is DRY
                    if (dvContainersWithTaskCompletions[i]["isReefer"].ToString() == "0")
                    {
                        string CSVPathSetGrade = ConfigurationManager.AppSettings["TemporaryFileStore"].ToString() + dvContainersWithTaskCompletions[i]["containerId"] + "_" + "SETGRADE_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                        using (StreamWriter sw = File.CreateText(CSVPathSetGrade))
                        {
                            sw.WriteLine("WASHPADAPP_SETGRADE");
                            sw.WriteLine("ID,Cargo Type Descr,Field Name,Field Value");
                            sw.WriteLine(dvContainersWithTaskCompletions[i]["containerId"] + ",ISO Container,availabilityGrade," + dvContainersWithTaskCompletions[i]["grade"]);  // Write a line for each task update required in Jade, including completing the Inspection task
                            log.Info("backgroundprocessor.aspx: " + "CSV File generated: " + CSVPathSetGrade);
                        }

                        // Email grade setting EDI CSV to Jade
                        mail = new MailMessage();
                        mail.To.Add(new MailAddress(ConfigurationManager.AppSettings["EmailRecipient_JadeEDI"].ToString()));
                        mail.From = new MailAddress(ConfigurationManager.AppSettings["EmailSender"].ToString(), ConfigurationManager.AppSettings["EmailSenderName"].ToString());
                        mail.Body = "Set grade from washpad for " + dvContainersWithTaskCompletions[i]["containerId"];
                        mail.Subject = "Set grade completion from washpad for " + dvContainersWithTaskCompletions[i]["containerId"];
                        mail.Attachments.Add(new Attachment(CSVPathSetGrade));
                        try
                        {
                            smtpClient.Send(mail);
                        }
                        catch
                        {
                            // possibly could not reach the mail server - if not caught results in a hard failure
                            log.Error("error executing smtpClient.Send");
                        }
                        mail.Dispose();
                        log.Info("backgroundprocessor.aspx: " + "Email to set grade EDI CSV sent to Jade for " + dvContainersWithTaskCompletions[i]["containerId"]);
                    }


                    // Execute SQL for updating task status in WashDB
                    using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
                    {
                        _conn.Open();
                        SqlCommand cmd = new SqlCommand(strSQL, _conn);
                        cmd.ExecuteNonQuery();
                    }

                    // Return
                    log.Info("backgroundprocessor.aspx: " + "Finished updating task completion statuses for container " + dvContainersWithTaskCompletions[i]["containerId"]);
                    Response.Write("Updated task completion statuses for container " + dvContainersWithTaskCompletions[i]["containerId"] + ".");

                } // end loop through containers with tasks to complete
            }
            else
            {
                // No containers found with completed tasks
                log.Info("backgroundprocessor.aspx: " + "No containers with completed tasks found, no action taken.");
            }
        } // end submitTaskCompletionsToJade 

        private void processCompletedInspections()
        {
            var log = log4net.LogManager.GetLogger(this.GetType());

            DataView dv = new DataView();
            DataTable dt = new DataTable();

            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                _conn.Open();
                //string strSQL = "SELECT * FROM containerStates WHERE taskInspect = '30' OR taskInspect = '38' "; // 38 is inspected but not submitted to DP
                string strSQL = "SELECT * FROM containerStates WHERE taskInspect = '30'"; // 38 is inspected but not submitted to DP

                SqlCommand cmd = new SqlCommand(strSQL, _conn);

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }

                dv = new DataView(dt);
                log.Info("backgroundprocessor.aspx: " + "Found " + dv.Count.ToString() + " containers with completed inspections to submit.");
            }


            // For each inspected container: Submit an estimate to DepotPRO, then send any new tasks to Jade and complete Inspection in Jade
            for (int i = 0; i < dv.Count; i++)
            {
                log.Info("backgroundprocessor.aspx: " + "Processing task creation status updates for container " + dv[i]["containerId"]);
                //listTaskCreationLines.Clear();  // clear out lines as
                List<string> listTaskCreationLines = new List<string>();

                // Submit estimate to DepotPRO
                bool successfulDepotPROSubmission = submitEstimateForContainerToDepotPRO(dv[i]["containerId"].ToString());

                // note that successful submitEstimateForContainerToDepotPRO will have set taskInspect to 40 (completed)
                // however unsuccessful submission will not have altered any task statuses

                string strSQLJadeTaskUpdate = "UPDATE containerStates SET ";
                // Compile EDI lines for Jade EDI
                if (dv[i]["taskWash"].ToString() == "10")
                {
                    listTaskCreationLines.Add(dv[i]["containerId"] + ",ISO Container,WASH,Add,No");
                    strSQLJadeTaskUpdate = strSQLJadeTaskUpdate + ((successfulDepotPROSubmission) ? "taskWash = 22, " : "taskWash = 21, ");
                }
                if (dv[i]["taskBaseWash"].ToString() == "10")
                {
                    listTaskCreationLines.Add(dv[i]["containerId"] + ",ISO Container,BASE WASH (pol),Add,No");
                    strSQLJadeTaskUpdate = strSQLJadeTaskUpdate + ((successfulDepotPROSubmission) ? "taskBaseWash = 22, " : "taskBaseWash = 21, ");
                }
                if (dv[i]["taskRoofWash"].ToString() == "10")
                {
                    listTaskCreationLines.Add(dv[i]["containerId"] + ",ISO Container,Roof Wash,Add,No");
                    strSQLJadeTaskUpdate = strSQLJadeTaskUpdate + ((successfulDepotPROSubmission) ? "taskRoofWash = 22, " : "taskRoofWash = 21, ");
                }
                if (dv[i]["taskExternalWash"].ToString() == "10")
                {
                    listTaskCreationLines.Add(dv[i]["containerId"] + ",ISO Container,EXTCLEAN,Add,No");
                    strSQLJadeTaskUpdate = strSQLJadeTaskUpdate + ((successfulDepotPROSubmission) ? "taskExternalWash = 22, " : "taskExternalWash = 21, ");
                }
                if (dv[i]["taskSteamClean"].ToString() == "10")
                {
                    listTaskCreationLines.Add(dv[i]["containerId"] + ",ISO Container,STEAM.C,Add,No");
                    strSQLJadeTaskUpdate = strSQLJadeTaskUpdate + ((successfulDepotPROSubmission) ? "taskSteamClean = 22, " : "taskSteamClean = 21, ");
                }
                if (dv[i]["taskSteamCleanFQ"].ToString() == "10")
                {
                    listTaskCreationLines.Add(dv[i]["containerId"] + ",ISO Container,STEAM.C FQ,Add,No");
                    strSQLJadeTaskUpdate = strSQLJadeTaskUpdate + ((successfulDepotPROSubmission) ? "taskSteamCleanFQ = 22, " : "taskSteamCleanFQ = 21, ");
                }
                if (dv[i]["taskRepair"].ToString() == "10")
                {
                    listTaskCreationLines.Add(dv[i]["containerId"] + ",ISO Container,Repair,Add,No");
                    strSQLJadeTaskUpdate = strSQLJadeTaskUpdate + ((successfulDepotPROSubmission) ? "taskRepair = 22, " : "taskRepair = 21, ");
                }

                if (dv[i]["taskPhoto"].ToString() == "10" && containerHasPhotos(dv[i]["containerId"].ToString())) // photo task added on app
                {
                    listTaskCreationLines.Add(dv[i]["containerId"] + ",ISO Container,Photo,Add,No");   // Add the photo task
                    listTaskCreationLines.Add(dv[i]["containerId"] + ",ISO Container,Photo,Update,Yes");   // Complete the photo task
                    strSQLJadeTaskUpdate = strSQLJadeTaskUpdate + ((successfulDepotPROSubmission) ? "taskPhoto = 40, " : "taskPhoto = 38, "); // Edited 10.4.2017 to set as 40 or 38, instead of 22 or 21!
                }

                if (dv[i]["taskPhoto"].ToString() == "23" && containerHasPhotos(dv[i]["containerId"].ToString())) // photo task submitted to Jade, and photos taken
                {
                    listTaskCreationLines.Add(dv[i]["containerId"] + ",ISO Container,Photo,Update,Yes");   // Complete the photo task
                    strSQLJadeTaskUpdate = strSQLJadeTaskUpdate + ((successfulDepotPROSubmission) ? "taskPhoto = 40, " : "taskPhoto = 38, ");
                }


                listTaskCreationLines.Add(dv[i]["containerId"] + ",ISO Container,REQ INSP,Update,Yes"); //  Complete the inspection... no means for creation on app

                strSQLJadeTaskUpdate = strSQLJadeTaskUpdate + "completedInspection = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', ";
                strSQLJadeTaskUpdate = strSQLJadeTaskUpdate + ((successfulDepotPROSubmission) ? "taskInspect = 40 " : "taskInspect = 38 ");
                strSQLJadeTaskUpdate = strSQLJadeTaskUpdate + "WHERE containerID = '" + dv[i]["containerId"] + "'";

                // ------------------------------------------------------------------------------------
                // ---------------------  Submit task creation and completion to Jade -----------------

                // Prepare to email
                SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["EmailServer"].ToString(), 25);
                smtpClient.EnableSsl = false;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                // Generate CSV file for TODOTASKUPDATES
                string CSVPathTodoTaskCreation = ConfigurationManager.AppSettings["TemporaryFileStore"].ToString() + dv[i]["containerId"] + "_" + "TODOTASKUPDATE_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                using (StreamWriter sw = File.CreateText(CSVPathTodoTaskCreation))
                {
                    sw.WriteLine("WASHPAD_TODOTASKUPDATE");
                    sw.WriteLine("ID,Cargo Type Descr,TASK CODE,ACTION,IS COMPLETE");
                    listTaskCreationLines.ForEach(delegate(String line)
                    {
                        sw.WriteLine(line);  // Write a line for each task update required in Jade, including completing the Inspection task
                    });
                    log.Info("backgroundprocessor.aspx: " + "CSV File generated: " + CSVPathTodoTaskCreation);
                }

                // Sent email to Jade with attachment for creating tasks
                MailMessage mail = new MailMessage();
                mail.To.Add(new MailAddress(ConfigurationManager.AppSettings["EmailRecipient_JadeEDI"].ToString()));
                mail.From = new MailAddress(ConfigurationManager.AppSettings["EmailSender"].ToString(), ConfigurationManager.AppSettings["EmailSenderName"].ToString());
                mail.Body = "Todo task update from washpad for " + dv[i]["containerId"];
                mail.Subject = "Todo task update from washpad for " + dv[i]["containerId"];
                mail.Attachments.Add(new Attachment(CSVPathTodoTaskCreation));
                try
                {
                    smtpClient.Send(mail);
                }
                catch
                {
                    // possibly could not reach the mail server - if not caught results in a hard failure
                    log.Error("error executing smtpClient.Send");
                }
                mail.Dispose();
                log.Info("backgroundprocessor.aspx: " + "CSV File emailed to Jade: " + CSVPathTodoTaskCreation);

                // Compile remarks
                string compiledRemarks = "";

                // Alliance remark must go first so its easy to read in jade...
                if (Convert.ToInt32(dv[i]["qualityAlliance"]) > 0)
                {
                    compiledRemarks = compiledRemarks + "FOR ALLIANCE. ";
                }
                else
                {
                    compiledRemarks = compiledRemarks + "NON ALLIANCE. ";
                }
                // If there are remarks, or if Oxidised, add remarks in Jade
                if (Convert.ToInt32(dv[i]["qualityOxidised"]) > 0)
                {
                    compiledRemarks = compiledRemarks + "Oxidisation noted on washpad. ";
                }

                // lastly add any remarks
                compiledRemarks = compiledRemarks + dv[i]["remarks"];


                if (compiledRemarks != "")
                {
                    // There is a remark, so generate CSV and submit to Jade
                    string CSVPathComments = ConfigurationManager.AppSettings["TemporaryFileStore"].ToString() + dv[i]["containerId"] + "_" + "REMARKS_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
                    using (StreamWriter sw = File.CreateText(CSVPathComments))
                    {
                        sw.WriteLine("WASHPADAPP_REMARKS");
                        sw.WriteLine("ID,Cargo Type Descr,Field Name,Field Value");
                        sw.WriteLine(dv[i]["containerId"] + ",ISO Container,remarks," + compiledRemarks);
                        log.Info("backgroundprocessor.aspx: " + "CSV File generated: " + CSVPathComments);
                    }
                    // Sent email to Jade with attachment for adding remarks
                    MailMessage mail1 = new MailMessage();
                    mail1.To.Add(new MailAddress(ConfigurationManager.AppSettings["EmailRecipient_JadeEDI"].ToString()));
                    mail1.From = new MailAddress(ConfigurationManager.AppSettings["EmailSender"].ToString(), ConfigurationManager.AppSettings["EmailSenderName"].ToString());
                    mail1.Body = "Remarks from washpad for " + dv[i]["containerId"];
                    mail1.Subject = "Remarks from washpad for " + dv[i]["containerId"];
                    mail1.Attachments.Add(new Attachment(CSVPathComments));
                    try
                    {
                        smtpClient.Send(mail1);
                    }
                    catch
                    {
                        // possibly could not reach the mail server - if not caught results in a hard failure
                        log.Error("error executing smtpClient.Send");
                    }
                    mail1.Dispose();
                    log.Info("backgroundprocessor.aspx: " + "CSV File emailed to Jade: " + CSVPathComments);
                }

                // Execute SQL for updating task status in WashDB to reflect submissions to Jade
                using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
                {
                    _conn.Open();
                    SqlCommand cmd = new SqlCommand(strSQLJadeTaskUpdate, _conn);
                    cmd.ExecuteNonQuery();
                    log.Info("backgroundprocessor.aspx: " + "strSQLJadeTaskUpdate: " + strSQLJadeTaskUpdate);
                }

                // Send email alert on oxidised boxes if required
                if (Convert.ToInt32(dv[i]["qualityOxidised"]) > 0)
                {
                    string strOxAlertBody = "";
                    strOxAlertBody = strOxAlertBody + "Container " + dv[i]["containerId"] + " has had oxidisation identified at the washpad.\n";
                    if (dv[i]["remarks"].ToString() != "")
                    {
                        strOxAlertBody = strOxAlertBody + "Remarks: " + dv[i]["remarks"] + "\n";
                    }
                    strOxAlertBody = strOxAlertBody + "\n\n\n---------------------------------------------\n";
                    strOxAlertBody = strOxAlertBody + "This message was generated by the Washpad Tablet App, do not reply to it";

                    MailMessage mail4 = new MailMessage();
                    string[] multiEmails = ConfigurationManager.AppSettings["EmailRecipients_OxidisationAlert"].ToString().Split(';');
                    foreach (string emailAddress in multiEmails)
                    {
                        mail4.To.Add(new MailAddress(emailAddress));
                    }
                    mail4.From = new MailAddress(ConfigurationManager.AppSettings["EmailSender"].ToString(), ConfigurationManager.AppSettings["EmailSenderName"].ToString());
                    mail4.Subject = "Oxidised box identified on washpad: " + dv[i]["containerId"];
                    mail4.Body = strOxAlertBody;
                    try
                    {
                        smtpClient.Send(mail4);
                    }
                    catch
                    {
                        // possibly could not reach the mail server - if not caught results in a hard failure
                        log.Error("error executing smtpClient.Send");
                    }
                    mail4.Dispose();
                    log.Info("backgroundprocessor.aspx: " + "Email alert re container oxidisation sent.");
                }

                // Send email alert if the inspection was incomplete
                if (dv[i]["quoteStatus"].ToString() == "incomplete")
                {
                    string strOxAlertBody = "";
                    strOxAlertBody = strOxAlertBody + "Container " + dv[i]["containerId"] + " not fully inspected at washpad.\n";
                    if (dv[i]["remarks"].ToString() != "")
                    {
                        strOxAlertBody = strOxAlertBody + "The container was marked as an INCOMPLETE inspection.  Remarks: " + dv[i]["remarks"] + "\n";
                    }
                    strOxAlertBody = strOxAlertBody + "\n\n\n---------------------------------------------\n";
                    strOxAlertBody = strOxAlertBody + "This message was generated by the Washpad Tablet App, do not reply to it";

                    MailMessage mail5 = new MailMessage();
                    string[] multiEmails = ConfigurationManager.AppSettings["EmailRecipients_IncompleteInspection"].ToString().Split(';');
                    foreach (string emailAddress in multiEmails)
                    {
                        mail5.To.Add(new MailAddress(emailAddress));
                    }
                    mail5.From = new MailAddress(ConfigurationManager.AppSettings["EmailSender"].ToString(), ConfigurationManager.AppSettings["EmailSenderName"].ToString());
                    mail5.Subject = "Box with incomplete inspection on washpad: " + dv[i]["containerId"];
                    mail5.Body = strOxAlertBody;
                    try
                    {
                        smtpClient.Send(mail5);
                    }
                    catch
                    {
                        // possibly could not reach the mail server - if not caught results in a hard failure
                        log.Error("error executing smtpClient.Send");
                    }
                    mail5.Dispose();
                    log.Info("backgroundprocessor.aspx: " + "Email alert re incomplete inspection sent.");
                }


                // ------------------ END Submit task creation and completion to Jade -----------------
                // ------------------------------------------------------------------------------------
                log.Info("backgroundprocessor.aspx: " + "Finished estimate submission and task creation for container " + dv[i]["containerId"]);
            }

        } // end processCompletedInspections



        private void remedialResubmissionToDeportPROOnly()
        {
            // FUNCTION FOR REMEDIAL RESUBMISSION OF COMPLETED ESTIMATES TO DEPOTPRO
            var log = log4net.LogManager.GetLogger(this.GetType());
            DataView dv = new DataView();
            DataTable dt = new DataTable();
            Response.Write("\n\n===================================================================================\n");
            Response.Write("backgroundprocessor.aspx: " + "RRTDP: Remedial Resubmission to DepotPRO Envoked" + "\n");

            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                _conn.Open();
                string strSQL = "SELECT * FROM containerStates WHERE taskInspect = '42'"; // 42 is a temporary flag for resubmission

                SqlCommand cmd = new SqlCommand(strSQL, _conn);

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }

                dv = new DataView(dt);
                log.Info("backgroundprocessor.aspx: " + "RRTDP: Found " + dv.Count.ToString() + " containers flagged for resubmission.");
                Response.Write("backgroundprocessor.aspx: " + "RRTDP: Found " + dv.Count.ToString() + " containers flagged for resubmission." + "\n");
            }

            // For each inspected container: Submit an estimate to DepotPRO, then send any new tasks to Jade and complete Inspection in Jade
            for (int i = 0; i < dv.Count; i++)
            {
                log.Info("backgroundprocessor.aspx: " + "RRTDP: Processing task creation status updates for container " + dv[i]["containerId"]);
                Response.Write("backgroundprocessor.aspx: " + "RRTDP: Processing task creation status updates for container " + dv[i]["containerId"] + "\n");

                // Submit estimate to DepotPRO
                bool successfulDepotPROSubmission = submitEstimateForContainerToDepotPRO(dv[i]["containerId"].ToString());  // TODO CHECK THIS FUNCTION

                // note that successful submitEstimateForContainerToDepotPRO will have set taskInspect to 40 (completed)
                if (successfulDepotPROSubmission)
                {

                    log.Info("backgroundprocessor.aspx: " + "RRTDP: Successful submission of estimate for container " + dv[i]["containerId"]);
                    Response.Write("backgroundprocessor.aspx: " + "RRTDP: Successful submission of estimate for container " + dv[i]["containerId"] + "\n");
                }
                else
                {
                    log.Error("backgroundprocessor.aspx: " + "RRTDP: Failed submission of estimate for container " + dv[i]["containerId"]);
                    Response.Write("backgroundprocessor.aspx: " + "RRTDP: Failed submission of estimate for container " + dv[i]["containerId"] + "\n");
                }

                // ------------------ END Submit task creation and completion to Jade -----------------
                // ------------------------------------------------------------------------------------
                log.Info("backgroundprocessor.aspx: " + "RRTDP: Finished estimate resubmission to DepotPRO for container " + dv[i]["containerId"]);
                Response.Write("backgroundprocessor.aspx: " + "RRTDP: Finished estimate resubmission to DepotPRO for container " + dv[i]["containerId"] + "\n");
            }

            Response.Write("===================================================================================\n");
        } // end remedialResubmissionToDeportPROOnly



        protected void Page_Load(object sender, EventArgs e)
        {
            var log = log4net.LogManager.GetLogger(this.GetType());
            log.Info("backgroundprocessor.aspx: " + "Background Processor has been called.");

           // processCompletedInspections();
           // resubmitEstimatesToDepotPro();
          //  submitTaskCompletionsToJade();

            // Manual remedial resubmission July 2017
            var context = HttpContext.Current;
            if (context.Request["remedialResubmission"] == "yes")
            {
                remedialResubmissionToDeportPROOnly();
            }


            log.Info("backgroundprocessor.aspx: " + "Background Processor has finished.");
        } // end page load
    }
}