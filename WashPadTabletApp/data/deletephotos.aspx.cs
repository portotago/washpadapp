﻿// Note:  This program requires the download and installation of log4Net
// 1. install the NuGet package manager (Tools>>Extension Manager>>Online Gallery>>NuGet Package Manager
// 2. right-click the project and choose Manage NuGet Packages
// 3. search for log4Net and install log4Net

using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace WashPadTabletApp.data
{
    public partial class deletephotos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // get form submission
            var context = HttpContext.Current;
            var log = log4net.LogManager.GetLogger(this.GetType());

            if (context.Request["photosToDelete"] != "")
            {
                log.Info("deletephotos.aspx: " + "Deleting the following photos from WashDB: " + context.Request["photosToDelete"].ToString());

                try
                {
                    if (context.Request.Params.Keys.Count > 0)
                    {
                        // Delete this photo from DB
                        using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
                        {
                            dbConnection.Open();
                            // TODO: determine estimate number that this photo is going against...
                            // TODO: stored proc for the following:
                            SqlCommand command = new SqlCommand("DELETE FROM containerPhotos WHERE PHOTOID IN (" + context.Request["photosToDelete"].ToString() + ")", dbConnection);
                            command.ExecuteNonQuery();
                            Response.Write("Photo with IDs " + context.Request["photosToDelete"].ToString() + " removed from DB.");
                            dbConnection.Close();
                        }
                    }
                    else
                    {
                        Response.Write("No photos found to delete.");
                        return;
                    }
                }
                catch (Exception ex)
                {
                    log.Error("deletephotos.aspx: error message: " + ex.Message);
                    Response.Write("An error occurred while uploading the file. Error Message: " + ex.Message);
                    return;
                }
            }
            else {
                log.Info("deletephotos.aspx: Called, but no photos to delete.");
            }

        } // End Page Load
    }
}