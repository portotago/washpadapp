﻿// Note:  This program requires the download and installation of log4Net
// 1. install the NuGet package manager (Tools>>Extension Manager>>Online Gallery>>NuGet Package Manager
// 2. right-click the project and choose Manage NuGet Packages
// 3. search for log4Net and install log4Net

using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace WashPadTabletApp.data
{
    public partial class deleterepairlines : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // get form submission
            var context = HttpContext.Current;
            var log = log4net.LogManager.GetLogger(this.GetType());

            if (context.Request["repairLinesToDelete"] != "")
            {
                 // there are repairs to delete
                log.Info("deleterepairlines.aspx: " + "Deleting the following repair lines from WashDB: " + context.Request["repairLinesToDelete"]);
                log.Debug("deleterepairlines.aspx: " + "Called by: " + context.Request.UrlReferrer.ToString());

                try
                {
                    if (context.Request.Params.Keys.Count > 0)
                    {
                        // Delete this photo from DB
                        using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
                        {
                            dbConnection.Open();
                            // TODO: determine estimate number that this photo is going against...
                            // TODO: stored proc for the following:
                            SqlCommand command = new SqlCommand("DELETE FROM containerRepairs WHERE containerRepairID IN (" + context.Request["repairLinesToDelete"].ToString() + ")", dbConnection);
                            log.Debug("deleterepairs.aspx: SQL = DELETE FROM containerRepairs WHERE containerRepairID IN (" + context.Request["repairLinesToDelete"].ToString() + ")");
                            command.ExecuteNonQuery();
                            Response.Write("Repair lines with IDs " + context.Request["repairLinesToDelete"].ToString() + " removed from DB.");
                            dbConnection.Close();
                        }
                    }
                    else
                    {
                        Response.Write("No repair lines found to delete.");
                        return;
                    }
                }
                catch (Exception ex)
                {
                    log.Error("deleterepairs.aspx: error message: " + ex.Message);
                    Response.Write("An error occurred while deleting repair lines. Error Message: " + ex.Message);
                    return;
                }

            }
            else { 
                // no repairs to delete
                log.Info("deleterepairlines.aspx: " + "Called, but no repairs to delete.");
            }


        } // End Page_Load
    }
}