﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Data.SqlClient;
using System.Data;

public class Container2
{
    public string id;
    public string oid;
    public string operatorCode;
    public string isReefer;
    public string Location;
    public DateTime WentToWash;
    public string RecTransportMode;
    public string is40ftLong;
    public string RecMT;
    public string taskWash;
    public string taskBaseWash;
    public string taskRoofWash;
    public string taskExternalWash;
    public string taskSteamClean;
    public string taskSteamCleanFQ;
    public string taskPhoto;
    public string taskRepair;
    public string repairCount;
    public string qualityOxidised;
    public string qualityAlliance;
    public string remarks;
    public string quoteStatus;
    public string taskMoveToRepair;

    public Container2
    (
        string id,
        string oid,
        string operatorCode,
        string isReefer,
        string Location,
        DateTime WentToWash,
        string RecTransportMode,
        string is40ftLong,
        string RecMT,
        string taskWash,
        string taskBaseWash,
        string taskRoofWash,
        string taskExternalWash,
        string taskSteamClean,
        string taskSteamCleanFQ,
        string taskPhoto,
        string taskRepair,
        string repairCount,
        string qualityOxidised,
        string qualityAlliance,
        string remarks,
        string quoteStatus,
        string taskMoveToRepair
    )
    {
        this.id = id;
        this.oid = oid;
        this.operatorCode = operatorCode;
        this.isReefer = isReefer;
        this.Location = Location;
        this.WentToWash = WentToWash;
        this.RecTransportMode = RecTransportMode;
        this.is40ftLong = is40ftLong;
        this.RecMT = RecMT;
        this.taskWash = taskWash;
        this.taskBaseWash = taskBaseWash;
        this.taskRoofWash = taskRoofWash;
        this.taskExternalWash = taskExternalWash;
        this.taskSteamClean = taskSteamClean;
        this.taskSteamCleanFQ = taskSteamCleanFQ;
        this.taskPhoto = taskPhoto;
        this.taskRepair = taskRepair;
        this.repairCount = repairCount;
        this.qualityOxidised = qualityOxidised;
        this.qualityAlliance = qualityAlliance;
        this.remarks = remarks;
        this.quoteStatus = quoteStatus;
        this.taskMoveToRepair = taskMoveToRepair;
    }
} // end class container2

namespace WashPadTabletApp.data
{
    public partial class getcontainer : System.Web.UI.Page
    {

        private string LookupContainer()
        {
            var context = HttpContext.Current;

            var log = log4net.LogManager.GetLogger(this.GetType());
            log.Info("getcontainer.aspx: " + "Getting details for container: " + context.Request["cid"]);

            // Connect to the DB
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                _conn.Open();

                // Call the stored procedure, return the results
                SqlCommand cmd = new SqlCommand("dbo.spGetContainerState", _conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ContainerID", SqlDbType.VarChar).Value = context.Request["cid"];    // get containerID from querystring
                DataTable dt = new DataTable();
                using (SqlDataReader reader = cmd.ExecuteReader()) {
                    dt.Load(reader);
                }
                // Turn the results into an array of containers
                DataView dv = new DataView(dt);

                Container2[] results = new Container2[dv.Count];
                for (int i = 0; i < dv.Count; i++)
                {
                    results[i] = new Container2
                    (
                        dv[i]["containerId"].ToString(),
                        dv[i]["oid"].ToString(),
                        dv[i]["operatorCode"].ToString(),
                        dv[i]["isReefer"].ToString(),
                        dv[i]["Location"].ToString(),
                        (DateTime)dv[i]["WentToWash"],
                        dv[i]["RecTransportMode"].ToString(),
                        dv[i]["is40ftLong"].ToString(),
                        dv[i]["RecMT"].ToString(),
                        dv[i]["taskWash"].ToString(),
                        dv[i]["taskBaseWash"].ToString(),
                        dv[i]["taskRoofWash"].ToString(),
                        dv[i]["taskExternalWash"].ToString(),
                        dv[i]["taskSteamClean"].ToString(),
                        dv[i]["taskSteamCleanFQ"].ToString(),
                        dv[i]["taskPhoto"].ToString(),
                        dv[i]["taskRepair"].ToString(),
                        dv[i]["repairCount"].ToString(),
                        dv[i]["qualityOxidised"].ToString(),
                        dv[i]["qualityAlliance"].ToString(),
                        Server.HtmlEncode(dv[i]["remarks"].ToString()),
                        dv[i]["quoteStatus"].ToString(),
                        dv[i]["taskMoveToRepair"].ToString()
                    );
                }

                // Return the JSon Array to the calling script
                return JsonConvert.SerializeObject(results);
            }
        } // end LookupContainer

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write(LookupContainer());
        } // end page_load
    }
}