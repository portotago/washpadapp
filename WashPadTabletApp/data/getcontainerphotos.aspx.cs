﻿// Note:  This program requires the download and installation of JSON.net
// 1. install the NuGet package manager (Tools>>Extension Manager>>Online Gallery>>NuGet Package Manager
// 2. right-click the project and choose Manage NuGet Packages
// 3. search for JSON and install JSON.net

// Note:  This program requires the download and installation of log4Net
// 1. install the NuGet package manager (Tools>>Extension Manager>>Online Gallery>>NuGet Package Manager
// 2. right-click the project and choose Manage NuGet Packages
// 3. search for log4Net and install log4Net

using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Data.SqlClient;
using System.Data;


public class containerPhoto
{
    public string PHOTOID;
    public string CONTAINERID;
    public string PHOTO;

    public containerPhoto
    (
        string PHOTOID,
        string CONTAINERID,
        string PHOTO
    )
    {
        this.PHOTOID = PHOTOID;
        this.CONTAINERID = CONTAINERID;
        this.PHOTO = PHOTO;
    }
} // end class containerPhoto


namespace WashPadTabletApp.data
{
    public partial class getcontainerphotos : System.Web.UI.Page
    {
       
        private string LookupContainerPhotos()
        {
            var context = HttpContext.Current;

            var log = log4net.LogManager.GetLogger(this.GetType());
            log.Info("getcontainerphotos.aspx: " + "Getting photos for container: " + context.Request["cid"]);

            // Connect to the DB
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                _conn.Open();
                // Call the stored procedure, return the results
                SqlCommand cmd = new SqlCommand("SELECT CONTAINERID,PHOTO,PHOTOTHUMB,PHOTOID FROM containerPhotos WHERE CONTAINERID = '" + context.Request["cid"] + "'", _conn);
                DataTable dt = new DataTable();
                using (SqlDataReader reader = cmd.ExecuteReader()) {
                    dt.Load(reader);
                }
           
                DataView dv = new DataView(dt);

                containerPhoto[] results = new containerPhoto[dv.Count];
                for (int i = 0; i < dv.Count; i++)
                {
                    byte[] img = new byte[] { };
                    // first dig out photo...
                   // byte[] img = (byte[])dv[i]["PHOTOTHUMB"];
                    if (dv[i]["PHOTOTHUMB"] != DBNull.Value)
                    {
                        img = (byte[])dv[i]["PHOTOTHUMB"];
                    }
                    else
                    {
                        img = (byte[])dv[i]["PHOTO"];
                    }

                    string base64String = Convert.ToBase64String(img, 0, img.Length);

                    results[i] = new containerPhoto
                    (
                        dv[i]["PHOTOID"].ToString(),
                        dv[i]["CONTAINERID"].ToString(),
                        base64String.ToString()
                    );
                }

                // Return the JSon Array to the calling script
                return JsonConvert.SerializeObject(results);
            }
        } // end LookupContainerPhotos

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write(LookupContainerPhotos());
        }
    }
}