﻿// Note:  This program requires the download and installation of JSON.net
// 1. install the NuGet package manager (Tools>>Extension Manager>>Online Gallery>>NuGet Package Manager
// 2. right-click the project and choose Manage NuGet Packages
// 3. search for JSON and install JSON.net

// Note:  This program requires the download and installation of log4Net
// 1. install the NuGet package manager (Tools>>Extension Manager>>Online Gallery>>NuGet Package Manager
// 2. right-click the project and choose Manage NuGet Packages
// 3. search for log4Net and install log4Net

using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Data.SqlClient;
using System.Data;


public class containerRepair
{
    public string CONTAINERREPAIRID;
    public string CONTAINERID;
    public string DAMAGELOCATION;
    public string REPAIRCODE;
    public string REPAIRDESCRIPTION;
    public string QUANTITY;
    public string REPAIRSTATUS;

    public containerRepair
    (
        string CONTAINERREPAIRID,
        string CONTAINERID,
        string DAMAGELOCATION,
        string REPAIRCODE,
        string REPAIRDESCRIPTION,
        string QUANTITY,
        string REPAIRSTATUS
    )
    {
        this.CONTAINERREPAIRID = CONTAINERREPAIRID;
        this.CONTAINERID = CONTAINERID;
        this.DAMAGELOCATION = DAMAGELOCATION;
        this.REPAIRCODE = REPAIRCODE;
        this.REPAIRDESCRIPTION = REPAIRDESCRIPTION;
        this.QUANTITY = QUANTITY;
        this.REPAIRSTATUS = REPAIRSTATUS;
    }
} // end class containerRepair


namespace WashPadTabletApp.data
{
    public partial class getcontainerrepairs : System.Web.UI.Page
    {

        private string LookupContainerRepairs()
        {
            var context = HttpContext.Current;

            var log = log4net.LogManager.GetLogger(this.GetType());
            log.Info("getcontainerrepairs.aspx: " + "Looking up repairs for container " + context.Request["cid"]);

            // Connect to the DB
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                _conn.Open();
                // Call the stored procedure, return the results
                SqlCommand cmd = new SqlCommand("SELECT * FROM containerRepairs WHERE CONTAINERID = '" + context.Request["cid"] + "'", _conn);
                DataTable dt = new DataTable();
                using (SqlDataReader reader = cmd.ExecuteReader()) {
                    dt.Load(reader);
                }
                DataView dv = new DataView(dt);

                containerRepair[] results = new containerRepair[dv.Count];
                for (int i = 0; i < dv.Count; i++)
                {
                    results[i] = new containerRepair
                    (
                        dv[i]["CONTAINERREPAIRID"].ToString(),
                        dv[i]["CONTAINERID"].ToString(),
                        dv[i]["DAMAGELOCATION"].ToString(),
                        dv[i]["REPAIRCODE"].ToString(),
                        dv[i]["REPAIRDESCRIPTION"].ToString(),
                        dv[i]["QUANTITY"].ToString(),
                        dv[i]["REPAIRSTATUS"].ToString()
                    );
                }

                // Return the JSon Array to the calling script
                return JsonConvert.SerializeObject(results);
            }
        } // end LookupContainerRepairs

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write(LookupContainerRepairs());
        } // End Page_Load
    }
}