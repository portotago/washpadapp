﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Data.SqlClient;
using System.Data;


namespace WashPadTabletApp.data
{
    public partial class getcontainers : System.Web.UI.Page
    {
        private string LookupContainers()
        {
            var log = log4net.LogManager.GetLogger(this.GetType());
            // This page gets called a LOT, so not logging routine behavior
            //log.Info("getcontainers.aspx: " + "Getting all containers from WashDB (that are still on the washpad)");

            // Connect to the DB
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                _conn.Open();

                // Call the stored procedure, return the results
                SqlCommand cmd = new SqlCommand("dbo.spGetContainerStates", _conn);
                cmd.CommandType = CommandType.StoredProcedure;
                DataTable dt = new DataTable();
                using (SqlDataReader reader = cmd.ExecuteReader()) {
                    dt.Load(reader);
                }

                // Turn the results into an array of containers
                DataView dv = new DataView(dt);

                Container[] results = new Container[dv.Count];
                for (int i = 0; i < dv.Count; i++)
                {
                    results[i] = new Container
                    (
                        dv[i]["containerId"].ToString(),
                        dv[i]["OID"].ToString(),
                        dv[i]["operatorCode"].ToString(),
                        dv[i]["isReefer"].ToString(),
                        dv[i]["Location"].ToString(),
                        (DateTime)dv[i]["WentToWash"],
                        dv[i]["RecTransportMode"].ToString(),
                        dv[i]["is40ftLong"].ToString(),
                        dv[i]["RecMT"].ToString(),
                        dv[i]["taskWash"].ToString(),
                        dv[i]["taskBaseWash"].ToString(),
                        dv[i]["taskRoofWash"].ToString(),
                        dv[i]["taskExternalWash"].ToString(),
                        dv[i]["taskSteamClean"].ToString(),
                        dv[i]["taskSteamCleanFQ"].ToString(),
                        dv[i]["taskPhoto"].ToString(),
                        dv[i]["taskRepair"].ToString(),
                        dv[i]["taskInspect"].ToString(),
                        dv[i]["quoteStatus"].ToString(),
                        dv[i]["taskMoveToRepair"].ToString(),
                        Convert.ToInt32(dv[i]["failedEstimateSubmissionCount"])
                    );
                }

                // Return the JSon Array to the calling script
                return JsonConvert.SerializeObject(results);
            }
        } // end LookupContainers


        protected void Page_Load(object sender, EventArgs e)
        {
           Response.Write(LookupContainers());
        } // end page_load
    }
}