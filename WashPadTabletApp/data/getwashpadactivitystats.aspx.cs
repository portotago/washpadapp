﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Data.SqlClient;
using System.Data;

namespace WashPadTabletApp.data
{
    public partial class getwashpadactivitystats : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Connect to the DB
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                _conn.Open();

                // Call the stored procedure, return the results
                SqlCommand cmd = new SqlCommand("dbo.spGetWashpadActivityStats", _conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@day", SqlDbType.Date).Value = DateTime.Today.ToString("yyyy-MM-dd");

                DataTable dt = new DataTable();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }

                // Return the JSon Array to the calling script
                Response.Write( JsonConvert.SerializeObject(dt));
            }

        }
    }
}