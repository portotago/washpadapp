﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Data.SqlClient;
using System.Data;


public class Washpad
{
    public string WashpadId;
    public decimal RPIC;

    public Washpad
    (
        string WashpadId,
        decimal RPIC
    )
    {
        this.WashpadId = WashpadId;
        this.RPIC = RPIC;
    }
} // end class Washpad


namespace WashPadTabletApp.data
{
    public partial class getwashpadrpic : System.Web.UI.Page
    {
        private string LookupWashpad()
        {
            var context = HttpContext.Current;

            var log = log4net.LogManager.GetLogger(this.GetType());
            log.Info("getwashpadrpic.aspx: " + "Getting RPIC for each washpad");

            // Connect to the DB
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                _conn.Open();

                // Call the stored procedure, return the results
                SqlCommand cmd = new SqlCommand("dbo.spGetWashpadRPIC", _conn);
                cmd.CommandType = CommandType.StoredProcedure;

                DataTable dt = new DataTable();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }

                // Turn the results into an array of containers
                DataView dv = new DataView(dt);

                Washpad[] results = new Washpad[4];

                for (int i = 0; i < 4 ; i++)
                {
                    if (dv[0]["W" + (i + 1)] == DBNull.Value) {
                        results[i] = new Washpad
                        (
                            "W" + (i + 1),
                             -1
                        );
                    }
                    else {
                        results[i] = new Washpad
                        (
                            "W" + (i + 1),
                             Convert.ToDecimal(dv[0]["W" + (i + 1)])
                        );
                    }

                }

                // Return the JSon Array to the calling script
                return JsonConvert.SerializeObject(results);
            }
        } // end LookupWashpad


        protected void Page_Load(object sender, EventArgs e)
        {

            Response.Write( LookupWashpad());

        } // end page_load
    }
}