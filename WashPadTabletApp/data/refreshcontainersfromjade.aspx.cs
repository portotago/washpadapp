﻿// Note:  This program requires the download and installation of log4Net
// 1. install the NuGet package manager (Tools>>Extension Manager>>Online Gallery>>NuGet Package Manager
// 2. right-click the project and choose Manage NuGet Packages
// 3. search for log4Net and install log4Net

using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;


public class ContainerFromJMT
{
    public string id;
    public string oid;
    public string operatorCode;
    public string isReefer;
    public string Location;
    public DateTime WentToWash;
    public string RecTransportMode;
    public string RecMT;
    public string taskInspect;

    public ContainerFromJMT
    (
        string id,
        string oid,
        string operatorCode,
        string isReefer,
        string Location,
        DateTime WentToWash,
        string RecTransportMode,
        string RecMT,
        string taskInspect
    )
    {
        this.id = id;
        this.oid = oid;
        this.operatorCode = operatorCode;
        this.isReefer = isReefer;
        this.Location = Location;
        this.WentToWash = WentToWash;
        this.RecTransportMode = RecTransportMode;
        this.RecMT = RecMT;
        this.taskInspect = taskInspect;
    }
} // end class ContainerFromJMT

public class Container
{
    public string id;
    public string oid;
    public string operatorCode;
    public string isReefer;
    public string Location;
    public DateTime WentToWash;
    public string RecTransportMode;
    public string is40ftLong;
    public string RecMT;
    public string taskWash;
    public string taskBaseWash;
    public string taskRoofWash;
    public string taskExternalWash;
    public string taskSteamClean;
    public string taskSteamCleanFQ;
    public string taskPhoto;
    public string taskRepair;
    public string taskInspect;
    public string quoteStatus;
    public string RepairWorkSite;
    public int failedEstimateSubmissionCount;

    public Container
    (
        string id,
        string oid,
        string operatorCode,
        string isReefer,
        string Location,
        DateTime WentToWash,
        string RecTransportMode,
        string is40ftLong,
        string RecMT,
        string taskWash,
        string taskBaseWash,
        string taskRoofWash,
        string taskExternalWash,
        string taskSteamClean,
        string taskSteamCleanFQ,
        string taskPhoto,
        string taskRepair,
        string taskInspect,
        string quoteStatus,
        string RepairWorkSite,
        int failedEstimateSubmissionCount
    )
    {
        this.id = id;
        this.oid = oid;
        this.operatorCode = operatorCode;
        this.isReefer = isReefer;
        this.Location = Location;
        this.WentToWash = WentToWash;
        this.RecTransportMode = RecTransportMode;
        this.is40ftLong = is40ftLong;
        this.RecMT = RecMT;
        this.taskWash = taskWash;
        this.taskBaseWash = taskBaseWash;
        this.taskRoofWash = taskRoofWash;
        this.taskExternalWash = taskExternalWash;
        this.taskSteamClean = taskSteamClean;
        this.taskSteamCleanFQ = taskSteamCleanFQ;
        this.taskPhoto = taskPhoto;
        this.taskRepair = taskRepair;
        this.taskInspect = taskInspect;
        this.quoteStatus = quoteStatus;
        this.RepairWorkSite = RepairWorkSite;
        this.failedEstimateSubmissionCount = failedEstimateSubmissionCount;
    }
} // end class container

namespace WashPadTabletApp.data
{
    public partial class refreshcontainersfromjade : System.Web.UI.Page
    {
        public static string ByteArrayToString(byte[] ba)
        {
            string hex = BitConverter.ToString(ba);
            return hex.Replace("-", "");
        }


        private string RefreshWashDBContainerList()
        {
            var log = log4net.LogManager.GetLogger(this.GetType());
            string _connection_string = ConfigurationManager.ConnectionStrings["connStatsDB"].ConnectionString;
            string _connection_string2 = ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString;
            DataTable dtContainersFromStatsDB = new DataTable(); ;
            DataTable dtContainersFromWashDB = new DataTable(); ;

            try {
                // Get container list from StatsDB
                using (SqlConnection _conn1 = new SqlConnection(_connection_string))
                {
                    _conn1.Open();
                    SqlCommand cmd = new SqlCommand("dbo.spGetContainersOnWash", _conn1);
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        dtContainersFromStatsDB.Load(reader);
                    }
                    _conn1.Close();
                    log.Debug("refreshcontainersfromjade.aspx: " + dtContainersFromStatsDB.Rows.Count.ToString() + " containers on Wash according to StatsDB");
                } // End using SqlConnection to StatsDB

                // Get container list from WashDB - of containers that haven't left the washpad
                using (SqlConnection _conn3 = new SqlConnection(_connection_string2))
                {
                    _conn3.Open();
                    SqlCommand cmd = new SqlCommand("dbo.spGetContainerStates", _conn3);
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        dtContainersFromWashDB.Load(reader);
                    }
                    _conn3.Close();
                    log.Debug("refreshcontainersfromjade.aspx: " + dtContainersFromWashDB.Rows.Count.ToString() + " containers on Wash according to WashDB");
                } // end using SqlConnection to WashDB
            }
            catch(Exception Ex){
                // TODO: catch any SQL connection errors - don't just remove all the containers
                log.Error("refreshcontainersfromjade.aspx: " + "problem connecting to SQL");
                log.Error("refreshcontainersfromjade.aspx: " + Ex.Message.ToString());
            }



            // --------------------------------------------------------------------------------------------
            // Determine containers to add, remove, and/or update
            // --------------------------------------------------------------------------------------------
            var rows = dtContainersFromWashDB.AsEnumerable().Where(ra => !dtContainersFromStatsDB.AsEnumerable().Any(rb => rb.Field<string>("id") == ra.Field<string>("containerId")));
            DataTable dtContainersToRemove = rows.Any() ? rows.CopyToDataTable() : null;  // container in WashDB, not in StatsDB

            rows = dtContainersFromStatsDB.AsEnumerable().Where(ra => !dtContainersFromWashDB.AsEnumerable().Any(rb => rb.Field<string>("containerId") == ra.Field<string>("id")));
            DataTable dtContainersToAdd = rows.Any() ? rows.CopyToDataTable() : null;  // container in StatsDB, not in WashDB

            var matched = from table1 in dtContainersFromStatsDB.AsEnumerable()
                           join table2 in dtContainersFromWashDB.AsEnumerable()
                           on table1.Field<string>("id")
                           equals table2.Field<string>("containerId")
                           select table1;
            DataTable dtContainersToUpdate = matched.Any() ? matched.CopyToDataTable() : null;  // container in StatsDB AND in WashDB


            // --------------------------------------------------------------------------------------------------------
            // Remove containers that have left the washpad
            if (dtContainersToRemove != null)
            {
                log.Info("refreshcontainersfromjade.aspx: " + "Removing containers from WashDB...");
                using (SqlConnection _conn4 = new SqlConnection(_connection_string2))
                {
                    _conn4.Open();
                    Int32 Tally = 0;
                    // Compile list of containers to remove
                    List<string> containersToRemove = new List<string>();  // including whitespace 
                    foreach (DataRow foundContainer in dtContainersToRemove.Rows)
                    {
                        containersToRemove.Add(foundContainer["containerId"].ToString());
                        Tally++;
                    }
                    // Execute "removal" via stored procedure (which actually just sets attribute LeftWashpad, not a hard delete)
                    SqlCommand cmd = new SqlCommand("dbo.spRemoveContainersFromWash", _conn4);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@ContainerIDs", SqlDbType.VarChar).Value = string.Join(",", containersToRemove);
                    Int32 rowsAffected = cmd.ExecuteNonQuery(); 
                    log.Debug("refreshcontainersfromjade.aspx: " + "containersToRemove: '" + string.Join(",", containersToRemove) + "'");
                    log.Info("refreshcontainersfromjade.aspx: " + Tally + " containers removed from WashDB");
                    _conn4.Close();
                }
            }
            else
            { 
                // No containers to remove, just log
                log.Info("refreshcontainersfromjade.aspx: " + "No containers to remove from the WashDB");
            }
            // End Removal of containers that have left the washpad


            // --------------------------------------------------------------------------------------------------------
            // Insert new containers to WashDB, or unremove them
            if (dtContainersToAdd != null)
            {
                log.Info("refreshcontainersfromjade.aspx: " + "Adding containers to WashDB...");
                using (SqlConnection _conn5 = new SqlConnection(_connection_string2))
                {
                    _conn5.Open();
                    Int32 TallyAdded = 0;
                    Int32 TallyUnremoved = 0;
                    foreach (DataRow newContainer in dtContainersToAdd.Rows)
                    {
                        log.Debug("refreshcontainersfromjade.aspx: " + "Container: " + newContainer["id"].ToString() + " is on the washpad according to StatsDB...");
                        // Check if the container exists in the DB - if it does, "unremove" flag LeftWash via stored proc
                        SqlCommand cmd = new SqlCommand("dbo.spGetContainerState", _conn5);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@ContainerID", SqlDbType.VarChar).Value = newContainer["id"].ToString();
                        DataTable dtExistingContainer = new DataTable(); 
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            dtExistingContainer.Load(reader);
                        }
                        
                        // Either add new row or update existing row depending if exists
                        if (dtExistingContainer != null && dtExistingContainer.Rows.Count > 0)
                        {
                            // Container is already in WashDB, so execute "unremoval" via stored procedure 
                            log.Debug("refreshcontainersfromjade.aspx: " + newContainer["id"].ToString() + " is already in WashDB, so unsetting LeftWash attribute...");
                            cmd = new SqlCommand("dbo.spUnremoveContainerFromWash", _conn5);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@ContainerID", SqlDbType.VarChar).Value = newContainer["id"].ToString();
                            Int32 rowsAffected = cmd.ExecuteNonQuery();
                            TallyUnremoved++;
                        }
                        else {
                            // Container isn't in WashDB so load as new
                            log.Debug("refreshcontainersfromjade.aspx: " + newContainer["id"].ToString() + " is not in WashDB, so adding new record...");
                            log.Debug("refreshcontainersfromjade.aspx: " + "isReefer: " + newContainer["isReefer"].ToString());

                            // Execute Insert via stored procedure
                            cmd = new SqlCommand("dbo.spAddContainerToWashpad", _conn5);
                            cmd.CommandType = CommandType.StoredProcedure;
                            // Parameters - data from statsdb
                            cmd.Parameters.Add("@ContainerID", SqlDbType.VarChar).Value = newContainer["id"].ToString();
                            cmd.Parameters.Add("@OID", SqlDbType.VarChar).Value = ByteArrayToString((byte[])newContainer["oid"]);
                            cmd.Parameters.Add("@operatorCode", SqlDbType.VarChar).Value = newContainer["operatorCode"].ToString();
                            cmd.Parameters.Add("@isReefer", SqlDbType.Int).Value = newContainer["isReefer"];
                            cmd.Parameters.Add("@Location", SqlDbType.VarChar).Value = newContainer["Location"].ToString();
                            cmd.Parameters.Add("@WentToWash", SqlDbType.DateTime).Value = newContainer["WentToWash"].ToString();
                            cmd.Parameters.Add("@RecTransportMode", SqlDbType.VarChar).Value = newContainer["RecTransportMode"].ToString();
                            cmd.Parameters.Add("@is40ftLong", SqlDbType.VarChar).Value = newContainer["is40ftLong"].ToString();
                            cmd.Parameters.Add("@RecMT", SqlDbType.VarChar).Value = newContainer["RecMT"].ToString();
                            cmd.Parameters.Add("@taskDBUpgrade3", SqlDbType.VarChar).Value = newContainer["DBUpgrade3"].ToString();
                            cmd.Parameters.Add("@taskDBCertAdmin", SqlDbType.VarChar).Value = newContainer["DBCertAdmin"].ToString();
                            cmd.Parameters.Add("@PT_lessthan_Rec", SqlDbType.VarChar).Value = newContainer["PT_lessthan_Rec"].ToString();
        
                            // Set actionable task statuses for incoming containers
                            // -------------- Task Inspect (inferred complete tasks for dry)
                            if (newContainer["RequiresInspection"].ToString() != "")
                            {
                                cmd.Parameters.Add("@taskInspect", SqlDbType.Int).Value = 23; // Open task from Jade
                            }
                            else
                            {
                                if (newContainer["isReefer"].ToString() == "True")
                                {
                                    cmd.Parameters.Add("@taskInspect", SqlDbType.Int).Value = 0;   // No task
                                } else {
                                    cmd.Parameters.Add("@taskInspect", SqlDbType.Int).Value = 40; // ASSUME DRYS initially have a COMPLETED inspect task
                                }
                            }

                            // -------------- Task Wash
                            if (newContainer["Wash"].ToString() != "")
                            {
                                cmd.Parameters.Add("@taskWash", SqlDbType.Int).Value = 23; // Open task from Jade
                            }
                            else
                            {
                                cmd.Parameters.Add("@taskWash", SqlDbType.Int).Value = 0;   // Task N/A
                            }

                            // -------------- Task BaseWash
                            if (newContainer["BaseWash"].ToString() != "")
                            {
                                cmd.Parameters.Add("@taskBaseWash", SqlDbType.Int).Value = 23; // Open task from Jade
                            }
                            else
                            {
                                cmd.Parameters.Add("@taskBaseWash", SqlDbType.Int).Value = 0;   // Task N/A
                            }

                            // -------------- Task Roof Wash
                            if (newContainer["RoofWash"].ToString() != "")
                            {
                                cmd.Parameters.Add("@taskRoofWash", SqlDbType.Int).Value = 23; // Open task from Jade
                            }
                            else
                            {
                                cmd.Parameters.Add("@taskRoofWash", SqlDbType.Int).Value = 0;   // Task N/A
                            }

                            // -------------- Task External Clean
                            if (newContainer["ExternalClean"].ToString() != "")
                            {
                                cmd.Parameters.Add("@taskExternalWash", SqlDbType.Int).Value = 23; // Open task from Jade
                            }
                            else
                            {
                                cmd.Parameters.Add("@taskExternalWash", SqlDbType.Int).Value = 0;   // Task N/A
                            }

                            // -------------- SteamClean
                            if (newContainer["SteamClean"].ToString() != "")
                            {
                                cmd.Parameters.Add("@taskSteamClean", SqlDbType.Int).Value = 23; // Open task from Jade
                            }
                            else {
                                cmd.Parameters.Add("@taskSteamClean", SqlDbType.Int).Value = 0;   // Task N/A
                            }

                            // -------------- SteamCleanFQ
                            if (newContainer["SteamCleanFQ"].ToString() != "")
                            {
                                cmd.Parameters.Add("@taskSteamCleanFQ", SqlDbType.Int).Value = 23; // Open task from Jade
                            }
                            else
                            {
                                cmd.Parameters.Add("@taskSteamCleanFQ", SqlDbType.Int).Value = 0;   // Task N/A
                            }

                            // -------------- Photo
                            if (newContainer["Photo"].ToString() != "")
                            {
                                cmd.Parameters.Add("@taskPhoto", SqlDbType.Int).Value = 23; // Open task from Jade
                            }
                            else
                            {
                                cmd.Parameters.Add("@taskPhoto", SqlDbType.Int).Value = 0;   // Task N/A
                            }

                            // -------------- Repair
                            if (newContainer["Repair"].ToString() != "")
                            {
                                cmd.Parameters.Add("@taskRepair", SqlDbType.Int).Value = 23; // Open task from Jade
                            }
                            else
                            {
                                cmd.Parameters.Add("@taskRepair", SqlDbType.Int).Value = 0;   // Task N/A
                            }

                            // Parameters - Metadata
                            cmd.Parameters.Add("@updatedBy", SqlDbType.VarChar).Value = Context.User.Identity.Name;
                            Int32 rowsAffected = cmd.ExecuteNonQuery();
                            TallyAdded++;
                        }
                    }
                    log.Info("refreshcontainersfromjade.aspx: " + TallyAdded + " containers added to WashDB");
                    log.Info("refreshcontainersfromjade.aspx: " + TallyUnremoved + " containers unremoved from WashDB");
                    _conn5.Close();
                } // end using
            }
            else
            {
                // No containers to add, just log
                log.Info("refreshcontainersfromjade.aspx: " + "No containers added or unremoved from WashDB");
            }
            // End Insertion of new containers


            // --------------------------------------------------------------------------------------------------------
            // Update containers that are already in WashDB, particularly updating Task status
            if (dtContainersToUpdate != null)
            {
                log.Info("refreshcontainersfromjade.aspx: " + "Updating containers in WashDB...");
                Int32 TallyUpdateCandidates = 0;
                Int32 TallyUpdated = 0;
                // Compile list of containers to remove
                List<string> containersToUpdate = new List<string>();  // including whitespace
                foreach (DataRow containerInStatsDB in dtContainersToUpdate.Rows)
                {
                    string strSQLFragment = "";

                    containersToUpdate.Add(containerInStatsDB["id"].ToString());
                    TallyUpdateCandidates++;

                    // Get the container state details from WashDB
                    DataRow[] containerInWashDB = dtContainersFromWashDB.Select("containerID = '" + containerInStatsDB["id"].ToString() + "'");

                    // ---------------------------------------------------------------------------------------
                    // Build up fragments for SQL Update statement -------------------------------------------

                    // Wash task --------------------
                    // if open in StatsDB  (if task in WashDB is 0 or 10 or 20, update WashDB to 23)
                    if (containerInStatsDB["Wash"].ToString() != "" && (containerInWashDB[0]["taskWash"].ToString() == "0" || containerInWashDB[0]["taskWash"].ToString() == "10" || containerInWashDB[0]["taskWash"].ToString() == "20")) 
                    {
                        strSQLFragment = strSQLFragment + "taskWash = '23', ";
                    }
                    // if closed in StatsDB (if task in WashDB is 23, update WashDB to 0)
                    if (containerInStatsDB["Wash"].ToString() == "" && containerInWashDB[0]["taskWash"].ToString() == "23")
                    {
                        strSQLFragment = strSQLFragment + "taskWash = '0', ";
                    }

                    // BaseWash task --------------------
                    // if open in StatsDB  (if task in WashDB is 0 or 10 or 20, update WashDB to 23)
                    if (containerInStatsDB["BaseWash"].ToString() != "" && (containerInWashDB[0]["taskBaseWash"].ToString() == "0" || containerInWashDB[0]["taskBaseWash"].ToString() == "10" || containerInWashDB[0]["taskBaseWash"].ToString() == "20"))
                    {
                        strSQLFragment = strSQLFragment + "taskBaseWash = '23', ";
                    }
                    // if closed in StatsDB (if task in WashDB is 23, update WashDB to 0)
                    if (containerInStatsDB["BaseWash"].ToString() == "" && containerInWashDB[0]["taskBaseWash"].ToString() == "23")
                    {
                        strSQLFragment = strSQLFragment + "taskBaseWash = '0', ";
                    }

                    // RoofWash task --------------------
                    // if open in StatsDB  (if task in WashDB is 0 or 10 or 20, update WashDB to 23)
                    if (containerInStatsDB["RoofWash"].ToString() != "" && (containerInWashDB[0]["taskRoofWash"].ToString() == "0" || containerInWashDB[0]["taskRoofWash"].ToString() == "10" || containerInWashDB[0]["taskRoofWash"].ToString() == "20"))
                    {
                        strSQLFragment = strSQLFragment + "taskRoofWash = '23', ";
                    }
                    // if closed in StatsDB (if task in WashDB is 23, update WashDB to 0)
                    if (containerInStatsDB["RoofWash"].ToString() == "" && containerInWashDB[0]["taskRoofWash"].ToString() == "23")
                    {
                        strSQLFragment = strSQLFragment + "taskRoofWash = '0', ";
                    }

                    // ExternalClean task --------------------
                    // if open in StatsDB  (if task in WashDB is 0 or 10 or 20, update WashDB to 23)
                    if (containerInStatsDB["ExternalClean"].ToString() != "" && (containerInWashDB[0]["taskExternalWash"].ToString() == "0" || containerInWashDB[0]["taskExternalWash"].ToString() == "10" || containerInWashDB[0]["taskExternalWash"].ToString() == "20"))
                    {
                        strSQLFragment = strSQLFragment + "taskExternalWash = '23', ";
                    }
                    // if closed in StatsDB (if task in WashDB is 23, update WashDB to 0)
                    if (containerInStatsDB["ExternalClean"].ToString() == "" && containerInWashDB[0]["taskExternalWash"].ToString() == "23")
                    {
                        strSQLFragment = strSQLFragment + "taskExternalWash = '0', ";
                    }

                    // SteamClean task --------------------
                    // if open in StatsDB  (if task in WashDB is 0 or 10 or 20, update WashDB to 23)
                    if (containerInStatsDB["SteamClean"].ToString() != "" && (containerInWashDB[0]["taskSteamClean"].ToString() == "0" || containerInWashDB[0]["taskSteamClean"].ToString() == "10" || containerInWashDB[0]["taskSteamClean"].ToString() == "20"))
                    {
                        strSQLFragment = strSQLFragment + "taskSteamClean = '23', ";
                    }
                    // if closed in StatsDB (if task in WashDB is 23, update WashDB to 0)
                    if (containerInStatsDB["SteamClean"].ToString() == "" && containerInWashDB[0]["taskSteamClean"].ToString() == "23")
                    {
                        strSQLFragment = strSQLFragment + "taskSteamClean = '0', ";
                    }

                    // SteamCleanFQ task --------------------
                    // if open in StatsDB  (if task in WashDB is 0 or 10 or 20, update WashDB to 23)
                    if (containerInStatsDB["SteamCleanFQ"].ToString() != "" && (containerInWashDB[0]["taskSteamCleanFQ"].ToString() == "0" || containerInWashDB[0]["taskSteamCleanFQ"].ToString() == "10" || containerInWashDB[0]["taskSteamCleanFQ"].ToString() == "20"))
                    {
                        strSQLFragment = strSQLFragment + "taskSteamCleanFQ = '23', ";
                    }
                    // if closed in StatsDB (if task in WashDB is 23, update WashDB to 0)
                    if (containerInStatsDB["SteamCleanFQ"].ToString() == "" && containerInWashDB[0]["taskSteamCleanFQ"].ToString() == "23")
                    {
                        strSQLFragment = strSQLFragment + "taskSteamCleanFQ = '0', ";
                    }

                    // Photo task --------------------
                    // if open in StatsDB  (if task in WashDB is 0 or 10 or 20, update WashDB to 23)
                    if (containerInStatsDB["Photo"].ToString() != "" && (containerInWashDB[0]["taskPhoto"].ToString() == "0" || containerInWashDB[0]["taskPhoto"].ToString() == "10" || containerInWashDB[0]["taskSteamCleanFQ"].ToString() == "20"))
                    {
                        strSQLFragment = strSQLFragment + "taskPhoto = '23', ";
                    }
                    // if closed in StatsDB (if task in WashDB is 23, update WashDB to 0)
                    if (containerInStatsDB["Photo"].ToString() == "" && containerInWashDB[0]["taskPhoto"].ToString() == "23")
                    {
                        strSQLFragment = strSQLFragment + "taskPhoto = '0', ";
                    }

                    // Repair task --------------------
                    // if open in StatsDB  (if task in WashDB is 0 or 10 or 20, update WashDB to 23)
                    if (containerInStatsDB["Repair"].ToString() != "" && (containerInWashDB[0]["taskRepair"].ToString() == "0" || containerInWashDB[0]["taskRepair"].ToString() == "10" || containerInWashDB[0]["taskRepair"].ToString() == "20"))
                    {
                        strSQLFragment = strSQLFragment + "taskRepair = '23', ";
                    }
                    // if closed in StatsDB (if task in WashDB is 23, update WashDB to 0)
                    if (containerInStatsDB["Repair"].ToString() == "" && containerInWashDB[0]["taskRepair"].ToString() == "23")
                    {
                        strSQLFragment = strSQLFragment + "taskRepair = '0', ";
                    }

                    // Inspection task------------------------
                    // The ONLY scenario is being handled for inspections is closure of inspection for DRYs
                    // if RequiresInspection closed in StatsDB AND if taskInspect in WashDB is 23, update taskInspect WashDB to 40 INSPECTED
                    if (containerInStatsDB["RequiresInspection"].ToString() == "" && containerInWashDB[0]["taskInspect"].ToString() == "23")
                    {
                        strSQLFragment = strSQLFragment + "taskInspect = '40', ";
                    }

                    // DB Upgrade Tasks ------------------------
                    // Just pass through value verbatim
                    if (containerInStatsDB["DBUpgrade3"].ToString() != "")
                    {
                        strSQLFragment = strSQLFragment + "taskDBUpgrade3 = '" + containerInStatsDB["DBUpgrade3"].ToString() + "', ";
                    }
                    if (containerInStatsDB["DBCertAdmin"].ToString() != "")
                    {
                        strSQLFragment = strSQLFragment + "taskDBCertAdmin = '" + containerInStatsDB["DBCertAdmin"].ToString() + "', ";
                    }

                    // Update the container location (containers can get moved from one washpad to another)
                    if(containerInStatsDB["location"].ToString() != containerInWashDB[0]["location"].ToString()){
                        strSQLFragment = strSQLFragment + "location = '" + containerInStatsDB["location"].ToString() + "', ";
                    }

                    // if SQL fragment has been populated at all, wrap up the SQL statement and execute it
                    if (strSQLFragment.Length > 0)
                    {
                        log.Debug("refreshcontainersfromjade.aspx: updating task status or location in WashDB for " + containerInStatsDB["id"]);
                        strSQLFragment = "UPDATE containerStates SET " + strSQLFragment.Substring(0, strSQLFragment.Length - 2) + " WHERE containerId = '" + containerInStatsDB["id"] + "'";
                        log.Debug("refreshcontainersfromjade.aspx: strSQLFragment: " + strSQLFragment);
                        // Execute the SQL
                        using (SqlConnection _conn6 = new SqlConnection(_connection_string2))
                        {
                            _conn6.Open();
                            SqlCommand cmd = new SqlCommand(strSQLFragment, _conn6);
                            Int32 rowsAffected = cmd.ExecuteNonQuery();
                            _conn6.Close();
                        }
                        TallyUpdated++;
                    }
     
                }
                log.Info("refreshcontainersfromjade.aspx: " + TallyUpdated + " containers updated in WashDB");

           }
            else
            {
                // No containers to update, just log
                log.Info("refreshcontainersfromjade.aspx: " + "No containers found that require update");
            }
            // End Update containers 


            return "Completed refresh of containers from StatsDB";
        } // end LookupContainers




        protected void Page_Load(object sender, EventArgs e)
        {
            var log = log4net.LogManager.GetLogger(this.GetType());
            log.Info("refreshcontainersfromjade.aspx: " + "Refreshing container list from StatsDB to WashDB");

            Response.Write(RefreshWashDBContainerList());
            log.Info("refreshcontainersfromjade.aspx: " + "Refresh completed.");
        } // end page_load
    }
}