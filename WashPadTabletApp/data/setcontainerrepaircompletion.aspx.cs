﻿// Note:  This program requires the download and installation of log4Net
// 1. install the NuGet package manager (Tools>>Extension Manager>>Online Gallery>>NuGet Package Manager
// 2. right-click the project and choose Manage NuGet Packages
// 3. search for log4Net and install log4Net

using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;

namespace WashPadTabletApp.data
{
    public partial class setcontainerrepaircompletion : System.Web.UI.Page
    {

        // Input sanitization function for container numbers
        public void sanitizeContainerNo(out string cleanOutput, string dirtyInput)
        {
            if (!String.IsNullOrEmpty(dirtyInput) && Regex.IsMatch(dirtyInput, @"^[a-zA-z]{4}[0-9]{7}$"))
            {
                cleanOutput = dirtyInput;
            }
            else
            {
                cleanOutput = "INVALID";
            }
            return;
        } // end sanitizeContainerNo

        // Input sanitization function for strings, commas, spaces, empty strings also ok
        public void sanitizeString(out string cleanOutput, string dirtyInput)
        {
            if (!String.IsNullOrEmpty(dirtyInput) && Regex.IsMatch(dirtyInput, @"^[a-zA-z0-9, ]+$"))
            {
                cleanOutput = dirtyInput;
            }
            else
            {
                if (String.IsNullOrEmpty(dirtyInput)) // Allowing blanks
                {
                    cleanOutput = "";
                }
                else
                {
                    cleanOutput = "INVALID";
                }
            }
            return;
        } // end sanitizeString

        // Input sanitization function for integers
        public void sanitizeInteger(out Int32 cleanOutput, string dirtyInput)
        {
            if (!String.IsNullOrEmpty(dirtyInput) && Regex.IsMatch(dirtyInput, @"^[0-9]+$"))
            {
                cleanOutput = Int32.Parse(dirtyInput);
            }
            else
            {
                cleanOutput = -1;
            }
            return;
        } // end sanitizeString


        protected void Page_Load(object sender, EventArgs e)
        {
            var log = log4net.LogManager.GetLogger(this.GetType());

            var context = HttpContext.Current;

            // Declare inputs
            string ContainerID = "INVALID";
            string tabletUser = "INVALID";
            string repairsDone = "INVALID";
            string repairsNotDone = "INVALID";
            Int32 taskRepair = -1;

            // Sanitize input 
            sanitizeContainerNo(out ContainerID, context.Request["cid"]);
            sanitizeInteger(out taskRepair, context.Request["taskRepair"]);
            sanitizeString(out tabletUser, Context.User.Identity.Name);
            sanitizeString(out repairsDone, context.Request["repairsDone"]);
            sanitizeString(out repairsNotDone, context.Request["repairsNotDone"]);

            // TODO: IF ANY INPUT INVALID DO NOTHING, ELSE
            log.Info("setcontainerrepaircompletion.aspx: Marking selected tasks as complete for " + ContainerID + "...");
            log.Debug("setcontainerrepaircompletion.aspx: taskRepair =  " + taskRepair);
            log.Debug("setcontainerrepaircompletion.aspx: repairsDone =  " + repairsDone);
            log.Debug("setcontainerrepaircompletion.aspx: repairsNotDone =  " + repairsNotDone);

            // Compile dynamic SQL string for repairs done
            //string strSQLRepairsDone = "UPDATE dbo.containerRepairs SET repairStatus = '30' WHERE containerRepairID IN (" + repairsDone + ")";
            string strSQLRepairsDone = "UPDATE dbo.containerRepairs SET repairStatus = '30', completedRepair = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' WHERE containerRepairID IN (" + repairsDone + ")";
            // Compile dynamic SQL string for repairs not done
            string strSQLRepairsNotDone = "UPDATE dbo.containerRepairs SET repairStatus = '20', completedRepair = NULL WHERE containerRepairID IN (" + repairsNotDone + ")";
            // Compile dynamic SQL string for repair task status
            string strSQLRepairTaskStatus = "UPDATE dbo.containerStates SET taskRepair = '" + taskRepair + "' WHERE containerId = '" + ContainerID + "'";

            log.Debug("setcontainerrepaircompletion.aspx: strSQLRepairsDone =  " + strSQLRepairsDone);
            log.Debug("setcontainerrepaircompletion.aspx: strSQLRepairsNotDone =  " + strSQLRepairsNotDone);
            log.Debug("setcontainerrepaircompletion.aspx: strSQLRepairTaskStatus =  " + strSQLRepairTaskStatus);

            // Execute SQL
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                _conn.Open();
                SqlCommand cmd = new SqlCommand(strSQLRepairTaskStatus, _conn);
                cmd.ExecuteNonQuery();

                if (repairsDone != "")
                {
                    cmd = new SqlCommand(strSQLRepairsDone, _conn);
                    cmd.ExecuteNonQuery();
                }

                if (repairsNotDone != "")
                {
                    cmd = new SqlCommand(strSQLRepairsNotDone, _conn);
                    cmd.ExecuteNonQuery();
                }

                _conn.Close();
            }

            Response.Write("Repair completion recorded in the database.");
        }
    }
}