﻿// Note:  This program requires the download and installation of log4Net
// 1. install the NuGet package manager (Tools>>Extension Manager>>Online Gallery>>NuGet Package Manager
// 2. right-click the project and choose Manage NuGet Packages
// 3. search for log4Net and install log4Net

using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;

namespace WashPadTabletApp.data
{
    public partial class setcontainerrepairs : System.Web.UI.Page
    {

        // Input sanitization function for container numbers
        public void sanitizeContainerNo(out string cleanOutput, string dirtyInput)
        {
            if (!String.IsNullOrEmpty(dirtyInput) && Regex.IsMatch(dirtyInput, @"^[a-zA-Z]{4}[0-9]{7}$"))
            {
                cleanOutput = dirtyInput;
            }
            else
            {
                cleanOutput = "INVALID";
            }
            return;
        } // end sanitizeContainerNo

        // Input sanitization function for strings
        public void sanitizeString(out string cleanOutput, string dirtyInput)
        {
            if (!String.IsNullOrEmpty(dirtyInput) && Regex.IsMatch(dirtyInput, @"^[a-zA-Z0-9 ]+$"))
            {
                cleanOutput = dirtyInput;
            }
            else
            {
                cleanOutput = "INVALID";
            }
            return;
        } // end sanitizeString

        // Input sanitization function for integers
        public void sanitizeInteger(out Int32 cleanOutput, string dirtyInput)
        {
            if (!String.IsNullOrEmpty(dirtyInput) && Regex.IsMatch(dirtyInput, @"^[0-9]+$"))
            {
                cleanOutput = Int32.Parse(dirtyInput);
            }
            else
            {
                cleanOutput = -1;
            }
            return;
        } // end sanitizeString

        protected void Page_Load(object sender, EventArgs e)
        {
            var log = log4net.LogManager.GetLogger(this.GetType());
            log.Info("setcontainerrepairs.aspx: " + "Adding a repairs tasks");

            var context = HttpContext.Current;

            // Connect to the DB
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                _conn.Open();

                // Declare inputs
                Int32 howManyRepairs = -1;
                Int32 ContainerRepairID = -1;
                string ContainerID = "INVALID";
                string ContainerOID = "INVALID";
                string tabletUser = "INVALID";
                string damageLocation = "INVALID";
                string repairCode = "INVALID";
                string repairDescription = "INVALID";
                Int32 quantity = -1;

                sanitizeInteger(out howManyRepairs, context.Request["howManyRepairs"]);

                // call stored proc once per repair
                for (int i = 1; i <= howManyRepairs; i++)
                {
                    sanitizeString(out tabletUser, Context.User.Identity.Name);
                    sanitizeContainerNo(out ContainerID, context.Request["cid_" + i]);
                    sanitizeInteger(out ContainerRepairID, context.Request["containerRepairID_" + i]);
                    sanitizeString(out ContainerOID, context.Request["oid_" + i]);
                    sanitizeString(out damageLocation, context.Request["damageLocation_" + i]);
                    sanitizeString(out repairCode, context.Request["repairCode_" + i]);
                    // TODO: Not sanitising description... need more flexible string sanitiser
                    // sanitizeString(out repairDescription, context.Request["repairDescription_" + i]);
                    repairDescription = context.Request["repairDescription_" + i];
                    sanitizeInteger(out quantity, context.Request["quantity_" + i]);

                    log.Debug("setcontainerrepairs.aspx: " + "Adding repair " + damageLocation + " to " + ContainerID);
                    log.Debug("setcontainerrepairs.aspx: " + "OID " + context.Request["oid_" + i]);

                    // Call the stored procedure, return the results
                    SqlCommand cmd = new SqlCommand("dbo.spInsertContainerRepair", _conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@ContainerRepairID", SqlDbType.Int).Value = ContainerRepairID;
                    cmd.Parameters.Add("@oid", SqlDbType.VarChar).Value = ContainerOID;
                    cmd.Parameters.Add("@ContainerID", SqlDbType.VarChar).Value = ContainerID;
                    cmd.Parameters.Add("@damageLocation", SqlDbType.VarChar).Value = damageLocation;
                    cmd.Parameters.Add("@repairCode", SqlDbType.VarChar).Value = repairCode;
                    cmd.Parameters.Add("@repairDescription", SqlDbType.VarChar).Value = repairDescription;
                    cmd.Parameters.Add("@quantity", SqlDbType.Int).Value = quantity;
                    cmd.Parameters.Add("@updatedBy", SqlDbType.VarChar).Value = tabletUser;

                    cmd.ExecuteNonQuery();
                }

            } // end using connection

  
            // return JSON
            // TODO: determine when to return error
            Response.Write("{\"status\": \"success\"}");
        } // End Page_Load
    }
}