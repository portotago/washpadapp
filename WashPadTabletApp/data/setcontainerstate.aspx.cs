﻿// Note:  This program requires the download and installation of log4Net
// 1. install the NuGet package manager (Tools>>Extension Manager>>Online Gallery>>NuGet Package Manager
// 2. right-click the project and choose Manage NuGet Packages
// 3. search for log4Net and install log4Net

using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;

namespace WashPadTabletApp.data
{
    public partial class setcontainerstate : System.Web.UI.Page
    {
        // Input sanitization function for container numbers
        public void sanitizeContainerNo(out string cleanOutput, string dirtyInput) {
            if (!String.IsNullOrEmpty(dirtyInput) && Regex.IsMatch(dirtyInput, @"^[a-zA-z]{4}[0-9]{7}$"))
            {
                cleanOutput = dirtyInput;
            }
            else {
                cleanOutput = "INVALID";
            }
            return;
        } // end sanitizeContainerNo

        // Input sanitization function for strings, empty strings also ok
        public void sanitizeString(out string cleanOutput, string dirtyInput)
        {
            if (!String.IsNullOrEmpty(dirtyInput) && Regex.IsMatch(dirtyInput, @"^[a-zA-z0-9 .;:()]+$"))
            {
                cleanOutput = dirtyInput;
            }
            else
            {
                if (String.IsNullOrEmpty(dirtyInput)) // Allowing blanks
                {
                    cleanOutput = "";
                }
                else
                {
                    cleanOutput = "INVALID";
                }
            }
            return;
        } // end sanitizeString

        // Input sanitization function for integers
        public void sanitizeInteger(out Int32 cleanOutput, string dirtyInput)
        {
            if (!String.IsNullOrEmpty(dirtyInput) && Regex.IsMatch(dirtyInput, @"^[0-9]+$"))
            {
                cleanOutput = Int32.Parse(dirtyInput);
            }
            else
            {
                cleanOutput = -1;
            }
            return;
        } // end sanitizeInteger

        // Input sanitization function for strings, empty strings also ok
        public void cleanupRemarks(out string cleanOutput, string dirtyInput)
        {
            cleanOutput = dirtyInput;
            cleanOutput = cleanOutput.Replace("'", " ");
            cleanOutput = cleanOutput.Replace(",", " ");
            cleanOutput = cleanOutput.Replace("%", " ");
            cleanOutput = cleanOutput.Replace(":", " ");
            cleanOutput = cleanOutput.Replace(";", " ");
            cleanOutput = cleanOutput.Replace("\r\n", " ");
            cleanOutput = cleanOutput.Replace("\n", " ");
            cleanOutput = cleanOutput.Replace("\r", " ");
            cleanOutput = cleanOutput.Replace("\"", " ");
            return;
        } // end cleanupRemarks

        protected void Page_Load(object sender, EventArgs e)
        {
            var log = log4net.LogManager.GetLogger(this.GetType());
            var context = HttpContext.Current;

            // Declare inputs
            string ContainerID = "INVALID";
            string tabletUser = "INVALID";
            string remarks = "INVALID";
            string quoteStatus = "INVALID";
            Int32 taskMoveToRepair = -1;
            Int32 taskInspect = -1;
            Int32 taskWash = -1;
            Int32 taskBaseWash = -1;
            Int32 taskRoofWash = -1;
            Int32 taskExternalWash = -1;
            Int32 taskPhoto = -1;
            Int32 taskRepair = -1;
            Int32 qualityOxidised = -1;
            Int32 qualityAlliance = -1;

            // Sanitize input 
            sanitizeContainerNo(out ContainerID, context.Request["cid"]);
            sanitizeInteger(out taskInspect, context.Request["taskInspect"]);
            sanitizeInteger(out taskWash, context.Request["taskWash"]);
            sanitizeInteger(out taskBaseWash, context.Request["taskBaseWash"]);
            sanitizeInteger(out taskRoofWash, context.Request["taskRoofWash"]);
            sanitizeInteger(out taskExternalWash, context.Request["taskExternalWash"]);
            sanitizeInteger(out taskPhoto, context.Request["taskPhoto"]);
            sanitizeInteger(out taskRepair, context.Request["taskRepair"]);
            sanitizeInteger(out taskMoveToRepair, context.Request["taskMoveToRepair"]);
            sanitizeInteger(out qualityOxidised, context.Request["qualityOxidised"]);
            sanitizeInteger(out qualityAlliance, context.Request["qualityAlliance"]);
            cleanupRemarks(out remarks, context.Request["remarks"]);
            //remarks = context.Request["remarks"];
            sanitizeString(out quoteStatus, context.Request["quoteStatus"]);
            sanitizeString(out tabletUser, Context.User.Identity.Name);

            // TODO: IF ANY INPUT INVALID DO NOTHING, ELSE
            log.Info("setcontainerstate.aspx: " + " Setting state for " + ContainerID + "...");
            log.Debug("setcontainerstate.aspx: " + " ContainerID=" + ContainerID + ", remarks=" + remarks + ", quoteStatus=" + quoteStatus + ", taskMoveToRepair=" + taskMoveToRepair + ", taskInspect=" + taskInspect + ", updatedBy=" + tabletUser + ", referrer=" + context.Request.UrlReferrer.ToString());

            // Connect to the DB
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                _conn.Open();
                // Call the stored procedure, return the results
                SqlCommand cmd = new SqlCommand("dbo.spSetContainerState", _conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@ContainerID", SqlDbType.VarChar).Value = ContainerID;
                cmd.Parameters.Add("@remarks", SqlDbType.VarChar).Value = remarks;
                cmd.Parameters.Add("@quoteStatus", SqlDbType.VarChar).Value = quoteStatus;
                cmd.Parameters.Add("@taskMoveToRepair", SqlDbType.VarChar).Value = taskMoveToRepair;

                cmd.Parameters.Add("@taskInspect", SqlDbType.Int).Value = taskInspect;
                cmd.Parameters.Add("@taskWash", SqlDbType.Int).Value = taskWash;
                cmd.Parameters.Add("@taskBaseWash", SqlDbType.Int).Value = taskBaseWash;
                cmd.Parameters.Add("@taskRoofWash", SqlDbType.Int).Value = taskRoofWash;
                cmd.Parameters.Add("@taskExternalWash", SqlDbType.Int).Value = taskExternalWash;
                cmd.Parameters.Add("@taskPhoto", SqlDbType.Int).Value = taskPhoto;
                cmd.Parameters.Add("@taskRepair", SqlDbType.Int).Value = taskRepair;
                cmd.Parameters.Add("@qualityOxidised", SqlDbType.Int).Value = qualityOxidised;
                cmd.Parameters.Add("@qualityAlliance", SqlDbType.Int).Value = qualityAlliance;

                cmd.Parameters.Add("@lastUpdated", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@updatedBy", SqlDbType.VarChar).Value = tabletUser;

                cmd.ExecuteNonQuery();
               
                _conn.Close();
            }

            log.Info("setcontainerstate.aspx: FYI state saved - ContainerID=" + ContainerID + ", taskInspect=" + taskInspect);


        } // End Page_Load
    }
}