﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;

namespace WashPadTabletApp.data
{
    public partial class setcontainertaskcompletion : System.Web.UI.Page
    {

        // Input sanitization function for container numbers
        public void sanitizeContainerNo(out string cleanOutput, string dirtyInput)
        {
            if (!String.IsNullOrEmpty(dirtyInput) && Regex.IsMatch(dirtyInput, @"^[a-zA-z]{4}[0-9]{7}$"))
            {
                cleanOutput = dirtyInput;
            }
            else
            {
                cleanOutput = "INVALID";
            }
            return;
        } // end sanitizeContainerNo

        // Input sanitization function for strings, empty strings also ok
        public void sanitizeString(out string cleanOutput, string dirtyInput)
        {
            if (!String.IsNullOrEmpty(dirtyInput) && Regex.IsMatch(dirtyInput, @"^[a-zA-z0-9 ]+$"))
            {
                cleanOutput = dirtyInput;
            }
            else
            {
                if (String.IsNullOrEmpty(dirtyInput)) // Allowing blanks
                {
                    cleanOutput = "";
                }
                else
                {
                    cleanOutput = "INVALID";
                }
            }
            return;
        } // end sanitizeString

        // Input sanitization function for integers
        public void sanitizeInteger(out Int32 cleanOutput, string dirtyInput)
        {
            if (!String.IsNullOrEmpty(dirtyInput) && Regex.IsMatch(dirtyInput, @"^[0-9]+$"))
            {
                cleanOutput = Int32.Parse(dirtyInput);
            }
            else
            {
                cleanOutput = -1;
            }
            return;
        } // end sanitizeString


        protected void Page_Load(object sender, EventArgs e)
        {
            var log = log4net.LogManager.GetLogger(this.GetType());
            var context = HttpContext.Current;

            // Declare inputs
            string ContainerID = "INVALID";
            string tabletUser = "INVALID";
            string grade = "INVALID";
            Int32 taskWash = -1;
            Int32 taskBaseWash = -1;
            Int32 taskRoofWash = -1;
            Int32 taskExternalWash = -1;
            Int32 taskSteamClean = -1;
            Int32 taskSteamCleanFQ = -1;
            Int32 taskPhoto = -1;
            Int32 taskRepair = -1;
  
            // Sanitize input 
            sanitizeContainerNo(out ContainerID, context.Request["cid"]);
            sanitizeInteger(out taskWash, context.Request["taskWash"]);
            sanitizeInteger(out taskBaseWash, context.Request["taskBaseWash"]);
            sanitizeInteger(out taskRoofWash, context.Request["taskRoofWash"]);
            sanitizeInteger(out taskExternalWash, context.Request["taskExternalWash"]);
            sanitizeInteger(out taskSteamClean, context.Request["taskSteamClean"]);
            sanitizeInteger(out taskSteamCleanFQ, context.Request["taskSteamCleanFQ"]);
            sanitizeInteger(out taskPhoto, context.Request["taskPhoto"]);
            sanitizeInteger(out taskRepair, context.Request["taskRepair"]);
            // Not sanitising grade
            grade = context.Request["grade"];
            sanitizeString(out tabletUser, Context.User.Identity.Name);

            // TODO: IF ANY INPUT INVALID DO NOTHING, ELSE
            log.Info("setcontainertaskcompletion.aspx: " + "Marking selected tasks as complete for " + ContainerID + "...");
            log.Debug("setcontainertaskcompletion.aspx: " + "taskWash =  " + taskWash);
            log.Debug("setcontainertaskcompletion.aspx: " + "taskBaseWash =  " + taskBaseWash);
            log.Debug("setcontainertaskcompletion.aspx: " + "taskRoofWash =  " + taskRoofWash);
            log.Debug("setcontainertaskcompletion.aspx: " + "taskExternalWash =  " + taskExternalWash);
            log.Debug("setcontainertaskcompletion.aspx: " + "taskSteamClean =  " + taskSteamClean);
            log.Debug("setcontainertaskcompletion.aspx: " + "taskSteamCleanFQ =  " + taskSteamCleanFQ);
            log.Debug("setcontainertaskcompletion.aspx: " + "taskPhoto =  " + taskPhoto);
            log.Debug("setcontainertaskcompletion.aspx: " + "taskRepair =  " + taskRepair);
            log.Debug("setcontainertaskcompletion.aspx: " + "grade =  " + grade);

            // Compile dynamic SQL string
            string strSQL = "UPDATE dbo.containerStates SET ";
            if (taskWash == 30 || taskWash == 20)
            {
                strSQL += "taskWash='" + taskWash + "', ";
            }
            // Record time of completed wash
            if (taskWash == 30)
            {
                strSQL += "completedWash = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', ";
            }

            if (taskBaseWash == 30 || taskBaseWash == 20)
            {
                strSQL += "taskBaseWash='"+taskBaseWash+"', ";
            }
            // Record time of completed BaseWash
            if (taskBaseWash == 30)
            {
                strSQL += "completedBaseWash = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', ";
            }

            if (taskRoofWash == 30 || taskRoofWash == 20)
            {
                strSQL += "taskRoofWash='"+taskRoofWash+"', ";
            }
            // Record time of completed RoofWash
            if (taskRoofWash == 30)
            {
                strSQL += "completedRoofWash = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', ";
            }

            if (taskExternalWash == 30 || taskExternalWash == 20)
            {
                strSQL += "taskExternalWash='" + taskExternalWash + "', ";
            }
            // Record time of completed ExternalWash
            if (taskExternalWash == 30)
            {
                strSQL += "completedExternalWash = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', ";
            }

            if (taskSteamClean == 30 || taskSteamClean == 20)
            {
                strSQL += "taskSteamClean='" + taskSteamClean + "', ";
            }
            // Record time of completed SteamClean
            if (taskSteamClean == 30)
            {
                strSQL += "completedSteamClean = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', ";
            }

            if (taskSteamCleanFQ == 30 || taskSteamCleanFQ == 20)
            {
                strSQL += "taskSteamCleanFQ='" + taskSteamCleanFQ + "', ";
            }
            // Record time of completed SteamCleanFQ
            if (taskSteamCleanFQ == 30)
            {
                strSQL += "completedSteamCleanFQ = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', ";
            }

            if (taskPhoto == 30)
            {
                strSQL += "taskPhoto='"+taskPhoto+"', ";
            }
            if (taskRepair == 30)
            {
                strSQL += "taskRepair='" + taskRepair + "', ";
            }
            strSQL += "grade='" + grade + "', ";
            strSQL += "lastUpdated = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' , updatedBy = '" + Context.User.Identity.Name + "' WHERE containerStates.containerId='"+ContainerID+"'";
           
            // Execute SQL
            string _connection_string = ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString;
            SqlConnection _conn = new SqlConnection(_connection_string);
            _conn.Open();
            SqlCommand cmd = new SqlCommand(strSQL, _conn);
            cmd.ExecuteNonQuery();
            _conn.Close();

            Response.Write("Task completion recorded in the database successfully.");
     
        } // End Page_Load
    }
}