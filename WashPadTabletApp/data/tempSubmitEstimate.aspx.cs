﻿// Note:  This program requires the download and installation of log4Net
// 1. install the NuGet package manager (Tools>>Extension Manager>>Online Gallery>>NuGet Package Manager
// 2. right-click the project and choose Manage NuGet Packages
// 3. search for log4Net and install log4Net

using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
//using System.Data.OleDb;
//using System.Globalization;
using System.IO;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;


namespace WashPadTabletApp.data
{
    public partial class tempSubmitEstimate : System.Web.UI.Page
    {
        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            StreamReader sr = new StreamReader(strFilePath);
            string[] headers = sr.ReadLine().Split(',');
            DataTable dt = new DataTable();
            foreach (string header in headers)
            {
                dt.Columns.Add(header);
            }
            while (!sr.EndOfStream)
            {
                string[] rows = Regex.Split(sr.ReadLine(), ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                DataRow dr = dt.NewRow();
                for (int i = 0; i < headers.Length; i++)
                {
                    dr[i] = rows[i];
                }
                dt.Rows.Add(dr);
            }
            return dt;
        } 
 

        private string dataTableToHTML(DataTable DT)
        {
            string htmlReturnText = "";
            foreach (DataRow dataRow in DT.Rows)
            {
                htmlReturnText = htmlReturnText + "<br/>";
                foreach (var item in dataRow.ItemArray)
                {
                    htmlReturnText = htmlReturnText + item + ", ";
                }
            }
            return htmlReturnText;
        }

        private string datasetToHTML(DataSet dataSet)
        {
            string htmlReturnText = "";
            Int32 count = 0;
            // For each table in the DataSet, print the row values.
            foreach (DataTable table in dataSet.Tables)
            {
                count++;
                htmlReturnText = htmlReturnText + "<h2>Table " + count.ToString() + " within dataset:</h2>, ";

                htmlReturnText = htmlReturnText + "<table>";
                htmlReturnText = htmlReturnText + "<th>";
                foreach (DataColumn column in table.Columns)
                {
                    htmlReturnText = htmlReturnText + "<td>" + column.ColumnName + "</td>";
                }
                htmlReturnText = htmlReturnText + "</th>";

                foreach (DataRow row in table.Rows)
                {
                    htmlReturnText = htmlReturnText + "<tr>";
                    foreach (DataColumn column in table.Columns)
                    {
                        htmlReturnText = htmlReturnText + "<td>" + (row[column]) + "</td>";
                    }
                    htmlReturnText = htmlReturnText + "</tr>";
                }

                htmlReturnText = htmlReturnText + "</table>";
            }
            return htmlReturnText;
        }


        private void submitEstimateToDepotPRO(string containerID)
        {
            // Compile estimate and submit to DepotPRO via web service

            // 0) Init, Validation and business rules
                    // 0a) get container details
                    // 0b) validation - i.e. container found in WashDB and DepotPRO_DB
                    // 0c) supress any estimates according to business rules                                                  [TODO]
            // 1) Compile estimate line into cpEstimate
            // 2) Compile repair lines into cpEstimateLines
            // 3) Compile service lines into cpEstimateServices
                    // 3a) get any tasks that will equate to services
                    // 3b) derive service lines into cpEstimateServices                                                       [DOING]
            // 4) Compile photo lines into cpEstimatePhotos
            // 5) Compile stocktake lines into cpStockTake (stocktake not used, but required for web service call)
            // 6) Submit estimate via Web service
            // 7) update status of tasks in WashDB containerStates                                                            [TODO]


            // -----------------------------------------------------------------------------------------------------------
            // -------------------------------------- 0) Init, Validation and business rules -----------------------------
            // -----------------------------------------------------------------------------------------------------------
            var log = log4net.LogManager.GetLogger(this.GetType());
            log.Info("func submitEstimateToDepotPRO: " + "Generating estimate for " + containerID);
            DataTable dtContainerState = new DataTable();
            DataTable dtContainerFromDP = new DataTable();
            int PK_ESTIMATE = 0;

            // Get container row from WashDB containerStates
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                try { 
                    _conn.Open();
                    string strSQL = "SELECT * FROM containerStates WHERE containerId = '"+containerID+"'";
                    SqlCommand cmd = new SqlCommand(strSQL, _conn);
                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        dtContainerState.Load(reader);
                    }
                }
                catch(Exception ex){
                    // TODO: return errork
                    log.Error("func submitEstimateToDepotPRO: " + "Error reading from WashDB containerState: " + ex.Message);
                }
            }
            
            // Get further container attributes from DepotPRO cpContainer table
            // TODO: SQL METHOD NOT OPTIMAL, ENHANCEMENT TO RETRIEVE INFO FROM WEB SERVICE, WILL NEED SUPPORT FROM GLOBAL
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connDepotPRODB"].ConnectionString))
            {
                try { 
                    _conn.Open();
                    string strSQLDP = "SELECT [CONTAINER_NO],[ISO_CODE],[SIZE_CODE],[HEIGHT_CODE],[TYPE_CODE],[OWNER],[QUALITY_CODE],[LOAD_STATUS] FROM [DepotPRO_DP_TEST].[dbo].[cpContainer] WHERE CONTAINER_NO='" + containerID + "' ORDER BY REF_NO DESC";
                    SqlCommand cmd = new SqlCommand(strSQLDP, _conn);
                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        dtContainerFromDP.Load(reader);
                    } 
                }
                catch (Exception ex)
                {
                    // TODO: return error
                    log.Error("func submitEstimateToDepotPRO: " + "Error reading from DepotPRO_DP_TEST cpContainer: " + ex.Message);
                }
            }

            // Basic validation, adequate information to proceed with estimate submission?
            if (dtContainerFromDP.Rows.Count <1)
            {
                // No rows returned from DepotPRO, can't proceed with submission, leave the function
                log.Error("func submitEstimateToDepotPRO: " + "Container not found in DepotPRO, cancelling submission.");
                Response.Write("func submitEstimateToDepotPRO: " + "Container not found in DepotPRO, cancelling submission.");
                return;
            }
            // TODO: Apply Business Rules


            // -----------------------------------------------------------------------------------------------------------
            // ----------------------------------- END 0) Init, Validation and business rules ----------------------------
            // -----------------------------------------------------------------------------------------------------------



            // -----------------------------------------------------------------------------------------------------------
            // ----------------------------------------------- 1) Compile estimate ---------------------------------------
            // -----------------------------------------------------------------------------------------------------------

            // combine container state details from WashDB and container attributes from DepotPRO into new containerEstimate line
            // TODO: stored proc for this
            string strNewEstimateSQL = "";
            strNewEstimateSQL = strNewEstimateSQL + "INSERT INTO cpEstimate (";
            strNewEstimateSQL = strNewEstimateSQL + "ESTIMATE_NO, "; // use containerStateId for ESTIMATE_NO, gets overridden once loaded to DP
            strNewEstimateSQL = strNewEstimateSQL + "BRANCH_CODE, ";
            strNewEstimateSQL = strNewEstimateSQL + "USERNAME, ";
            strNewEstimateSQL = strNewEstimateSQL + "CONTAINER_NO, ";
            strNewEstimateSQL = strNewEstimateSQL + "OWNER, "; // [2] is the column for owner
            strNewEstimateSQL = strNewEstimateSQL + "ESTIMATE_DATE, ";
            strNewEstimateSQL = strNewEstimateSQL + "NOTES, ";  // [19] is the column for notes... TODO - better lookup!  probably use a dataview
            strNewEstimateSQL = strNewEstimateSQL + "INSPECTED_BY, ";
            strNewEstimateSQL = strNewEstimateSQL + "ISO_CODE, ";
            strNewEstimateSQL = strNewEstimateSQL + "SIZE_CODE, ";
            strNewEstimateSQL = strNewEstimateSQL + "HEIGHT_CODE, ";
            strNewEstimateSQL = strNewEstimateSQL + "TYPE_CODE, ";
            strNewEstimateSQL = strNewEstimateSQL + "UPDATE_BATCH_NO, ";
            strNewEstimateSQL = strNewEstimateSQL + "QUALITY_CODE, ";
            strNewEstimateSQL = strNewEstimateSQL + "LOAD_STATUS, "; 
            strNewEstimateSQL = strNewEstimateSQL + "FINALISE_ENTRY) ";

            strNewEstimateSQL = strNewEstimateSQL + "OUTPUT INSERTED.PK_ESTIMATE ";
        
            strNewEstimateSQL = strNewEstimateSQL + "VALUES (";
            strNewEstimateSQL = strNewEstimateSQL + "'" + dtContainerState.Rows[0][0] + "', "; // use containerStateId for ESTIMATE_NO, gets overridden once loaded to DP, avoids duplicate submissions
            strNewEstimateSQL = strNewEstimateSQL + "'PC', ";
            strNewEstimateSQL = strNewEstimateSQL + "'" + Context.User.Identity.Name + "', ";
            strNewEstimateSQL = strNewEstimateSQL + "'" + containerID + "', ";
            strNewEstimateSQL = strNewEstimateSQL + "'" + dtContainerState.Rows[0][2] + "', "; // OWNER   [2] is the column from WashDB containerState
            strNewEstimateSQL = strNewEstimateSQL + "'" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', ";
            strNewEstimateSQL = strNewEstimateSQL + "'" + dtContainerState.Rows[0][19] + "', ";  // [19] is the column for notes... TODO - better lookup!  probably use a dataview
            strNewEstimateSQL = strNewEstimateSQL + "'" + Context.User.Identity.Name + "', "; // INSPECTED_BY
            strNewEstimateSQL = strNewEstimateSQL + "'" + dtContainerFromDP.Rows[0][1] + "', "; // ISO_CODE
            strNewEstimateSQL = strNewEstimateSQL + "'" + dtContainerFromDP.Rows[0][2] + "', "; // SIZE_CODE
            strNewEstimateSQL = strNewEstimateSQL + "'" + dtContainerFromDP.Rows[0][3] + "', "; // HEIGHT_CODE
            strNewEstimateSQL = strNewEstimateSQL + "'" + dtContainerFromDP.Rows[0][4] + "', "; // TYPE_CODE
            strNewEstimateSQL = strNewEstimateSQL + "'0', "; // UPDATE_BATCH_NO - DONT KNOW WHAT THIS IS
            strNewEstimateSQL = strNewEstimateSQL + "'" + dtContainerFromDP.Rows[0][6] + "', "; // QUALITY_CODE  
            strNewEstimateSQL = strNewEstimateSQL + "'" + dtContainerFromDP.Rows[0][7] + "', "; // LOAD_STATUS  
            if (dtContainerState.Rows[0][20].ToString() == "complete")   // [20] is the column for quoteStatus
            {
                strNewEstimateSQL = strNewEstimateSQL + "'true')";  // FINALISE_ENTRY conditional on quoteStatus from tablet
            }
            else
            {
                strNewEstimateSQL = strNewEstimateSQL + "'false')";  // FINALISE_ENTRY conditional on quoteStatus from tablet
            }
            log.Debug("func submitEstimateToDepotPRO: " + "inserting estimate line into WashDB. strNewEstimateSQL: " + strNewEstimateSQL);

            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                try
                {
                    _conn.Open();
                    SqlCommand cmd = new SqlCommand(strNewEstimateSQL, _conn);
                    PK_ESTIMATE = (int)cmd.ExecuteScalar();
                    cmd.Dispose();
                    Response.Write("<br/>SQL INSERT executed. PK_ESTIMATE=" + PK_ESTIMATE);
                }
                catch (Exception ex)
                {
                    // TODO: log/alert/return error
                    Response.Write("<br/>Error with SQL INSERT: " + ex.Message);
                    log.Error("func submitEstimateToDepotPRO: " + "Error with SQL INSERT: " + ex.Message);
                }
            }
            // -----------------------------------------------------------------------------------------------------------
            // -------------------------------------------- END 1) Compile estimate --------------------------------------
            // -----------------------------------------------------------------------------------------------------------



            // -----------------------------------------------------------------------------------------------------------
            // ------------------------------------------- 2) Compile repair lines ---------------------------------------
            // -----------------------------------------------------------------------------------------------------------
            DataTable dtContainerRepairs = new DataTable();
            DataTable dtRepairCodes = new DataTable();
           
            // 2a) get repairLines from containerRepairs
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                try
                {
                    _conn.Open();
                    string strSQL = "SELECT * FROM containerRepairs WHERE containerId = '" + containerID + "'";
                    SqlCommand cmd = new SqlCommand(strSQL, _conn);
                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        dtContainerRepairs.Load(reader);
                    }
                }
                catch (Exception ex)
                {
                    // TODO: return error
                    log.Error("func submitEstimateToDepotPRO: " + "Error reading from WashDB containerRepairs: " + ex.Message);
                }
            }

            if (dtContainerRepairs.Rows.Count > 0)   // only if there are repair lines, build SQL and insert into cpEstimateLines
            {

                // 2b) get repair line detail from CSV
                dtRepairCodes = ConvertCSVtoDataTable(ConfigurationManager.AppSettings["RepairLinesDataFile"].ToString());
                //Response.Write(dataTableToHTML(dtRepairCodes));

                // 2c) derive fully-formed containerEstimateRepairLines rows
                DataView dvRepairCodes = new DataView(dtRepairCodes);
                string SQLInsertRepairLineFragment = "";

                DataView dvContainerRepairs = new DataView(dtContainerRepairs);  // dataview for subsequent filtering
                int lineNo = 0;
                foreach (DataRowView rowView in dvContainerRepairs)
                {
                    lineNo++;
                    DataRow repairRow = rowView.Row;
                    //Response.Write("FOUND REPAIR... CODE: " + repairRow["repairCode"] + ", DESC: " + repairRow["repairDescription"]);
                    dvRepairCodes.RowFilter = "ID = " + repairRow["repairCode"];   // Filter repair codes by ID, assume only results in 1 row
                    foreach (DataRowView rowV in dvRepairCodes)
                    {
                        DataRow rowRepairCode = rowV.Row;
                        //Response.Write(rowRepairCode["PANEL"]);         // compile SQL insert
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "(";
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + PK_ESTIMATE + ", ";
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + lineNo + ", ";
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + repairRow["damageLocation"] + "', "; // LOC_CODE
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["DAM"] + "', "; // DAM_CODE
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["RESP_CODE"] + "', "; // RESP_CODE
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["MAT"] + "', "; // MAT_CODE
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["EQTYPE_CODE"] + "', "; // EQTYPE_CODE
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["COMP"] + "', "; // COMP_CODE
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["REP"] + "', "; // REP_CODE
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["CAT_CODE"] + "', "; // CAT_CODE
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["Charge By"] + "', "; // CHARGE_BY
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["UOM"] + "', "; // UOM_CODE
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["QTY"] + "', "; // QTY
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["LENGTH"] + "', "; // LENGTH
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["HEIGHT"] + "', "; // HEIGHT
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["HOURS"] + "', "; // HOURS
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["MATERIAL"] + "', "; // MATERIAL
                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["Notes"] + "', "; // NOTES
                        if (dtContainerFromDP.Rows[0][5].ToString() == "MSK")
                        {
                            SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'" + rowRepairCode["TARIFF"]; // TARRIFDET_CODE - LAST LINE WITH NO COMMA , 
                        }
                        else
                        {
                            SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'"; // EMPTY TARRIFDET_CODE FOR NON-MSK - LAST LINE WITH NO COMMA , 
                        }

                        SQLInsertRepairLineFragment = SQLInsertRepairLineFragment + "'), ";
                    }
                }
                SQLInsertRepairLineFragment = SQLInsertRepairLineFragment.Substring(0, SQLInsertRepairLineFragment.Length - 2);  // remove trailing comma
                SQLInsertRepairLineFragment = "INSERT INTO cpEstimateLines (FK_ESTIMATE, LINE_NO, LOC_CODE, DAM_CODE, RESP_CODE, MAT_CODE, EQTYPE_CODE, COMP_CODE, REP_CODE, CAT_CODE, CHARGE_BY,UOM_CODE,QTY,LENGTH,HEIGHT,HOURS,MATERIAL, NOTES,TARIFFDET_CODE) VALUES " + SQLInsertRepairLineFragment;
                Response.Write("SQLInsertRepairLineFragment: " + SQLInsertRepairLineFragment);
                using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
                {
                    try
                    {
                        _conn.Open();
                        SqlCommand cmd = new SqlCommand(SQLInsertRepairLineFragment, _conn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        Response.Write("<br/>SQL REPAIR LINES INSERT executed.");
                    }
                    catch (Exception ex)
                    {
                        // TODO: log/alert/return error
                        Response.Write("<br/>Error with SQL REPAIR LINE INSERT: " + ex.Message);
                    }
                }
            }
            else
            {
                Response.Write("No repair lines for this estimate.");
                log.Info("No repair lines for this estimate.");
            }
            // -----------------------------------------------------------------------------------------------------------
            // ---------------------------------------- END 2) Compile repair lines --------------------------------------
            // -----------------------------------------------------------------------------------------------------------



            // -----------------------------------------------------------------------------------------------------------
            // ---------------------------------------- 3) Compile service lines -----------------------------------------
            // -----------------------------------------------------------------------------------------------------------
            // 3a) get any tasks that will equate to services
            DataView dvContainerState = new DataView(dtContainerState);  // dataview for subsequent filtering
            string strSQLServiceInsertLines = "";
            List<string> strSQLValues = new List<string>();

            foreach (DataRowView rowView in dvContainerState) {
                DataRow rowState = rowView.Row;
                // ADD Each Service
                if (rowState["taskWash"].ToString() == "10") {
                    strSQLValues.Add("(" + PK_ESTIMATE + ",'WASH')");
                }

                if (rowState["taskRepair"].ToString() == "10")
                {
                    strSQLValues.Add("(" + PK_ESTIMATE + ",'Repair')");
                }

                if (rowState["taskPhoto"].ToString() == "10")
                {
                    strSQLValues.Add("(" + PK_ESTIMATE + ",'Photo')");
                }
                //TODO: Validate service submission with Vathsan
            }
            if (strSQLValues.Count > 0)
            {
                strSQLServiceInsertLines = "INSERT INTO cpEstimateServices (FK_ESTIMATE,SERVICE_CODE) VALUES " + string.Join(", ", strSQLValues.ToArray());
                Response.Write("<br><br>strSQLServiceInsertLines = " + strSQLServiceInsertLines);
                using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
                {
                    try
                    {
                        _conn.Open();
                        SqlCommand cmd = new SqlCommand(strSQLServiceInsertLines, _conn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        Response.Write("<br/>SQL SERVICE LINES INSERT executed.<br>");
                    }
                    catch (Exception ex)
                    {
                        // TODO: log/alert/return error
                        Response.Write("<br/>Error with SQL SERVICE LINE INSERT: " + ex.Message);
                    }
                }
            }
            else
            {
                Response.Write("<br><br>no service lines for this estimate.");
            }
            // -----------------------------------------------------------------------------------------------------------
            // ------------------------------------- END 3) Compile service lines ----------------------------------------
            // -----------------------------------------------------------------------------------------------------------



            // -----------------------------------------------------------------------------------------------------------
            // ----------------------------------------- 4) Compile photo lines ------------------------------------------
            // -----------------------------------------------------------------------------------------------------------
            DataTable dtContainerPhotos = new DataTable();
            string SQLInsertPhotoFragment = "";
            List<SqlParameter> cParameters = new List<SqlParameter>();
            using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                try
                {
                    _conn.Open();
                    string strSQL = "SELECT * FROM containerPhotos WHERE containerId = '" + containerID + "'";
                    SqlCommand cmd = new SqlCommand(strSQL, _conn);
                    using (SqlDataReader reader = cmd.ExecuteReader()) {
                        dtContainerPhotos.Load(reader);
                    }
                }
                catch (Exception ex)
                {
                    // TODO: return error
                    log.Error("func submitEstimateToDepotPRO: " + "Error reading from WashDB containerPhotos: " + ex.Message);
                }
            }

            if (dtContainerPhotos.Rows.Count > 0)   // only if there are photo lines, build SQL and insert into cpEstimatePhotos
            {
                Response.Write("<br>" + dtContainerPhotos.Rows.Count + " photos found.");
                DataView dvContainerPhotos = new DataView(dtContainerPhotos);  // dataview for subsequent filtering
                int lineNo = 0;
                foreach (DataRowView rowView in dvContainerPhotos)
                {
                    DataRow photoRow = rowView.Row;
                    byte[] photoBytes = (byte[])photoRow["PHOTO"];
                    SQLInsertPhotoFragment = SQLInsertPhotoFragment + "(";
                    SQLInsertPhotoFragment = SQLInsertPhotoFragment + "" + PK_ESTIMATE + ", "; // ESTIMATE
                    SQLInsertPhotoFragment = SQLInsertPhotoFragment + "@photo" + lineNo.ToString(); // PHOTO - parameterised
                    SQLInsertPhotoFragment = SQLInsertPhotoFragment + "), ";
                    cParameters.Add(new SqlParameter("@photo" + lineNo.ToString(), @photoBytes));
                    lineNo++;
                }

                SQLInsertPhotoFragment = SQLInsertPhotoFragment.Substring(0, SQLInsertPhotoFragment.Length - 2);  // remove trailing comma
                SQLInsertPhotoFragment = "INSERT INTO cpEstimatePhotos (FK_ESTIMATE,PHOTO) VALUES " + SQLInsertPhotoFragment;
                Response.Write("<br>SQLInsertPhotoFragment: " + SQLInsertPhotoFragment);

                using (SqlConnection _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
                {
                    try
                    {
                        _conn.Open();
                        SqlCommand cmd = new SqlCommand(SQLInsertPhotoFragment, _conn);
                        // add photo data as parameters
                        if (cParameters.Count != 0)
                        {
                            cmd.Parameters.AddRange(cParameters.ToArray());
                        }
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        Response.Write("<br/>SQL PHOTO LINES INSERT executed.");
                        }
                    catch (Exception ex)
                    {
                        // TODO: log/alert/return error
                        log.Error("func submitEstimateToDepotPRO: " + "Error inserting photos into cpEstimatePhotos: " + ex.Message);
                        Response.Write("<br/>Error with SQL PHOTO LINE INSERT: " + ex.Message);
                    }
                } // End using SqlConnection
            }
            else
            {
                // No photos for this estimate
                Response.Write("<br>No photo lines for this estimate.");
                log.Info("No photo lines for this estimate.");
            }
            // -----------------------------------------------------------------------------------------------------------
            // --------------------------------------- END 4) Compile photo lines ----------------------------------------
            // -----------------------------------------------------------------------------------------------------------



            // -----------------------------------------------------------------------------------------------------------
            // ------------------------------------ 6) Submit estimate via Web service -----------------------------------
            // -----------------------------------------------------------------------------------------------------------
            DataTable dtEstimates1 = new DataTable();
            DataTable dtEstimateLines1 = new DataTable();
            DataTable dtEstimateServices1 = new DataTable();
            DataTable dtEstimatePhotos = new DataTable();
            DataTable dtStocktake = new DataTable();

            // Fill Datatables from DB
            using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
            {
                dbConnection.Open();

                SqlCommand cmd = new SqlCommand("SELECT * FROM cpEstimate WHERE PK_ESTIMATE = " + PK_ESTIMATE, dbConnection);
                using (SqlDataAdapter a = new SqlDataAdapter(cmd))
                {
                    a.Fill(dtEstimates1);
                }

                cmd = new SqlCommand(" SELECT cpEstimateLines.* from cpEstimateLines inner join cpEstimate on (cpEstimate.PK_ESTIMATE = cpEstimateLines.FK_ESTIMATE) " + " WHERE cpEstimate.PK_ESTIMATE = " + PK_ESTIMATE, dbConnection);
                using (SqlDataAdapter a = new SqlDataAdapter(cmd))
                {
                    a.Fill(dtEstimateLines1);
                }

                cmd = new SqlCommand(" SELECT cpEstimateServices.* from cpEstimateServices inner join cpEstimate on (cpEstimate.PK_ESTIMATE = cpEstimateServices.FK_ESTIMATE) " + " WHERE (cpEstimate.PK_ESTIMATE = " + PK_ESTIMATE + ") and (SERVICE_CODE <> '') and (SERVICE_CODE is not null)", dbConnection);
                using (SqlDataAdapter a = new SqlDataAdapter(cmd))
                {
                    a.Fill(dtEstimateServices1);
                }

                cmd = new SqlCommand("SELECT cpEstimatePhotos.* from cpEstimatePhotos inner join cpEstimate on (cpEstimate.PK_ESTIMATE = cpEstimatePhotos.FK_ESTIMATE) " + " WHERE (cpEstimate.PK_ESTIMATE = " + PK_ESTIMATE + ")", dbConnection);
                using (SqlDataAdapter a = new SqlDataAdapter(cmd))
                {
                    a.Fill(dtEstimatePhotos);
                }

                cmd = new SqlCommand(" SELECT * from cpStocktake", dbConnection);
                using (SqlDataAdapter a = new SqlDataAdapter(cmd))
                {
                    a.Fill(dtStocktake);
                }
            }

            // Populate dataSet from dataTables
            DataSet ds = new DataSet();
            ds.Tables.Add(dtEstimates1);
            ds.Tables.Add(dtEstimateLines1);
            ds.Tables.Add(dtEstimateServices1);
            ds.Tables.Add(dtEstimatePhotos);
            ds.Tables.Add(dtStocktake);

            // Pass dataset to web service
            try {
                string mystring = "";
                DepotProHHWebService.DepotProHHeldSvcSoapClient DPSoapClient = new DepotProHHWebService.DepotProHHeldSvcSoapClient();
                bool success = DPSoapClient.UploadEstimatesVersion(out mystring, ds, "3.11"); // 3.10
                if (success)
                {
                    log.Info("Successful submission of estimate to DepotPRO web service.  Web service reponse: " + mystring);
                    Response.Write("Successful submission of estimate to DepotPRO web service. Web service reponse: " + mystring);
                }
                else
                {
                    log.Error("Failed submission of estimate to DepotPRO web service. Web service reponse: " + mystring);
                    Response.Write("Failed submission of estimate to DepotPRO web service. Web service reponse: " + mystring);
                }
            }
            catch (Exception ex) {
                log.Error("Error with web service submission: " + ex.Message);
            }

            // -----------------------------------------------------------------------------------------------------------
            // ------------------------------ END 6) Submit estimate via Web service -------------------------------------
            // -----------------------------------------------------------------------------------------------------------


        } // End function submitEstimateToDepotPRO



        protected void Page_Load(object sender, EventArgs e)
        {
            var context = HttpContext.Current;
            submitEstimateToDepotPRO(context.Request["containerID"]);
        }
    }
}