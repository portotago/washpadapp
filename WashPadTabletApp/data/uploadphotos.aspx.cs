﻿// Note:  This program requires the download and installation of log4Net
// 1. install the NuGet package manager (Tools>>Extension Manager>>Online Gallery>>NuGet Package Manager
// 2. right-click the project and choose Manage NuGet Packages
// 3. search for log4Net and install log4Net

using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace WashPadTabletApp.data
{
    public partial class uploadphotos : System.Web.UI.Page
    {
        public static byte[] GetPhoto(string filePath)
        {
            FileStream stream = new FileStream(
                filePath, FileMode.Open, FileAccess.Read);
            BinaryReader reader = new BinaryReader(stream);

            byte[] photo = reader.ReadBytes((int)stream.Length);

            reader.Close();
            stream.Close();

            return photo;
        }

        private void resizeCompressSaveImage(System.Drawing.Image img, string fileName, long quality, int maxImageDimension)
        {
            // Get the image's original width and height
            int originalWidth = img.Width;
            int originalHeight = img.Height;
            int newWidth = 0;
            int newHeight = 0;

            float ratio = (float)originalWidth / (float)originalHeight;

            if (ratio > 1)
            {
                newWidth = maxImageDimension;
                newHeight = Convert.ToInt32(maxImageDimension / ratio);
            }
            else
            {
                newWidth = Convert.ToInt32(maxImageDimension * ratio);
                newHeight = maxImageDimension;
            }

            Response.Write("originalWidth: " + originalWidth);
            Response.Write("originalHeight: " + originalHeight);
            Response.Write("ratio: " + ratio.ToString());
            Response.Write("newWidth: " + newWidth);
            Response.Write("newHeight: " + newHeight);
            img = ResizeImage(img, newWidth, newHeight);
            EncoderParameters parameters = new EncoderParameters(1);
            parameters.Param[0] = new EncoderParameter(Encoder.Quality, quality);
            img.Save(fileName, GetCodecInfo("image/jpeg"), parameters);
        } // end resizeCompressSaveImage

        private static ImageCodecInfo GetCodecInfo(string mimeType)
        {
            foreach (ImageCodecInfo encoder in ImageCodecInfo.GetImageEncoders())
                if (encoder.MimeType == mimeType)
                    return encoder;
            throw new ArgumentOutOfRangeException(
                string.Format("'{0}' not supported", mimeType));
        }

        public static Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Get form submission
            var log = log4net.LogManager.GetLogger(this.GetType());
            log.Info("uploadphotos.aspx: " + "called.");
            var context = HttpContext.Current;

            var fileSaveFolder = ConfigurationManager.AppSettings["TemporaryPhotoFileStore"].ToString(); 

            try
            {
                if (context.Request.Files.AllKeys.Any())
                {
                    // create folder for this container
                    var selectedContainer = context.Request["containerNumber"];
                    var selectedOID = context.Request["oid"];
                    log.Info("uploadphotos.aspx: " + "Uploading photos for " + context.Request["containerNumber"]);
                    System.IO.Directory.CreateDirectory(fileSaveFolder + selectedContainer);

                    // Work each of the uploaded images from Files collection
                    foreach (string uploadedphoto in Request.Files)
                    {
                        // Save photo to filesystem
                        var httpPostedFile = context.Request.Files[uploadedphoto];
                        var fileSavePath = fileSaveFolder + "\\" + selectedContainer + "\\original_" + httpPostedFile.FileName;
                        log.Info("uploadphotos.aspx: " + "Handling photo " + httpPostedFile.FileName);
                        httpPostedFile.SaveAs(fileSavePath);
                        Response.Write("Photo " + httpPostedFile.FileName + " saved to server filesystem successfully. ");

                        // Generate thumbnail and resized photo files
                        System.Drawing.Image myImg = System.Drawing.Image.FromFile(fileSavePath);
                        var fileSavePathThumbnail = fileSaveFolder + "\\" + selectedContainer + "\\thumb_" + httpPostedFile.FileName;
                        var fileSavePathResized = fileSaveFolder + "\\" + selectedContainer + "\\resized_" + httpPostedFile.FileName;
                        resizeCompressSaveImage(myImg, fileSavePathThumbnail, Convert.ToInt32(ConfigurationManager.AppSettings["photoResizeQuality"]), Convert.ToInt32(ConfigurationManager.AppSettings["photoThumbnailMaxDimension"])); // quality can be 1 to 100
                        resizeCompressSaveImage(myImg, fileSavePathResized, Convert.ToInt32(ConfigurationManager.AppSettings["photoResizeQuality"]), Convert.ToInt32(ConfigurationManager.AppSettings["photoResizedMaxDimension"])); // quality can be 1 to 100

                        // Upload original photo, thumbnail and resized files to DB
                        using (SqlConnection dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["connWashDB"].ConnectionString))
                        {
                            dbConnection.Open();
                            // Turn each into a byte array
                            byte[] photo = GetPhoto(fileSavePath);
                            byte[] photoResized = GetPhoto(fileSavePathResized);
                            byte[] photoThumb = GetPhoto(fileSavePathThumbnail);

                            SqlCommand command = new SqlCommand("INSERT INTO containerPhotos (CONTAINERID, OID, PHOTO, CREATED, LASTMODIFIED, PHOTORESIZED, PHOTOTHUMB) Values('" + selectedContainer + "', '" + selectedOID + "',@Photo, '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "', @PhotoResized, @PhotoThumb)", dbConnection);
                            command.Parameters.Add("@Photo", SqlDbType.Image, photo.Length).Value = photo;
                            command.Parameters.Add("@PhotoResized", SqlDbType.Image, photoResized.Length).Value = photoResized;
                            command.Parameters.Add("@PhotoThumb", SqlDbType.Image, photoThumb.Length).Value = photoThumb;
                            command.ExecuteNonQuery();
                            Response.Write("Photo " + httpPostedFile.FileName + " uploaded to database successfully. ");
                            dbConnection.Close();
                        }
                    } // End foreach photo
                }
                else
                {
                    // No photos received
                    Response.Write("No photos found to upload.");
                    log.Info("uploadphotos.aspx: " + "No photos found to upload.");
                    return;
                }
            }
            catch (Exception ex)
            {
                log.Error("uploadphotos.aspx: " + "An error occurred while uploading the file. Error Message: " + ex.Message);
                Response.Write("An error occurred while uploading the file. Error Message: " + ex.Message);
                return;
            }

        } // End Page Load
    }
}