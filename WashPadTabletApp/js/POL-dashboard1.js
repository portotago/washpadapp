﻿// --------------------------------------------------------------------------------------------
// --------------------------------------------- CONFIG ---------------------------------------
// --------------------------------------------------------------------------------------------
var containersListURL = "data/getcontainers.aspx";
var RPICURL = "data/getwashpadrpic.aspx";
var activityStatsURL = "data/getwashpadactivitystats.aspx";
var redirectURL = "default.aspx";
var heatbeatFileURL = "heartbeat/alive.txt";
var heartBeatFreq = 10  // seconds delay in checking connectivity
var statusFlashDurationMS = 1111;
var refreshListInterval = 30; // seconds delay before checking the list of containers and status in WashDB
var RPICMinValue = 0; // values this low will be green (lower will be grey)
var RPICMaxValue = 5; // values this high will be red (higher will also be red) - this value represents a REALLY BAD washpad

var ContainersCache = "";
var RPIC = "";



// --------------------------------------------------------------------------------------------

// ------------------- initiate heartbeat ----------------------------
setInterval(function () {
    $.ajax({
        url: heatbeatFileURL + '?random=' + Math.random().toString(36).substring(7),
        type: 'HEAD',
        error: function () {
            //file can't be retrieved, assume connection problem
            $.blockUI({
                message: '<h1 style="margin:.5em">Error with connection to POL, check 3G connection and/or VPN.</h1><p style="font-size:16pt;">Retrying in ' + heartBeatFreq + ' seconds...</p>',
                title: "In-house moves app",
                css: {
                    padding: 0,
                    margin: 0,
                    width: '70%',
                    top: '20%',
                    left: '15%',
                    textAlign: 'center',
                    color: '#f00',
                    border: '3px solid #aaa',
                    backgroundColor: '#fff',
                    cursor: 'wait'
                }
            });
        },
        success: function () {
            //file exists
            $.unblockUI();
        }
    });
}, heartBeatFreq * 1000);



// --------------------------------------------------------------------------------------------
// ---------------------------------------- FUNCTIONS -----------------------------------------
// --------------------------------------------------------------------------------------------

function StatusFlash(message) {
    $("#StatusContainer").show();
    $("#StatusBar").html(message);
    $("#StatusBar").hide().fadeIn(500).delay(statusFlashDurationMS).fadeOut(1000).hide(function () {
        $("#StatusContainer").hide();
    });
}  // end StatusFlash


// function to retrieve querystring parameters
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function refreshContainerCache() {
    //console.log("Refreshing cache of containers...");
    var json = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': containersListURL + "?random=" + Math.random().toString(36).substring(7),
        'dataType': "json",
        'success': function (data) {
            console.log("Successfully refreshed containers cache");
            //console.log(JSON.stringify(data));
            json = data;
        }
    });
    return json;
}

function refreshRPIC() {
    var json = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': RPICURL + "?random=" + Math.random().toString(36).substring(7),
        'dataType': "json",
        'success': function (data) {
            console.log("Successfully refreshed RPIC");
            //console.log(JSON.stringify(data));
            json = data;
        }
    });
    return json;
} // end refreshRPIC

function refreshActivityStats() {
    var json = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': activityStatsURL + "?random=" + Math.random().toString(36).substring(7),
        'dataType': "json",
        'success': function (data) {
            console.log("Successfully refreshed Activity Stats");
            //console.log(JSON.stringify(data));
            json = data;
        }
    });
    return json;
} // end refreshActivityStats


// --------------------------------------------------------------------------------------------
function toTrafficLight(min, max, val) {
    // returns a hex color value string

    // config for the desired traffic light coloring -  note 5 color points in RGB

    // less than minimum
    subR = 20;
    subG = 101;
    subB = 179;
    // minimum
    minR = 33;
    minG = 231;
    minB = 21;
    // mid point
    midR = 222; //232
    midG = 192; //202
    midB = 9;
    // maximum
    maxR = 224;
    maxG = 12;
    maxB = 24;
    // beyond maximum
    //  xtrR = 255;
    //  xtrG = 0;
    //   xtrB = 255;
    xtrR = 255;
    xtrG = 0;
    xtrB = 0;

    var theR = 0;
    var theG = 0;
    var theB = 0;
    mid = (min + max) / 2

    if (val < min) {
        console.log('val is less that minimum');
        theR = subR;
        theG = subG;
        theB = subB;
    }
    else if (val == min) {
        console.log('val is minimum');
        theR = minR;
        theG = minG;
        theB = minB;
    }
    else if (val > min && val < mid) {
        console.log('val between minimum and mid point');
        range = mid - min;
        distance = val - min;
        percentofgradient = (1 / range) * distance;
        console.log('percent between min and mid: ' + percentofgradient);
        theR = minR + (midR - minR) * percentofgradient;
        theG = minG + (midG - minG) * percentofgradient;
        theB = minB + (midB - minB) * percentofgradient;
    }
    else if (val == mid) {
        console.log('val equals mid point');
        theR = midR;
        theG = midG;
        theB = midB;
    }
    else if (val > mid && val < max) {
        console.log('val between mid point and maximum');
        range = max - mid;
        distance = val - mid;
        percentofgradient = (1 / range) * distance;
        console.log('percent between mid and max: ' + percentofgradient);
        theR = midR + (maxR - midR) * percentofgradient;
        theG = midG + (maxG - midG) * percentofgradient;
        theB = midB + (maxB - midB) * percentofgradient;
    }
    else if (val == max) {
        console.log('val equals maximum');
        theR = maxR;
        theG = maxG;
        theB = maxB;
    }
    else if (val > max) {
        console.log('val beyond maximum');
        theR = xtrR;
        theG = xtrG;
        theB = xtrB;
    }

    console.log(parseInt(theR));
    console.log(parseInt(theG));
    console.log(parseInt(theB));

    return '#' + pad(parseInt(theR).toString(16), 2) + pad(parseInt(theG).toString(16), 2) + pad(parseInt(theB).toString(16), 2);
}

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function setRPICcolors() {
    // set traffic light colors to RPIC values
    $('.rpicvalue').each(function (index) {
        var paramVal = $(this).text();
        $(this).parent().css('background', toTrafficLight(RPICMinValue, RPICMaxValue, paramVal));
    });
}


function renderActivityStatsTable() {
    //refresh cache of container numbers in case has been udpated
    ActivityStats = refreshActivityStats();

    $('#ActivityStats').html("");

    $.each(ActivityStats, function (i, v) {
        // only expect one of these
        $('#ActivityStats').append
        var htmlTable = "";
        htmlTable += "<table id='ActivityStatsData'>";

        htmlTable += "<tr>";
        htmlTable += "<td class='blank'></td>";
        htmlTable += "<th colspan='3'>Today</th>";
        htmlTable += "<th colspan='3'>Yesterday</th>";
        htmlTable += "</tr>";

        htmlTable += "<tr>";
        htmlTable += "<td class='blank'></td>";
        htmlTable += "<th colspan='1'>1st Shift</th>";
        htmlTable += "<th colspan='1'>2nd Shift</th>";
        htmlTable += "<th colspan='1'>24hr</th>";
        htmlTable += "<th colspan='1'>1st Shift</th>";
        htmlTable += "<th colspan='1'>2nd Shift</th>";
        htmlTable += "<th colspan='1'>24hr</th>";
        htmlTable += "</tr>";

        htmlTable += "<tr>";
        htmlTable += "<th>Boxes inspected</th>";
        htmlTable += "<td>" + v.BoxesInspectedWXDay1st + "</td>";
        htmlTable += "<td>" + v.BoxesInspectedWXDay2nd + "</td>";
        htmlTable += "<td>" + v.BoxesInspectedWXDay24hrs + "</td>";
        htmlTable += "<td>" + v.BoxesInspectedWXDayPrev1st + "</td>";
        htmlTable += "<td>" + v.BoxesInspectedWXDayPrev2nd + "</td>";
        htmlTable += "<td>" + v.BoxesInspectedWXDayPrev24hrs + "</td>";
        htmlTable += "</tr>";

      //  htmlTable += "<tr class='separator'>";
      //  htmlTable += "<th>Need repair W1</th>";
     //   htmlTable += "<td>" + v.BoxesNeedRepairW1Day1st + "</td>";
      //  htmlTable += "<td>" + v.BoxesNeedRepairW1Day2nd + "</td>";
     //   htmlTable += "<td>" + v.BoxesNeedRepairW1Day24hrs + "</td>";
     //   htmlTable += "<td>" + v.BoxesNeedRepairW1DayPrev1st + "</td>";
     //   htmlTable += "<td>" + v.BoxesNeedRepairW1DayPrev2nd + "</td>";
     //   htmlTable += "<td>" + v.BoxesNeedRepairW1DayPrev24hrs + "</td>";
    //    htmlTable += "</tr>";

     //   htmlTable += "<tr>";
     //   htmlTable += "<th>Need repair W2</th>";
     //   htmlTable += "<td>" + v.BoxesNeedRepairW2Day1st + "</td>";
    //    htmlTable += "<td>" + v.BoxesNeedRepairW2Day2nd + "</td>";
    //    htmlTable += "<td>" + v.BoxesNeedRepairW2Day24hrs + "</td>";
    //    htmlTable += "<td>" + v.BoxesNeedRepairW2DayPrev1st + "</td>";
    //    htmlTable += "<td>" + v.BoxesNeedRepairW2DayPrev2nd + "</td>";
    //    htmlTable += "<td>" + v.BoxesNeedRepairW2DayPrev24hrs + "</td>";
     //   htmlTable += "</tr>";

    //    htmlTable += "<tr>";
   //     htmlTable += "<th>Need repair W3</th>";
    //    htmlTable += "<td>" + v.BoxesNeedRepairW3Day1st + "</td>";
   //     htmlTable += "<td>" + v.BoxesNeedRepairW3Day2nd + "</td>";
   //     htmlTable += "<td>" + v.BoxesNeedRepairW3Day24hrs + "</td>";
   //     htmlTable += "<td>" + v.BoxesNeedRepairW3DayPrev1st + "</td>";
   //     htmlTable += "<td>" + v.BoxesNeedRepairW3DayPrev2nd + "</td>";
   //     htmlTable += "<td>" + v.BoxesNeedRepairW3DayPrev24hrs + "</td>";
   //     htmlTable += "</tr>";

   //     htmlTable += "<tr>";
   //     htmlTable += "<th>Need repair W4</th>";
   //     htmlTable += "<td>" + v.BoxesNeedRepairW4Day1st + "</td>";
  //      htmlTable += "<td>" + v.BoxesNeedRepairW4Day2nd + "</td>";
   //     htmlTable += "<td>" + v.BoxesNeedRepairW4Day24hrs + "</td>";
   //     htmlTable += "<td>" + v.BoxesNeedRepairW4DayPrev1st + "</td>";
   //     htmlTable += "<td>" + v.BoxesNeedRepairW4DayPrev2nd + "</td>";
   //     htmlTable += "<td>" + v.BoxesNeedRepairW4DayPrev24hrs + "</td>";
   //     htmlTable += "</tr>";

        htmlTable += "<tr>";
        htmlTable += "<th>Need repair</th>";
        htmlTable += "<td>" + v.BoxesNeedRepairWXDay1st + "</td>";
        htmlTable += "<td>" + v.BoxesNeedRepairWXDay2nd + "</td>";
        htmlTable += "<td>" + v.BoxesNeedRepairWXDay24hrs + "</td>";
        htmlTable += "<td>" + v.BoxesNeedRepairWXDayPrev1st + "</td>";
        htmlTable += "<td>" + v.BoxesNeedRepairWXDayPrev2nd + "</td>";
        htmlTable += "<td>" + v.BoxesNeedRepairWXDayPrev24hrs + "</td>";
        htmlTable += "</tr>";

        htmlTable += "<tr>";
        htmlTable += "<th>Boxes repaired</th>";
        htmlTable += "<td>" + v.BoxesRepairedWXDay1st + "</td>";
        htmlTable += "<td>" + v.BoxesRepairedWXDay2nd + "</td>";
        htmlTable += "<td>" + v.BoxesRepairedWXDay24hrs + "</td>";
        htmlTable += "<td>" + v.BoxesRepairedWXDayPrev1st + "</td>";
        htmlTable += "<td>" + v.BoxesRepairedWXDayPrev2nd + "</td>";
        htmlTable += "<td>" + v.BoxesRepairedWXDayPrev24hrs + "</td>";
        htmlTable += "</tr>";

        htmlTable += "<tr>";
        htmlTable += "<th>Individual repairs</th>";
        htmlTable += "<td>" + v.IndividualRepairsWXDay1st + "</td>";
        htmlTable += "<td>" + v.IndividualRepairsWXDay2nd + "</td>";
        htmlTable += "<td>" + v.IndividualRepairsWXDay24hrs + "</td>";
        htmlTable += "<td>" + v.IndividualRepairsWXDayPrev1st + "</td>";
        htmlTable += "<td>" + v.IndividualRepairsWXDayPrev2nd + "</td>";
        htmlTable += "<td>" + v.IndividualRepairsWXDayPrev24hrs + "</td>";
        htmlTable += "</tr>";

        htmlTable += "<tr>";
        htmlTable += "<th>Boxes washed</th>";
        htmlTable += "<td>" + v.BoxesWashedWXDay1st + "</td>";
        htmlTable += "<td>" + v.BoxesWashedWXDay2nd + "</td>";
        htmlTable += "<td>" + v.BoxesWashedWXDay24hrs + "</td>";
        htmlTable += "<td>" + v.BoxesWashedWXDayPrev1st + "</td>";
        htmlTable += "<td>" + v.BoxesWashedWXDayPrev2nd + "</td>";
        htmlTable += "<td>" + v.BoxesWashedWXDayPrev24hrs + "</td>";
        htmlTable += "</tr>";
        
        htmlTable += "</table>";

        $('#ActivityStats').append(htmlTable);

    });



} // end renderActivityStatsTable

function renderContainersList() {
    //refresh cache of container numbers in case has been udpated
    ContainersCache = refreshContainerCache();
    RPIC = refreshRPIC();

    $('#containerlistW1').html("");
    $('#containerlistW2').html("");
    $('#containerlistW3').html("");
    $('#containerlistW4').html("");

    $.each(RPIC, function (i, v) {
        if (v.RPIC < 0) {
            $('#RPIC-' + v.WashpadId).html('RPIC: N/A<span class="rpicvalue" style="display:none;">-1</span>');
        } else {
            $('#RPIC-' + v.WashpadId).html('RPIC: <span class="rpicvalue">' + v.RPIC + '</span>');
        }
    });


    $.each(ContainersCache, function (i, v) {

        // Determine whether inspection/wash/repair is applicable and/or required and display buttons accordingly

        // Determine state of wash - check applicable wash task statuses
        function isTodo(taskStatus) {
            if (taskStatus > 19 && taskStatus < 25) {
                return true;
            } else {
                return false
            }
        }

        // Determine the state of the wash button
        var washState = "0";
        if ((v.taskInspect > 30) || (v.isReefer == 0)) {
            if (isTodo(v.taskWash) || isTodo(v.taskBaseWash) || isTodo(v.taskRoofWash) || isTodo(v.taskExternalWash) || isTodo(v.taskSteamClean) || isTodo(v.taskSteamCleanFQ)) {
                // if any wash tasks are identified but not done,  status is TO WASH 20
                washState = "20";
            }
            if (v.taskWash >= "30" || v.taskBaseWash >= "30" || v.taskRoofWash >= "30" || v.taskExternalWash >= "30" || v.taskSteamClean >= "30" || v.taskSteamCleanFQ >= "30") {
                // if any wash tasks have been completed (30 or 40),  status is WASHING 25
                washState = "25";
            }
            if ((v.taskWash >= "30" || v.taskWash == 0) && (v.taskBaseWash >= "30" || v.taskBaseWash == 0) && (v.taskRoofWash >= "30" || v.taskRoofWash == 0) && (v.taskExternalWash >= "30" || v.taskExternalWash == 0) && (v.taskSteamClean >= "30" || v.taskSteamClean == 0) && (v.taskSteamCleanFQ >= "30" || v.taskSteamCleanFQ == 0)) {
                // if ALL wash tasks have either been completed 30/40, or are zero, then status is COMPELTED 40
                washState = "40";
            }
            if (v.taskWash == 0 && v.taskBaseWash == 0 && v.taskRoofWash == 0 && v.taskExternalWash == 0 && v.taskSteamClean == 0 && v.taskSteamCleanFQ == 0) {
                //if ALL wash tasks are zero, status is NOT APPLICABLE 0
                washState = "0";
            }
        }

        // build up HTML for container line
        if (v.quoteStatus == "incomplete" || v.taskMoveToRepair > 0) {
            // a submitted estimate is failing to go to DepotPRO, so indicate this in GUI
            containerLineHTML = "<div id=\"" + v.id + "\" class=\"container incompleteQuote\">";
        } else {
            containerLineHTML = "<div id=\"" + v.id + "\" class=\"container\">";
        }

        if (v.taskInspect == 30) {
            // Task is in submission state, display this in GUI
            if (v.isReefer == 1) {
                containerLineHTML = containerLineHTML + "<img src='images/icon-snowflake.png' width='32' style='float:left;margin:.35em -.2em 0 .2em' />";
                containerLineHTML = containerLineHTML + "<span class='containerid reefer'>" + v.id + "</span>";
            } else {
                containerLineHTML = containerLineHTML + "<img src='images/icon-dry.png' width='32' height='32' style='float:left;margin:.35em -.2em 0 .2em' />";
                containerLineHTML = containerLineHTML + "<span class='containerid dry'>" + v.id + "</span>";
            }
            containerLineHTML = containerLineHTML + "<span><img src='images/loading.png' style='padding:0 .5em;opacity:.5;' class='ProgressIcon'/></span><span style='margin-top:.3em;display:block;opacity:.5;'>Submitting inspection</span></div>";
        }
        else {
            // Construct full line with controls for Inspect / Repair / Wash
            if (v.isReefer == 1) {
                containerLineHTML = containerLineHTML + "<img src='images/icon-snowflake.png' width='32' style='float:left;margin:.35em -.2em 0 .2em' />";
                containerLineHTML = containerLineHTML + "<span class='containerid reefer'>" + v.id + "</span>";
            } else {
                containerLineHTML = containerLineHTML + "<img src='images/icon-dry.png' width='32' height='32' style='float:left;margin:.35em -.2em 0 .2em' />";
                containerLineHTML = containerLineHTML + "<span class='containerid dry'>" + v.id + "</span>";
            }

            // Add inspect button, linked or not depending on taskInspection status
            if (v.isReefer == 0) {
                containerLineHTML = containerLineHTML + "<span class='ButtonNA'>N/A</span>"; // Drys can't be inspected with this app
            }
            else if (v.taskInspect == "0" || v.taskInspect == "10" || v.taskInspect == "30" || v.taskInspect == "40") {
                containerLineHTML = containerLineHTML + "<span class='ButtonInspect _" + v.taskInspect + "'>Inspect</span>";
            } else {
                containerLineHTML = containerLineHTML + "<a href=\"#\"><span class='ButtonInspect _" + v.taskInspect + "'>Inspect</span></a>";
            }

            // Add repair button, linked or not depending on taskInspect status, and taskRepair status
            if (v.isReefer == 0) {
                containerLineHTML = containerLineHTML + "<span class='ButtonNA'>N/A</span>"; // Drys repairs can't be done with this app
            }
            else if (parseInt(v.taskInspect) <= 30 || v.taskRepair == "0" || v.taskRepair == "10" || v.taskRepair == "30" || v.taskRepair == "40") {
                containerLineHTML = containerLineHTML + "<span class='ButtonRepair _" + v.taskRepair + "'>Repair</span>";
            } else {
                containerLineHTML = containerLineHTML + "<a href=\"#\"><span class='ButtonRepair _" + v.taskRepair + "'>Repair</span></a>";
            }

            // Add wash button, linked or not depending on wash status (value already incorporates taskInspect status)
            if (washState == "0" || washState == "10" || washState == "30" || washState == "40") {
                containerLineHTML = containerLineHTML + "<span class='ButtonWash _" + washState + "'>Wash</span>";
            } else {
                containerLineHTML = containerLineHTML + "<a href=\"#\"><span class='ButtonWash _" + washState + "'>Wash</span></a>";
            }

        }
        containerLineHTML = containerLineHTML + "</div>";

        // render line to the relevant washpad div on page
        $('#containerlist' + v.Location).append(containerLineHTML);

        return;
    });  // end each containersCache

    // also update the activity stats
    renderActivityStatsTable();

    // finally set the colors
    setRPICcolors();

} // end function renderContainersList




// ---------------------------------------- Selection filters -------------------------------------
$("#containerstatusselection ul li").click(function () {
    console.log('filtering container list...');

    // if all
    if ($(this).attr('id') == "all") {
        $('#containerlist .container').css("display", "block");
    }

    // if toinspect  
    if ($(this).attr('id') == "toinspect") {
        $('#containerlist .container').css("display", "none");
        // Show any containers that have inspections to start or are in progress
        $('#containerlist .ButtonInspect._20').parent().parent().css("display", "block");
        $('#containerlist .ButtonInspect._23').parent().parent().css("display", "block");
        $('#containerlist .ButtonInspect._25').parent().parent().css("display", "block");
    }

    // if towash
    if ($(this).attr('id') == "towash") {
        $('#containerlist .container').css("display", "none");
        $('#containerlist .ButtonWash._20').parent().parent().css("display", "block");
        $('#containerlist .ButtonWash._23').parent().parent().css("display", "block");
        $('#containerlist .ButtonWash._25').parent().parent().css("display", "block");
    }

    // if torepair
    if ($(this).attr('id') == "torepair") {
        $('#containerlist .container').css("display", "none");
        $('#containerlist .ButtonRepair._20').parent().parent().css("display", "block");
        $('#containerlist .ButtonRepair._23').parent().parent().css("display", "block");
        $('#containerlist .ButtonRepair._25').parent().parent().css("display", "block");
    }

    // now highlight the selected button
    $('#containerstatusselection ul li').removeClass();
    $('#containerstatusselection ul li').addClass('unselectedstatus');
    $(this).removeClass();
    $(this).addClass('selectedstatus');

});




// ----------------------------------------------------------------------------------------------------
// ----------------------------------------- Initialise App -------------------------------------------
// ----------------------------------------------------------------------------------------------------
$(document).ready(function () {

    // Check for containers in WashDB on init, and then every refreshListInterval seconds
    renderContainersList();
    renderActivityStatsTable();
    setInterval(renderContainersList, refreshListInterval * 1000);


    if (getParameterByName("msgtype") == "Success") {
        StatusFlash(getParameterByName("msgtext"));
    }

    $('#main a').click(function (e) {
        e.preventDefault();
        //do other stuff when a click happens
    });


});
