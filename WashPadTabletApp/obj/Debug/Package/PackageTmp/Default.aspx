﻿<%@ Page Title="Home Page" Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WashPadTabletApp._Default" %>

<script runat="server">
  void Page_Load(object sender, EventArgs e)
    {
        Submit1.Text = "Logout " + Context.User.Identity.Name;
        // set the environment label
        EnvironmentLabel.InnerHtml = ConfigurationManager.AppSettings["EnvironmentLabel"].ToString();
    }
  void Signout_Click(object sender, EventArgs e)
  {
    FormsAuthentication.SignOut();
    Response.Redirect("Logon.aspx");
  }
</script>

<html>
<head id="Head1" runat="server">
    <title>WashPad Tablet App</title>
    <!-- fix for jQuery 2.x on IE11 -->
    <meta http-equiv="X-UA-Compatible" content="IE=10;" />
    
    <link rel="manifest" href="data/manifest.json">

    <!-- javascript libraries -->
    <script src="js/jquery.v2.1.4.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui.v1.11.4.min.js" type="text/javascript"></script>
    <script src="js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="js/underscore-min.js" type="text/javascript"></script>

    <!-- style -->
    <link rel="stylesheet" type="text/css" href="css/POL-TabletStyle.css?version=<% Response.Write(typeof(Washpad).Assembly.GetName().Version.ToString());%>" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />

    <!-- mobile icons -->
    <link rel="icon" sizes="128x128" href="images/launch-icon-128.png" />
    <link rel="apple-touch-startup-image" href="images/launch-icon-128.png" />
    <link rel="apple-touch-icon" sizes="128x128" href="images/launch-icon-128.png">

    <!-- mobile web app flags -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-title" content="Port Chalmers Washpad">
        
</head>

<body>

    <div id="main">
        <div id="breadcrumb"><span>Washpads</span></div>
        <div id="StatusContainer">
            <div id="StatusBar"></div>
        </div>

        <div id="washpads"> <!-- this div will be populated by jQuery client-side -->

        </div><!-- end #washpads -->



    </div><!-- end #main -->

    <!-- -------------------------------------- -->
    <div id="footer">
        <div id="environmentinfo">
            <div>Washpad App<br /><span id="EnvironmentLabel" runat="server"></span> <span id="AppVersionLabel">v<% Response.Write(typeof(Washpad).Assembly.GetName().Version.ToString()); %></span></div>
            <img src="images/logo.png" alt="POL Logo" />
        </div><!-- end #environmentinfo -->
        <div id="usermanagement">
            <form id="logout" runat="server">
                <asp:Button ID="Submit1" OnClick="Signout_Click" Text="Log Out" runat="server" /><p>
            </form>
        </div><!-- end #usermanagement -->
    </div><!-- end #footer -->
    <div id="StyleStub" runat="server"><% Response.Write("<style>#main{background: #" + ConfigurationManager.AppSettings["BackgroundColorHex"].ToString() + " !important;}</style>");%>
    </div>

</body>

<script src="js/POL-default.js?version=<% Response.Write(typeof(Washpad).Assembly.GetName().Version.ToString());%>" type="text/javascript"></script>

</html>