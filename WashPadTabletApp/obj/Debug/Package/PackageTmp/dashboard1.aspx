﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dashboard1.aspx.cs" Inherits="WashPadTabletApp.dashboard1" %>


<html>
<head>
    <!-- fix for jQuery 2.x on IE11 -->
    <meta http-equiv="X-UA-Compatible" content="IE=10;" />
    <title>WashPad Tablet App</title>

    <!-- javascript libraries -->
    <script src="js/jquery.v2.1.4.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui.v1.11.4.min.js" type="text/javascript"></script>
    <script src="js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="js/underscore-min.js" type="text/javascript"></script>

    <!-- style -->
    <link rel="stylesheet" type="text/css" href="css/POL-TabletStyle.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />

    <!-- mobile icons -->
    <link rel="icon" sizes="128x128" href="images/launch-icon-128.png" />
    <link rel="apple-touch-startup-image" href="images/launch-icon-128.png" />
    <link rel="apple-touch-icon" sizes="128x128" href="images/launch-icon-128.png">

    <!-- mobile web app flags -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-title" content="Port Chalmers Washpad">
        
</head>


<body>
    <div id="main">
        <div class="washcontainer">
            <div class="wphead">
                <span class="wplabel">Washpad 1</span>
                <span class="wprpic" id="RPIC-W1"></span>
            </div>
            <div id="containerlistW1" class="washpadContainerList"> <!-- this div will be populated by jQuery client-side -->
            </div><!-- end #containerlistW1 -->
        </div>
        <div class="washcontainer">
            <div class="wphead">
                <span class="wplabel">Washpad 2</span>
                <span class="wprpic" id="RPIC-W2"></span>
            </div>
            <div id="containerlistW2" class="washpadContainerList"> <!-- this div will be populated by jQuery client-side -->
            </div><!-- end #containerlistW2 -->
        </div>    

        
        <div class="washcontainer">
            <div class="wphead">
                <span class="wplabel">Washpad 3</span>
                <span class="wprpic" id="RPIC-W3"></span>
            </div>
            <div id="containerlistW3" class="washpadContainerList"> <!-- this div will be populated by jQuery client-side -->
            </div><!-- end #containerlistW3 -->
        </div>  
        <div class="washcontainerright">
            <div class="wphead">
                <span class="wplabel">Washpad 4</span>
                <span class="wprpic" id="RPIC-W4"></span>
            </div>
            <div id="containerlistW4" class="washpadContainerList"> <!-- this div will be populated by jQuery client-side -->
            </div><!-- end #containerlistW4 -->
        </div>

        <div id="ActivityStats">
             <!-- this div will be populated by jQuery client-side -->
        </div>

        <style>
            #main, body
            {
                background:#0455a3;
                }
            body
            {
                padding:1em;
                }
            .wphead
            {
                height:1.4em;
                }
            .wphead span.wplabel
            {
                background:#1465b3;
                display:inline-block;
                float:left;
                height:100%;
                width:54%;
                padding-left:4%;
                padding-right:4%;
                }
            .wphead span.wprpic
            {
                /* background set by jquery*/
                text-align:right;
                display:inline-block;
                float:right;
                height:100%;
                width:30%;
                padding-left:4%;
                padding-right:4%;
                }
            .washpadContainerList
            {
                margin-bottom:.5em;
                }
            .washcontainer
            {
                float:left;
                width:24.5%;
                margin-right:.5%;
                }
            .washcontainerright
            {
                float:left;
                width:24.5%;
                }
                
            .washpadContainerList div, .washpadContainerList span
            {
                font-size:75% !important;
            }
            .washpadContainerList img
            {
                font-size:75% !important;
                height:1em !important;
                width:1em !important;
                }
            .containerid
            {
                display:inline-block;
                }
            .container
            {
                background:#1465b3;
                margin-top:2px;
                clear:both;
                }
          /* ----------------------- Activity Stats Table ----------------------------*/
             #ActivityStatsData
             {
                 width:49.5%;
                 border-collapse:true;
                 }
             #ActivityStatsData td,#ActivityStatsData  th
             {
                 margin:1px;
                 padding:.2em;
                 text-align:center;
                 font-size:18pt !important;
                 }   
            #ActivityStatsData td
             {
                 background:#1465b3;
                 }   
            #ActivityStatsData th
             {
                 background:#2475c3;
                 }   
            #ActivityStatsData tr.separator td, #ActivityStatsData tr.separator th
             {
                 border-top:10px #0455a3 solid;
                 }   
             
            #ActivityStatsData td.blank
            {
                background: #0455a3;
                }  
        </style>

    </div><!-- end #main -->

</body>

<script src="js/POL-dashboard1.js?version=<% Response.Write(typeof(Washpad).Assembly.GetName().Version.ToString());%>" type="text/javascript"></script>

</html>