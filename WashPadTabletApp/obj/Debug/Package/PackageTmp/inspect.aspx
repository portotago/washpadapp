﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="inspect.aspx.cs" Inherits="WashPadTabletApp.inspect" %>

<script runat="server">
  void Page_Load(object sender, EventArgs e)
  {
    Submit1.Text = "Logout " + Context.User.Identity.Name;
    // set the environment label
    EnvironmentLabel.InnerHtml = ConfigurationManager.AppSettings["EnvironmentLabel"].ToString();
  }

  void Signout_Click(object sender, EventArgs e)
  {
    FormsAuthentication.SignOut();
    Response.Redirect("Logon.aspx");
  }
</script>

<html>
<head>
    <title>WashPad Tablet App</title>
    <!-- fix for jQuery 2.x on IE11 -->
    <meta http-equiv="X-UA-Compatible" content="IE=10;" />

    <!-- javascript libraries -->
    <script src="js/jquery.v2.1.4.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui.v1.11.4.min.js" type="text/javascript"></script>
    <script src="js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="js/underscore-min.js" type="text/javascript"></script>

    <!-- style -->
    <link rel="stylesheet" type="text/css" href="css/POL-TabletStyle.css?version=<% Response.Write(typeof(Washpad).Assembly.GetName().Version.ToString());%>" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />

    <!-- mobile icons -->
    <link rel="icon" sizes="128x128" href="images/launch-icon-128.png" />
    <link rel="apple-touch-startup-image" href="images/launch-icon-128.png" />
    <link rel="apple-touch-icon" sizes="128x128" href="images/launch-icon-128.png" />

    <!-- mobile web app flags -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-title" content="Port Chalmers Washpad" />

</head>

<body>
        <div id="InspectionCompletionPopup" style="display:none;">
            <div style="background:#ddd;margin:0 0 .5em 0;padding:.2em 0;">
                <p>Are you sure you want to submit this inspection for <span id="containernumber2"></span>?</p>
                <p><b>This will update DepotPRO and JMT</b></p>
            </div>
            <div id="completionbuttons"><input type="submit" value="< Cancel" id="cancel"/><input type="submit" value="Commit >" id="inspectioncomplete" /></div>
        </div>

    <div id="main">
        <div id="breadcrumb"><span id="selectedwashpad">__</span> / <span id="containernumber">__</span></div>

        <div id="StatusContainer">
            <div id="StatusBar"></div>
        </div>

        <div id="tasklist">
            <div class="repairseparator">Add tasks:</div><!-- -------------------------------------- -->
            <div class="taskline">
                <div class="taskaction"><input type="checkbox" id="taskWash"/></div>
                <div class="tasklabel"><label for="taskWash">Wash task</label></div>
            </div>
            <div class="taskline">
                <div class="taskaction"><input type="checkbox" id="taskBaseWash"/></div>
                <div class="tasklabel"><label for="taskBaseWash">Base wash task</label></div>
            </div>
            <div class="taskline">
                <div class="taskaction"><input type="checkbox" id="taskRoofWash"/></div>
                <div class="tasklabel"><label for="taskRoofWash">Roof wash task</label></div>
            </div>
            <div class="taskline">
                <div class="taskaction"><input type="checkbox" id="taskExternalWash"/></div>
                <div class="tasklabel"><label for="taskExternalWash">External wash task</label></div>
            </div>
            <div id="photoGroup">
                <span id="photoError" class="errorMessage" style="padding:0 !important;"></span>
                <div class="taskline">
                    <div class="taskaction"><input type="checkbox" id="taskPhoto"/></div>
                    <div class="tasklabel"><label for="taskPhoto">Photo task</label></div>
                </div>
                <div class="photoline" id="Photos" style="display:none;">
                    <div id="existingphotos"></div>
                    <div class="PhotoInput">
                        <input type="file" accept="image/*" capture="camera" id="Photo_1" style="display: none;" />
                        <div id="PhotoBrowse_1" class="TakePhotoButton"><img src="images/icon-camera-grey.png" width="32"/> Take a photo</div>
                    </div>
                </div>
            </div>
            <div class="taskline">
                <div class="taskaction"><input type="checkbox" id="taskRepair"/></div>
                <div class="tasklabel"><label for="taskRepair">Repair task</label></div>
            </div>
            <div class="repairsline" id="RepairsList" style="display:none;">
                <div id="existingrepairs"></div>
                <div class="RepairInput">
                    <div id="repairsbutton" class="AddRepairButton"><img src="images/icon-spanner-128.png" width="32"/> Add a repair line</div>
                </div>

                <div id="RepairWorkLocator">
                    <div class="tasklabel">Repair work site</div><!-- -------------------------------------- -->
                    <span id="RepairWorkSiteError" class="errorMessage"></span>
                    <div class="RadioOption">
                        <input type="radio" name="RepairWorkSite" id="RepairWorkSite_Washpad" value="washpad" /><label for="RepairWorkSite_Washpad"><span>Washpad</span></label>
                    </div>
                    <div class="RadioOption">
                        <input type="radio" name="RepairWorkSite" id="RepairWorkSite_Repair" value="repair" /><label for="RepairWorkSite_Repair"><span>Move to repair</span></label>
                    </div>
                </div>
            </div>

         </div>


        </div><!-- end #tasklist -->


        <div id="quality">
            <div class="repairseparator">Quality:</div><!-- -------------------------------------- -->
            <div class="taskline">
                <div class="taskaction"><input type="checkbox" id="qualityOxidised" /></div>
                <div class="tasklabel"><label for="qualityOxidised">Is oxidised</label></div>
            </div>
            <div class="taskline">
                <div class="taskaction"><input type="checkbox" id="qualityAlliance" /></div>
                <div class="tasklabel"><label for="qualityAlliance">Meets Alliance standard</label></div>
            </div>
        </div>

        
        <div class="repairseparator">Further remarks:</div><!-- -------------------------------------- -->
        <div id="inspectioncomments">
            <textarea id="remarks"></textarea>
        </div>

       <div class="repairseparator">Quote status:</div><!-- -------------------------------------- -->
   
        <div id="quotestatus">
            <span id="quotestatuserror" class="errorMessage"></span>
            <div class="RadioOption">
                <input type="radio" name="quoteStatus" id="status_complete" value="complete" /><label for="status_complete"><span><b>Complete inspection</b><br />Send estimate to customer</span></label>
            </div>
            <div class="RadioOption">
                <input type="radio" name="quoteStatus" id="status_incomplete" value="incomplete" /><label for="status_incomplete"><span><b>Incomplete inspection</b><br />Repair team to assess further</span></label>
            </div>
            <div class="clear"></div>
         </div>


        <div class="buttonarea"><!-- -------------------------------------- -->
            <div id="saveinspection"><span>< Save</span></div>
            <div id="initiatecomplete"><span>Submit to Jade and DepotPRO ></span></div>
        </div>

    </div><!-- end #main -->

    
    <!-- -------------------------------------- -->
    <div id="footer">
        <div id="environmentinfo">
            <div>Washpad App<br /><span id="EnvironmentLabel" runat="server"></span> <span id="AppVersionLabel">v<% Response.Write(typeof(Washpad).Assembly.GetName().Version.ToString()); %></span></div>
            <img src="images/logo.png" alt="POL Logo" />
        </div><!-- end #environmentinfo -->
        <div id="usermanagement">
            <form id="logout" runat="server">
                <asp:Button ID="Submit1" OnClick="Signout_Click" Text="Log Out" runat="server" /><p>
            </form>
        </div><!-- end #usermanagement -->
    </div><!-- end #footer -->
    <div id="StyleStub" runat="server"><% Response.Write("<style>#main{background: #" + ConfigurationManager.AppSettings["BackgroundColorHex"].ToString() + " !important;}</style>");%></div>

</body>

<script src="js/POL-inspect.js?version=<% Response.Write(typeof(Washpad).Assembly.GetName().Version.ToString());%>" type="text/javascript"></script>

</html>