﻿// function to retrieve querystring parameters
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

// --------------------------------------------------------------------------------------------
// --------------------------------------------- CONFIG ---------------------------------------
// --------------------------------------------------------------------------------------------
var selected_cid = getParameterByName("containerid");
var selected_oid = getParameterByName("oid");
var selected_wp = getParameterByName("washpad");

var containerDataURL = "data/getcontainer.aspx?cid=" + selected_cid + "&oid=" + selected_oid;
var redirectURL = "containerlist.aspx?washpad=" + selected_wp;  // relative to this page inspect.aspx
var submissionURL = "data/setcontainertaskcompletion.aspx?random=" + Math.random().toString(36).substring(7);
var heatbeatFileURL = "heartbeat/alive.txt";
var heartBeatFreq = 10  // seconds delay in checking connectivity
var statusFlashDurationMS = 1111;
var existingPhotosIDs = [];
var photosToDelete = [];

var selected_flagOxidised = "";
var selected_taskWash = "";
var selected_taskBaseWash = "";
var selected_taskRoofWash = "";
var selected_taskExternalWash = "";
var selected_taskSteamClean = "";
var selected_taskSteamCleanFQ = "";
var selected_taskPhoto = "";
var selected_taskRepair = "";
var selected_isReefer;
var selected_grade;


// --------------------------------------------------------------------------------------------
// ------------------------------ PRELIMINARY CACHING OF CONTAINER NOS ------------------------
// --------------------------------------------------------------------------------------------

// ------------------ cache containers list --------------------------
var ContainerCache = refreshContainerCache();


// ------------------- initiate heartbeat ----------------------------
setInterval(function () {
    $.ajax({
        url: heatbeatFileURL + '?random=' + Math.random().toString(36).substring(7),
        type: 'HEAD',
        error: function () {
            //file can't be retrieved, assume connection problem
            $.blockUI({
                message: '<span style="margin:.5em; font-weight:bold;">Error with connection to POL, check 3G connection and/or VPN.</span><p style="font-size:16pt;">Retrying in ' + heartBeatFreq + ' seconds...</p>',
                title: "In-house moves app",
                css: {
                    padding: 0,
                    margin: 0,
                    width: '70%',
                    top: '20%',
                    left: '15%',
                    textAlign: 'center',
                    color: '#f00',
                    border: '3px solid #aaa',
                    backgroundColor: '#fff',
                    cursor: 'wait'
                }
            });
        },
        success: function () {
            //file exists
            $.unblockUI();
        }
    });
}, heartBeatFreq * 1000);



// --------------------------------------------------------------------------------------------
// ---------------------------------------- FUNCTIONS -----------------------------------------
// --------------------------------------------------------------------------------------------
function resetAttributeNames(section) {
    var attrs = ['for', 'id', 'name'];
    var tags = section.find('input, label, select, div, a'), idx = section.index();
    tags.each(function () {
        var $this = $(this);
        $.each(attrs, function (i, attr) {
            var attr_val = $this.attr(attr);
            if (attr_val) {
                //$this.attr(attr, attr_val.replace(/_\d+$/, '_' + (idx + 1)))
                $this.attr(attr, attr_val.replace(/_\d+$/, '_' + (idx)))
            }
        })
    })
    // lastly change the .counter text within <span> tags
    section.find('.Counter').first().html(idx);
}

jQuery.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
    return this;
}

function StatusFlash(message) {
    $("#StatusContainer").show();
    $("#StatusBar").html(message);
    $("#StatusBar").hide().fadeIn(500).delay(statusFlashDurationMS).fadeOut(1000).hide(function () {
        $("#StatusContainer").hide();
    });
}  // end StatusFlash

function refreshContainerCache() {
    var json = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': containerDataURL + "&random=" + Math.random().toString(36).substring(7),
        'dataType': "json",
        'success': function (data) {
            console.log("Successfully refreshed containers cache");
            json = data;
        }
    });
    return json;
}



// -------------------------------------------------------------------------------------------------
function saveCompletionState(callerID) {
    //event.stopPropagation(); // Stop stuff happening
    //event.preventDefault(); // Like totally stop stuff happening

    // Retrive values from form GUI, note that:
    // - checked values are set to 30 which represents "marked as complete"
    // - unchecked values are set to 20 which represents "todo"
    // - invisible wash tasks are set to 0 "not applicable"
    // - disabled checkboxes were complete tacks (30 or 40) on pages load, set to 99 which is ignored and won't alter DB value when submitted

    function determineTaskValue(whichTask){
        if ($('#task'+whichTask+'Line').is(":visible")) {
            if ($('#task'+whichTask).is(":disabled")){
                return "99";  // this value will be ignored and will not change status in database
            } else {
                return ($("#task"+whichTask).is(":checked") ? "30" : "20");
            }
        }
        else {
            return "0";
        }
    } // end setSelectedTaskValue

    selected_taskWash = determineTaskValue("Wash");
    selected_taskBaseWash = determineTaskValue("BaseWash");
    selected_taskRoofWash = determineTaskValue("RoofWash");
    selected_taskExternalWash = determineTaskValue("ExternalWash");
    selected_taskSteamClean = determineTaskValue("SteamClean");
    selected_taskSteamCleanFQ = determineTaskValue("SteamCleanFQ");

    selected_grade = $("#containergrade").val();

    processOtherData();

    function processOtherData() {
        // Process all other data Send the data using post
        console.log('Posting data...');
        var posting = $.post(submissionURL, { cid: selected_cid, oid: selected_oid, taskWash: selected_taskWash, taskBaseWash: selected_taskBaseWash, taskRoofWash: selected_taskRoofWash, taskExternalWash: selected_taskExternalWash, taskSteamClean: selected_taskSteamClean, taskSteamCleanFQ: selected_taskSteamCleanFQ, grade: selected_grade });
        posting.done(function (data) {
            // Redirect to the relevant washpad list
            window.location = redirectURL + "&msgtype=Success&msgtext=Washes saved for " + selected_cid;
        });
        posting.fail(function () {
            //failure of posting to database
            alert("Error: Sorry I couldn't save washes to the database, please contact POL IT!");
        });
    }

} // end function saveInspectionState


// -------------------------------------------------------------------------------------------
function renderContainerState() {
    //refresh cache of container numbers in case has been udpated
    ContainerCache = refreshContainerCache();

    console.log(JSON.stringify(ContainerCache));

    $.each(ContainerCache, function (i, v) {
        // taskWash
        switch (v.taskWash) {
            case "20":
            case "21":
            case "22":
            case "23":
                $('#taskWashLine').show();
                break;
            case "30":
            case "40":
                $('#taskWashLine').show();
                $('#taskWash').prop('checked', true);
                $('#taskWash').prop('disabled', true);  // completed wash tasks will be or have been submitted to jade, so can't undo this tick
                $('#taskWashLine').addClass('added');
                break;
        }

        // taskBaseWash
        switch (v.taskBaseWash) {
            case "20":
            case "21":
            case "22":
            case "23":
                $('#taskBaseWashLine').show();
                break;
            case "30":
            case "40":
                $('#taskBaseWashLine').show();
                $('#taskBaseWash').prop('checked', true);
                $('#taskBaseWash').prop('disabled', true);  // completed wash tasks will get submitted to jade, so can't undo this tick
                $('#taskBaseWashLine').addClass('added');
                break;
        }

        // taskRoofWash
        switch (v.taskRoofWash) {
            case "20":
            case "21":
            case "22":
            case "23":
                $('#taskRoofWashLine').show();
                break;
            case "30":
            case "40":
                $('#taskRoofWashLine').show();
                $('#taskRoofWash').prop('checked', true);
                $('#taskRoofWash').prop('disabled', true);  // completed wash tasks will get submitted to jade, so can't undo this tick
                $('#taskRoofWashLine').addClass('added');
                break;
        }


        // taskExternalWash
        switch (v.taskExternalWash) {
            case "20":
            case "21":
            case "22":
            case "23":
                $('#taskExternalWashLine').show();
                break;
            case "30":
            case "40":
                $('#taskExternalWashLine').show();
                $('#taskExternalWash').prop('checked', true);
                $('#taskExternalWash').prop('disabled', true);  // completed wash tasks will get submitted to jade, so can't undo this tick
                $('#taskExternalWashLine').addClass('added');
                break;
        }


        // taskSteamClean
        switch (v.taskSteamClean) {
            case "20":
            case "21":
            case "22":
            case "23":
                $('#taskSteamCleanLine').show();
                break;
            case "30":
            case "40":
                $('#taskSteamCleanLine').show();
                $('#taskSteamClean').prop('checked', true);
                $('#taskSteamClean').prop('disabled', true);  // completed wash tasks will get submitted to jade, so can't undo this tick
                $('#taskSteamCleanLine').addClass('added');
                break;
        }


        // taskSteamCleanFQ
        switch (v.taskSteamCleanFQ) {
            case "20":
            case "21":
            case "22":
            case "23":
                $('#taskSteamCleanFQLine').show();
                break;
            case "30":
            case "40":
                $('#taskSteamCleanFQLine').show();
                $('#taskSteamCleanFQ').prop('checked', true);
                $('#taskSteamCleanFQ').prop('disabled', true);  // completed wash tasks will get submitted to jade, so can't undo this tick
                $('#taskSteamCleanFQLine').addClass('added');
                break;
        }

        // set isReefer
        selected_isReefer = v.isReefer;


        return;
    });    // end each containersCache

} // end renderContainerState

// --------------------------------------------------------------------------------------------


// --------------------------------------------------------------------------------------------
// ----------------------------------- INTERACTION HANDLERS -----------------------------------

// ---------------- GUI response when ticking/unticking boxes -----------------
$("div.taskline div input").click(function () {
    if ($(this).prop('checked')) {
        $(this).parent().parent().addClass('added');
    } else {
        $(this).parent().parent().removeClass('added');
    }
}); // end ticking/unticking handler


// -------------- Save Inspection and Complete Inspection Buttons --------------------
$("#savewashcompletion").click(function () {
    if (selected_isReefer == 0) {
        // Dry box, verify that  grade has  been selected
        if ($("#containergrade").val() === "") {
            // No grade selected, render validation prompt
            $("#gradeerror").html("You must select the grade for the container before saving:");
            $("#grade").addClass("error");
        } else {
            saveCompletionState($(this).attr('id'));
        }

    } else {
        // Reefer, just save
        saveCompletionState($(this).attr('id'));
    }

});    // end saveinspection handler


// ----------- Radio buttons ---------------
// If all VISIBLE radio buttons are ticked, submitting task completion to Jade
$('.taskline input').click(function () {

    if ($('.taskaction input:checked').size() == $('.taskaction input:visible').size()) {
        // All checkboxes are ticked

        if (selected_isReefer == 0) {
            // Box is dry
            // Show the grade selection control
            $("#grade").show();
            $("#savemessage").html("All washes for this box are finished, saving will complete the wash tasks and set the grade in Jade");
            $("#savewashcompletion span").html("Save, set grade and complete wash tasks >");
        } else {
            // Box is reefer
            $("#savemessage").html("Saving will complete the relevant wash tasks in Jade");
            $("#savewashcompletion span").html("Save and complete wash tasks >");
        }

    } else {
        // NOT ALL checkboxes ticked, so reset the button label and message
        $("#savemessage").html("Saving will complete the relevant wash tasks in Jade");
        $("#savewashcompletion span").html("Save and complete wash tasks >");
    }
});

$("#containergrade").change(function () {
    if ($("#containergrade").val() != ""){
        $("#gradeerror").html("");
        $("#grade").removeClass("error");
    }
});

// ---------------------------- Add 'clicked' class to any elements that could go slowly...
$('#savewashcompletion').click(function () {
    $(this).addClass('clicked');
});

// ----------------------------------------------------------------------------------------------------
// ----------------------------------- End INTERACTION HANDLERS ---------------------------------------



// ----------------------------------------------------------------------------------------------------
// ----------------------------------------- Initialise App -------------------------------------------
// ----------------------------------------------------------------------------------------------------
$(document).ready(function () {
    // initialise from querystring parameters
    washpadId = getParameterByName("washpad");
    if (washpadId < 1 || washpadId > 4) {
        washpadId = 1;
    }
    $('#selectedwashpad').html("<a href=\"containerlist.aspx?washpad=" + washpadId + "\">< W" + washpadId + "</a>");
    $('#containernumber').html(selected_cid);

    // tick or untick boxes based on containerstate table
    renderContainerState();

    // display state messages
    if (getParameterByName("msgtype") == "Success") {
        StatusFlash(getParameterByName("msgtext"));
    }
});
