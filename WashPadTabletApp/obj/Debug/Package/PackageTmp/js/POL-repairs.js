﻿// --------------------------------------------------------------------------------------------
// --------------------------------------------- CONFIG ---------------------------------------
// --------------------------------------------------------------------------------------------
var selected_cid = getParameterByName("containerid");
var selected_oid = getParameterByName("oid");
var selected_wp = getParameterByName("washpad");

var containersListURL = "data/getcontainers.aspx";
var containerListURL = "data/getcontainer.aspx?cid=" + selected_cid + "&oid=" + selected_oid;
var redirectURL = "inspect.aspx?washpad=" + selected_wp + "&containerid=" + selected_cid + "&oid=" + selected_oid;  // relative to this page repairs.aspx
var submissionURL = "data/setcontainerrepairs.aspx?random=" + Math.random().toString(36).substring(7);

var heatbeatFileURL = "heartbeat/alive.txt";
var csv_path = "data/washpadrepaircodes.csv?random=" + Math.random().toString(36).substring(7);
var heartBeatFreq = 10  // seconds delay in checking connectivity


// --------------------------------------------------------------------------------------------
// ------------------------------ PRELIMINARY CACHING OF CONTAINER NOS ------------------------
// --------------------------------------------------------------------------------------------

// ------------------ cache containers list --------------------------
var ContainersCache = refreshContainerCache();


// ------------------- initiate heartbeat ----------------------------
setInterval(function () {
    $.ajax({
        url: heatbeatFileURL + '?random=' + Math.random().toString(36).substring(7),
        type: 'HEAD',
        error: function () {
            //file can't be retrieved, assume connection problem
            $.blockUI({
                message: '<h1 style="margin:.5em"Network connection error. Check 3G, WIFI and/or VPN connections.</h1><p style="font-size:16pt;">Retrying in ' + heartBeatFreq + ' seconds...</p>',
                title: "In-house moves app",
                css: {
                    padding: 0,
                    margin: 0,
                    width: '70%',
                    top: '20%',
                    left: '15%',
                    textAlign: 'center',
                    color: '#f00',
                    border: 'none',
                    backgroundColor: '#fff',
                    cursor: 'wait'
                }
            });
        },
        success: function () {
            //file exists
            $.unblockUI();
        }
    });
}, heartBeatFreq * 1000);



// --------------------------------------------------------------------------------------------
// ---------------------------------------- FUNCTIONS -----------------------------------------
// --------------------------------------------------------------------------------------------
// function to retrieve querystring parameters
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function refreshContainerCache() {
    var json = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': containerListURL + "&random=" + Math.random().toString(36).substring(7),
        'dataType': "json",
        'success': function (data) {
            console.log("Successfully refreshed containers cache");
            json = data;
        }
    });
    return json;
}

// ---------------------------------
function CSVToArray(strData, strDelimiter) {
    // Check to see if the delimiter is defined. If not,
    // then default to comma.
    strDelimiter = (strDelimiter || ",");

    // Create a regular expression to parse the CSV values.
    var objPattern = new RegExp(
            (
    // Delimiters.
                "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

    // Quoted fields.
                "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

    // Standard fields.
                "([^\"\\" + strDelimiter + "\\r\\n]*))"
            ),
            "gi"
            );


    // Create an array to hold our data. Give the array
    // a default empty first row.
    var arrData = [[]];

    // Create an array to hold our individual pattern
    // matching groups.
    var arrMatches = null;


    // Keep looping over the regular expression matches
    // until we can no longer find a match.
    while (arrMatches = objPattern.exec(strData)) {

        // Get the delimiter that was found.
        var strMatchedDelimiter = arrMatches[1];

        // Check to see if the given delimiter has a length
        // (is not the start of string) and if it matches
        // field delimiter. If id does not, then we know
        // that this delimiter is a row delimiter.
        if (
                strMatchedDelimiter.length &&
                strMatchedDelimiter !== strDelimiter
                ) {

            // Since we have reached a new row of data,
            // add an empty row to our data array.
            arrData.push([]);

        }

        var strMatchedValue;

        // Now that we have our delimiter out of the way,
        // let's check to see which kind of value we
        // captured (quoted or unquoted).
        if (arrMatches[2]) {

            // We found a quoted value. When we capture
            // this value, unescape any double quotes.
            strMatchedValue = arrMatches[2].replace(
                    new RegExp("\"\"", "g"),
                    "\""
                    );
        } else {

            // We found a non-quoted value.
            strMatchedValue = arrMatches[3];
        }

        // Now that we have our value string, let's add
        // it to the data array.
        arrData[arrData.length - 1].push(strMatchedValue);
    }

    // Return the parsed data.
    return (arrData);
} // ------ end CSVToArray

// ------------------------------------END FUNCTIONS -------------------------------------------



// ----------------------------------------------------------------------------------------------
// -------------------------------------------------------------- Fault  Location Imagemaps -----
// ----------------------------------------------------------------------------------------------

// ----------------- Popup config ---------------------
$('#LocationMap').dialog({
    autoOpen: false, // Do not open on page load
    width: "100%",
    modal: true, // Freeze the background behind the overlay
    open: function () {
        // close popup if clicked outside
        jQuery('.ui-widget-overlay').bind('click', function () {
            jQuery('#LocationMap').dialog('close');
        })
    }
});

// ----------------- Panel selected - trigger Popup and set maxqty -----------------------
$(".RepairLineCategorySelector").change(function () {

    //$(".RepairLineCategorySelector option[value='null']").hide(); // remove Choose... option
    currentFaultNumber = $(this).attr('id').split('_').pop();
    selectedFaultCategory = $('#Repair_Category_' + currentFaultNumber + ' option:selected').text();
    // catch for category of sides

    // Trigger popup - swap the image and map, adjust image height
    $('#locationSelectionImage').attr("src", 'images/locations_' + selectedFaultCategory + '_' + selected_containerLength + '.jpg');
    $('#locationSelectionImage').attr("usemap", '#locations_' + selectedFaultCategory + '_' + selected_containerLength);
    if (selectedFaultCategory == "Door" || selectedFaultCategory == "FrontInterior") {
        $('#locationSelectionImage').attr("height", "650");
    } else {
        $('#locationSelectionImage').attr("height", "300");
    }

    // Now open dialog
    $('#LocationMap').dialog('open');
    console.log("currentFaultNumber: " + currentFaultNumber);
    $("#locationdefault").hide();

    // Hide/disable/deselect all repair options
    $("#Repair_Code_" + currentFaultNumber + " option").hide();
    $("#Repair_Code_" + currentFaultNumber + " option").prop('disabled', true);
    $("#Repair_Code_" + currentFaultNumber + " option").removeAttr("selected");

    // Hide quantity dropdown, show the placeholder
    $("#qtyplaceholder").show();
    $("#Repair_QTY_" + currentFaultNumber).hide();

    // Now show/enable relevant repair options
    $("#Repair_Code_" + currentFaultNumber + " option." + selectedFaultCategory).show();
    $("#Repair_Code_" + currentFaultNumber + " option." + selectedFaultCategory).prop('disabled', false);
    
    // Now select the default "choose..." option
    $("#chooser").prop('disabled', false);
    $("#chooser").prop("selected", true);
    $("#chooser").show();

    // Finally re-disable the save button - this will be changed once a repair is selected
    $("#saverepairs span").html("< Cancel");

});

// ------------------- Location selection handler -----------------
$(".LocationZone").click(function () {
    // display selected specific location in text input, and update GUI
    // Following line handles exception where location code contains a Leading _
    $('#Repair_Location_' + currentFaultNumber).val($(this).attr('id').charAt(0) === '_' ? $(this).attr('id').slice(-4) : $(this).attr('id'));
    $('#repairselectorplaceholder_' + currentFaultNumber).hide();
    $('#LocationMap').dialog('close');
    $('#Repair_Code_' + currentFaultNumber).show(500).focus();
});

// -------------------Repair selector handler -----------------
$(".repairselector").change(function () {
    // If the Choose selection is selected do nothing other than make sure the save button is still a Cancel button
    if ($(this).children(":selected").attr("id") == "chooser") {
        $("#saverepairs span").html("< Cancel");
    } else {
        $("#chooser").prop('disabled', true);
        $("#chooser").prop("selected", false);
        $("#chooser").hide();

        // Set the save button to save, assuming the user has picked an option
        $("#saverepairs span").html("< Save");

        // Set the Quantity options
        currentFaultNumber = $(this).attr('id').split('_').pop();
        maxqty = $('option:selected', this).attr('data-maxqty');
        $("#Repair_QTY_" + currentFaultNumber + " option").remove();

        // Add quantity options up to maxqty for the given repair
        for (i = 1; i <= maxqty; i++) {
            $("#Repair_QTY_" + currentFaultNumber).append($('<option></option>').val(i).html(i));
        }

        // Show the quantity dropdown, hide the placeholder
        $("#qtyplaceholder").hide();
        $("#Repair_QTY_" + currentFaultNumber).show();
    }
});


// ----------------------- Save repairs -----------------
$("#saverepairs").click(function (e) {
    // if the button says cancel, then just redirect back to container page, else save to DB
    if ($("#saverepairs span").text() == "< Cancel") {
        window.location = redirectURL + "&msgtype=Success&msgtext=Repair addition cancelled";
    }
    else {
        var howManyRepairs = $(".repairentry").length;
        var data = new FormData();
        data.append('howManyRepairs', howManyRepairs);

        // Compile data for each repair
        for (i = 1; i <= howManyRepairs; i++) {
            // Get selected values from form
            selected_containerRepairID = ($("#Repair_ID_" + i).val() > 0 ? $("#Repair_ID_" + i).val : "-1");
            selected_locationCode = ($("#Repair_Location_" + i).val());
            selected_repairCode = ($("#Repair_Code_" + i + " option:selected").attr('data-repairCode'));
            selected_repairDescription = ($("#Repair_Code_" + i + " option:selected").text());
            selected_quantity = ($("#Repair_QTY_" + i).val());

            // Create a formdata object and add the files
            data.append(('cid_' + i), selected_cid);
            data.append(('oid_' + i), selected_oid);
            data.append(('containerRepairID_' + i), selected_containerRepairID);
            data.append(('damageLocation_' + i), selected_locationCode);
            data.append(('repairCode_' + i), selected_repairCode);
            data.append(('repairDescription_' + i), selected_repairDescription);
            data.append(('quantity_' + i), selected_quantity);
        }

        $.ajax({
            url: submissionURL,
            type: 'POST',
            dataType: 'json',
            processData: false,
            contentType: false,
            data: data,
            success: function () {
                window.location = redirectURL + "&msgtype=Success&msgtext=Added " + howManyRepairs + " repairs";
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("Cound not save to the database. Please contact POL IT. errorThrown: " + errorThrown);
            }
        });
    } // end else


});// end saverepairs handler


// ---------------------------- Add 'clicked' class to any elements that could go slowly...
$('#saverepairs').click(function () {
    $(this).children().addClass('clicked');
});


// ----------------------------------------------------------------------------------------------------
// ----------------------------------------- Initialise App -------------------------------------------
// ----------------------------------------------------------------------------------------------------
$(document).ready(function () {

    // initialise from querystring parameters
    washpadId = getParameterByName("washpad");
    if (washpadId < 1 || washpadId > 4) {
        washpadId = 1;
    }
    $('#selectedwashpad').html("<a href=\"containerlist.aspx?washpad=" + washpadId + "\">W" + washpadId + "</a>");
    $('#containernumber').html(getParameterByName("containerid"));
    selected_containerLength = getParameterByName("length");

    // Populate repairselector dropdown options from CSV, and initially hide all options
    $.get(csv_path, function (data) {
        //Do stuff with the CSV data ...
        var csv = CSVToArray(data);
        renderCSVDropdown(csv);
    });

    var renderCSVDropdown = function (csv) {
        var dropdown = $('select#Repair_Code_1');
        for (var i = 0; i < csv.length; i++) {
            var record = csv[i];
            //console.log(JSON.stringify(record));
            var entry = $('<option>').attr('value', record[2]).html(record[2]).attr('class', record[1]).attr('data-maxqty', record[25]).attr('data-repairCode', record[0]).hide();
            dropdown.append(entry);
        }
    };



});
