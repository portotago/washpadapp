﻿// function to retrieve querystring parameters
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

// --------------------------------------------------------------------------------------------
// --------------------------------------------- CONFIG ---------------------------------------
// --------------------------------------------------------------------------------------------
var selected_cid = getParameterByName("containerid");
var selected_oid = getParameterByName("oid");
var selected_wp = getParameterByName("washpad");
var containerRepairsURL = "data/getcontainerrepairs.aspx?cid=" + selected_cid + "&oid=" + selected_oid;
var containerDataURL = "data/getcontainer.aspx?cid=" + selected_cid + "&oid=" + selected_oid;
var redirectURL = "containerlist.aspx?washpad=" + selected_wp;  // relative to this page inspect.aspx
var submitRepairCompletionURL = "data/setcontainerrepaircompletion.aspx?random=" + Math.random().toString(36).substring(7);

var heatbeatFileURL = "heartbeat/alive.txt";
var heartBeatFreq = 10  // seconds delay in checking connectivity
var statusFlashDurationMS = 1111;

var existingRepairIDs = [];
var repairIDsChecked = [];
var repairIDsUnchecked = [];

var selected_taskRepair = "";


// --------------------------------------------------------------------------------------------
// ------------------------------ PRELIMINARY CACHING OF CONTAINER NOS ------------------------
// --------------------------------------------------------------------------------------------


// ------------------- initiate heartbeat ----------------------------
setInterval(function () {
    $.ajax({
        url: heatbeatFileURL + '?random=' + Math.random().toString(36).substring(7),
        type: 'HEAD',
        error: function () {
            //file can't be retrieved, assume connection problem
            $.blockUI({
                message: '<span style="margin:.5em; font-weight:bold;">Error with connection to POL, check 3G connection and/or VPN.</span><p style="font-size:16pt;">Retrying in ' + heartBeatFreq + ' seconds...</p>',
                title: "In-house moves app",
                css: {
                    padding: 0,
                    margin: 0,
                    width: '70%',
                    top: '20%',
                    left: '15%',
                    textAlign: 'center',
                    color: '#f00',
                    border: '3px solid #aaa',
                    backgroundColor: '#fff',
                    cursor: 'wait'
                }
            });
        },
        success: function () {
            //file exists
            $.unblockUI();
        }
    });
}, heartBeatFreq * 1000);



// --------------------------------------------------------------------------------------------
// ---------------------------------------- FUNCTIONS -----------------------------------------
// --------------------------------------------------------------------------------------------
function resetAttributeNames(section) {
    var attrs = ['for', 'id', 'name'];
    var tags = section.find('input, label, select, div, a'), idx = section.index();
    tags.each(function () {
        var $this = $(this);
        $.each(attrs, function (i, attr) {
            var attr_val = $this.attr(attr);
            if (attr_val) {
                //$this.attr(attr, attr_val.replace(/_\d+$/, '_' + (idx + 1)))
                $this.attr(attr, attr_val.replace(/_\d+$/, '_' + (idx)))
            }
        })
    })
    // lastly change the .counter text within <span> tags
    section.find('.Counter').first().html(idx);
}

jQuery.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
    return this;
}

function StatusFlash(message) {
    $("#StatusContainer").show();
    $("#StatusBar").html(message);
    $("#StatusBar").hide().fadeIn(500).delay(statusFlashDurationMS).fadeOut(1000).hide(function () {
        $("#StatusContainer").hide();
    });
}  // end StatusFlash



// -------------------------------------------------------------------------------------------------
function saveCompletionState(callerID) {
    //event.stopPropagation(); // Stop stuff happening
    //event.preventDefault(); // Like totally stop stuff happening

    // Retrive values from form GUI, note that:
    // - checked values are set to 30 which represents "marked as complete"
    // - unchecked values are set to 20 which represents "todo"
    // - invisible wash tasks are set to 0 "not applicable"

    // Compile list of checked and uncheked repairIDs
    existingRepairIDs.forEach(function(repairId,index){
        if($("#Repair_" + repairId).is(":checked")){
            repairIDsChecked.push(repairId);
        } else {
                repairIDsUnchecked.push(repairId);
        }
        });

    // Determine status of overall repair task
    if (repairIDsChecked.length == 0) {
        // if no repairs are ticked, set repair status to 20 (task is TO START)
        selected_taskRepair = "20";
    }
    if (repairIDsChecked.length > 0 && (repairIDsChecked.length < existingRepairIDs.length)) {
        // if some but not all repairs are ticked, set repair status to 30 (task is REPAIRING)
        selected_taskRepair = "25";
    }
    if (repairIDsChecked.length == existingRepairIDs.length) {
        // if all repairs are ticked, set repair status to 30 (task is DONE BUT NOT SUBMITTED)
        selected_taskRepair = "30";
    }

    // Send the data using post
    console.log('Posting data...');
    var posting = $.post(submitRepairCompletionURL, { cid: selected_cid, oid: selected_oid, repairsDone: repairIDsChecked.join(","), repairsNotDone: repairIDsUnchecked.join(","), taskRepair: selected_taskRepair });
    posting.done(function (data) {
        // Redirect to the relevant washpad list
        window.location = redirectURL + "&msgtype=Success&msgtext=Saved " + selected_cid;
    });
    posting.fail(function () {
        //failure of posting to database
        alert("Error: Sorry I couldn't save to the database, please contact POL IT!");
    });

} // end function saveInspectionState


// -------------------------------------------------------------------------------------------
function getContainerRepairs(cid) {
    var json = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': containerRepairsURL + "&random=" + Math.random().toString(36).substring(7),
        'dataType': "json",
        'success': function (data) {
            console.log("Retrieved repair list for container.");
            json = data;
        }
    });
    return json;
}

// -------------------------------------------------------------------------------------------
function getContainerDetails() {
    var json = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': containerDataURL + "&random=" + Math.random().toString(36).substring(7),
        'dataType': "json",
        'success': function (data) {
            console.log("Successfully refreshed containers cache");
            json = data;
        }
    });
    return json;
}

// -------------------------------------------------------------------------------------------
function renderContainerState() {
    // Repairs
    theRepairs = getContainerRepairs();
    //console.log(JSON.stringify(theRepairs));
    $.each(theRepairs, function (i, repair) {
        //console.log("Repair status for " + repair.CONTAINERREPAIRID + " is: " + repair.REPAIRSTATUS);
        if (repair.REPAIRSTATUS == '30') {
            // repair has been done, display as ticked
            $('#tasklist').append("<div class='taskline added'><div class='taskaction'><input type='checkbox' id='Repair_" + repair.CONTAINERREPAIRID + "' checked='checked'/></div><div class='tasklabel'><label for='Repair_" + repair.CONTAINERREPAIRID + "'>" + repair.DAMAGELOCATION + ": " + repair.REPAIRDESCRIPTION.substring(0, 50) + " (QTY " + repair.QUANTITY + ")</label></div></div>");
        } else {
            // repair not done, display as unticked
            $('#tasklist').append("<div class='taskline'><div class='taskaction'><input type='checkbox' id='Repair_" + repair.CONTAINERREPAIRID + "'/></div><div class='tasklabel'><label for='Repair_" + repair.CONTAINERREPAIRID + "'>" + repair.DAMAGELOCATION + ": " + repair.REPAIRDESCRIPTION.substring(0, 50) + " (QTY " + repair.QUANTITY + ")</label></div></div>");
        }
        existingRepairIDs.push(repair.CONTAINERREPAIRID);
    });


    // Render remarks if present
    var thisContainer = getContainerDetails();
    // compile comments
    var compiledComments = "";
    if (thisContainer[0].qualityAlliance == "10") {
        compiledComments = "FOR ALLIANCE. ";
    } else {
        compiledComments = "NON ALLIANCE. ";
    }

    //console.log(JSON.stringify(thisContainer));
    compiledComments = compiledComments + thisContainer[0].remarks.replace(/\n/g, '<br/>');
    $('#commentsFromInspection').html(compiledComments);


    return;
} // end renderContainerState

// --------------------------------------------------------------------------------------------


// --------------------------------------------------------------------------------------------
// ----------------------------------- INTERACTION HANDLERS -----------------------------------

// ---------------- GUI response when ticking/unticking boxes -----------------
// note different selector syntax because navigation dynamically generated
$('body').on('click', '.taskline div input', function () {
    if ($(this).prop('checked')) {
        $(this).parent().parent().addClass('added');
    } else {
        $(this).parent().parent().removeClass('added');
    }
}); // end ticking/unticking handler

// ----------- Radio buttons ---------------------
// If all radio buttons are ticked, submitting tack completion to Jade
// note different selector syntax because navigation dynamically generated
$('body').on('click', '.taskline input', function () {
    if ($('.taskaction input:checked').size() == $('.taskaction input').size()) {
        // All checkboxes are ticked
        $("#savemessage").html("All repairs for this box are finished, the next save will also complete the repair task in Jade");
        $("#saverepaircompletion span").html("Save and complete Repair task >");
        $("#saverepaircompletion span").css();
    } else {
        // NOT ALL checkboxes ticked, so reset the button label and message
        $("#savemessage").html("");
        $("#saverepaircompletion span").html("< Save");
    }
});


// -------------- Save Inspection and Complete Inspection Buttons --------------------
$("#saverepaircompletion").click(function () {
    saveCompletionState($(this).attr('id'));
}); // end saveinspection handler



// ---------------------------- Add 'clicked' class to any elements that could go slowly...
$('#saverepaircompletion').click(function () {
    $(this).addClass('clicked');
});



// ----------------------------------------------------------------------------------------------------
// ----------------------------------- End INTERACTION HANDLERS ---------------------------------------






// ----------------------------------------------------------------------------------------------------
// ----------------------------------------- Initialise App -------------------------------------------
// ----------------------------------------------------------------------------------------------------
$(document).ready(function () {
    // initialise from querystring parameters
    washpadId = getParameterByName("washpad");
    if (washpadId < 1 || washpadId > 4) {
        washpadId = 1;
    }
    $('#selectedwashpad').html("<a href=\"containerlist.aspx?washpad=" + washpadId + "\">< W" + washpadId + "</a>");
    $('#containernumber').html(selected_cid);

    // tick or untick boxes based on containerstate table
    renderContainerState();

    // display state messages
    if (getParameterByName("msgtype") == "Success") {
        StatusFlash(getParameterByName("msgtext"));
    }
});
