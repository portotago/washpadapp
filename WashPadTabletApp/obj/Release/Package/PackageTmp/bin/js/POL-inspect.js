﻿// function to retrieve querystring parameters
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

// --------------------------------------------------------------------------------------------
// --------------------------------------------- CONFIG ---------------------------------------
// --------------------------------------------------------------------------------------------
var selected_cid = getParameterByName("containerid");
var selected_oid = getParameterByName("oid");
var selected_wp = getParameterByName("washpad");

var uploadPhotosURL = "data/uploadphotos.aspx";
var deletePhotosURL = "data/deletephotos.aspx";
var deleteRepairLinesURL = "data/deleterepairlines.aspx";
var containerDataURL = "data/getcontainer.aspx?cid=" + selected_cid + "&oid=" + selected_oid;
var containerPhotosURL = "data/getcontainerphotos.aspx?cid=" + selected_cid + "&oid=" + selected_oid;
var containerRepairsURL = "data/getcontainerrepairs.aspx?cid=" + selected_cid + "&oid=" + selected_oid;
var redirectURL = "containerlist.aspx?washpad=" + selected_wp;  // relative to this page inspect.aspx
var submissionURL = "data/setcontainerstate.aspx?random=" + Math.random().toString(36).substring(7);
var heatbeatFileURL = "heartbeat/alive.txt";

var heartBeatFreq = 10  // seconds delay in checking connectivity
var statusFlashDurationMS = 1111;

var existingPhotosIDs = [];
var photosToDelete = [];
var existingRepairIDs = [];
var repairLinesToDelete = [];

var selected_taskWash = "";
var selected_taskBaseWash = "";
var selected_taskRoofWash = "";
var selected_taskExternalWash = "";
var selected_taskPhoto = "";
var selected_taskRepair = "";
var selected_containerLength = "";
var selected_qualityOxidised = "";
var selected_qualityAlliance = "";


// --------------------------------------------------------------------------------------------
// ------------------------------ PRELIMINARY CACHING OF CONTAINER NOS ------------------------
// --------------------------------------------------------------------------------------------

// ------------------ cache containers list --------------------------
//var ContainerCache = refreshContainerCache();
var ContainerCache = "";


// ------------------- initiate heartbeat ----------------------------
setInterval(function () {
    $.ajax({
        url: heatbeatFileURL + '?random=' + Math.random().toString(36).substring(7),
        type: 'HEAD',
        error: function () {
            //file can't be retrieved, assume connection problem
            $.blockUI({
                message: '<span style="margin:.5em; font-weight:bold;">Error with connection to POL, check 3G connection and/or VPN.</span><p style="font-size:16pt;">Retrying in ' + heartBeatFreq + ' seconds...</p>',
                title: "In-house moves app",
                css: {
                    padding: 0,
                    margin: 0,
                    width: '70%',
                    top: '20%',
                    left: '15%',
                    textAlign: 'center',
                    color: '#f00',
                    border: '3px solid #aaa',
                    backgroundColor: '#fff',
                    cursor: 'wait'
                }
            });
        },
        success: function () {
            //file exists
            $.unblockUI();
        }
    });
}, heartBeatFreq * 1000);



// --------------------------------------------------------------------------------------------
// ---------------------------------------- FUNCTIONS -----------------------------------------
// --------------------------------------------------------------------------------------------
function resetAttributeNames(section) {
    var attrs = ['for', 'id', 'name'];
    var tags = section.find('input, label, select, div, a'), idx = section.index();
    tags.each(function () {
        var $this = $(this);
        $.each(attrs, function (i, attr) {
            var attr_val = $this.attr(attr);
            if (attr_val) {
                //$this.attr(attr, attr_val.replace(/_\d+$/, '_' + (idx + 1)))
                $this.attr(attr, attr_val.replace(/_\d+$/, '_' + (idx)))
            }
        })
    })
    // lastly change the .counter text within <span> tags
    section.find('.Counter').first().html(idx);
}

jQuery.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
    return this;
}

function StatusFlash(message) {
    $("#StatusContainer").show();
    $("#StatusBar").html(message);
    $("#StatusBar").hide().fadeIn(500).delay(statusFlashDurationMS).fadeOut(1000).hide(function () {
        $("#StatusContainer").hide();
    });
}  // end StatusFlash

function refreshContainerCache() {
    var json = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': containerDataURL + "&random=" + Math.random().toString(36).substring(7),
        'dataType': "json",
        'success': function (data) {
            console.log("Successfully refreshed containers cache");
            json = data;
        }
    });
    return json;
}

function getContainerPhotos(cid) {
    var json = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': containerPhotosURL + "&random=" + Math.random().toString(36).substring(7),
        'dataType': "json",
        'success': function (data) {
            console.log("Successfully got photos for container.");
            json = data;
        }
    });
    return json;
}

function getContainerRepairs(cid) {
    var json = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': containerRepairsURL + "&random=" + Math.random().toString(36).substring(7),
        'dataType': "json",
        'success': function (data) {
            console.log("Successfully got repairs for container.");
            json = data;
        }
    });
    return json;
}

// ------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
function saveInspectionState(callerID) {
    //event.stopPropagation(); // Stop stuff happening
    //event.preventDefault(); // Like totally stop stuff happening

    // Determine task status values from GUI
        // Each checked and not disabled tickbox setting the DB values to 10 (which represents the task has been "Identified")
        // Note that tasks that are disabled and ticked are passed in again with status 23 (open task received from Jade)
    selected_taskWash = $("#taskWash:not(:disabled)").is(":checked") ? "10" : $("#taskWash:disabled").is(":checked") ? "23" : "0";
    selected_taskBaseWash = ($("#taskBaseWash:not(:disabled)").is(":checked") ? "10" : $("#taskBaseWash:disabled").is(":checked") ? "23" : "0");
    selected_taskRoofWash = ($("#taskRoofWash:not(:disabled)").is(":checked") ? "10" : $("#taskRoofWash:disabled").is(":checked") ? "23" : "0");
    selected_taskExternalWash = ($("#taskExternalWash:not(:disabled)").is(":checked") ? "10" : $("#taskExternalWash:disabled").is(":checked") ? "23" : "0");
    selected_taskPhoto = ($("#taskPhoto:not(:disabled)").is(":checked") ? "10" : $("#taskPhoto:disabled").is(":checked") ? "23" : "0");
    selected_taskRepair = ($("#taskRepair:not(:disabled)").is(":checked") ? "10" : $("#taskRepair:disabled").is(":checked") ? "23" : "0");
    selected_qualityOxidised = ($("#qualityOxidised").is(":checked") ? "10" : "0"); // never sourced from Jade
    selected_qualityAlliance = ($("#qualityAlliance").is(":checked") ? "10" : "0"); // never sourced from Jade
    selected_remarks = ($("#remarks").val());
    selected_quoteStatus = ($("input[name=quoteStatus]:checked").val());
    if($("input[name=RepairWorkSite]:checked").val() == "repair"){
        selected_taskMoveToRepair = "10";  // never sourced from Jade
    } else if ($("input[name=RepairWorkSite]:checked").val() == "washpad") {
        selected_taskMoveToRepair = "5";  // hack state to indicate repair at washpad
    } else {
        selected_taskMoveToRepair = "0";  // no selection made
    }

    if (callerID == "inspectioncomplete") {
        // Final submission of inspection
        selected_taskInspect = 30; // Marked as complete, submitted to source system
    } else {
        // Interim save of inspection
        selected_taskInspect = 25;
    }

    // Actual processing is a daisy chain of functions, could be done in parallel, but have chained them in serial
    // Kicks of with the first function that calls the next when complete and so on...
    processRepairLineDeletion();

    //  ------------------ Process repair line deletion
    function processRepairLineDeletion() {
        console.log("Processing repair line deletion...");
        $.ajax({
            url: deleteRepairLinesURL + "?random=" + Math.random().toString(36).substring(7),
            type: 'POST',
            data: { repairLinesToDelete: repairLinesToDelete.join(",") },
            success: function (data, textStatus, jqXHR) {
                if (typeof data.error === 'undefined') {
                    // Success
                    console.log('Repair lines deleted without error.');
                }
                else {
                    // Handle errors here
                    console.log('Repair lines deleted with errors: ' + data.error);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Handle errors here
                console.log('Repair line deletion errors: ' + textStatus);
                alert("Error: Sorry I couldn't delete repair lines, please contact POL IT!");
            },
            complete: function () {
                processPhotoDeletion();
            }
        });
    } //  ------------------ END function processRepairLineDeletion


    //  ------------------ Process photo deletion
    function processPhotoDeletion() { 
        console.log("Processing photo deletion...");
        $.ajax({
            url: deletePhotosURL + "?random=" + Math.random().toString(36).substring(7),
            type: 'POST',
            data: { photosToDelete: photosToDelete.join(",") },
            success: function (data, textStatus, jqXHR) {
                if (typeof data.error === 'undefined') {
                    // Success
                    console.log('PHOTOS DELETED WITHOUT ERROR.');
                }
                else {
                    // Handle errors here
                    console.log('PHOTOS DELETED WITH ERRORS: ' + data.error);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Handle errors here
                console.log('PHOTO DELETE FAIL, ERRORS: ' + textStatus);
                alert("Error: Sorry I couldn't delete photos, please contact POL IT!");
            },
            complete: function () {
                processPhotoUploads();
            }
        });
    } //  ------------------ END function processPhotoDeletion

    //  ------------------ Process photo uploads
    function processPhotoUploads() {
        console.log("Processing photo uploads...");
        // Create a formdata object and add the files
        var data = new FormData();
        $.each(files, function (key, value) {
            // note that each entry in files is an array, but with only one file, hence [0]
            console.log('Uploading file #' + key);
            console.log('Data: ' + value[0]);
            data.append(('photo' + key), value[0]);
        });
        data.append('containerNumber', selected_cid);
        data.append('oid', selected_oid);
        $.ajax({
            url: uploadPhotosURL,
            type: 'POST',
            data: data,
            cache: false,
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data, textStatus, jqXHR) {
                if (typeof data.error === 'undefined') {
                    // Success
                    console.log('Posted photos without error.');
                }
                else {
                    // Handle errors here
                    console.log('Posted photos with error: ' + data.error);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // Handle errors here
                console.log('Post photo fail, error: ' + textStatus);
                alert("Error: Sorry I couldn't save photos, please contact POL IT!");
            },
            complete: function () {
                //alert("great");
                processOtherData();
            }
        });
    } //  ------------------ END function processPhotoUploads

    function processOtherData() {
        // Process all other data Send the data
        console.log('Posting non-photo data...');
        var posting = $.post(submissionURL, { cid: selected_cid, oid: selected_oid, taskInspect: selected_taskInspect, taskWash: selected_taskWash, taskBaseWash: selected_taskBaseWash, taskRoofWash: selected_taskRoofWash, taskExternalWash: selected_taskExternalWash, taskPhoto: selected_taskPhoto, taskRepair: selected_taskRepair, qualityOxidised: selected_qualityOxidised, qualityAlliance: selected_qualityAlliance, remarks: selected_remarks, quoteStatus: selected_quoteStatus, taskMoveToRepair: selected_taskMoveToRepair });
        posting.done(function (data) {
            // Redirect to the relevant washpad list
            if (selected_taskInspect == "30") {
                window.location = redirectURL + "&msgtype=Success&msgtext=Submitted inspection for " + selected_cid;
            } else {
                window.location = redirectURL + "&msgtype=Success&msgtext=Saved inspection for " + selected_cid;
            }
        });
        posting.fail(function () {
            //failure of posting to database
            alert("Error: Sorry I couldn't save to the database, please contact POL IT!");
        });
    }

} // end function saveInspectionState


// -------------------------------------------------------------------------------------------
function renderContainerState() {
    //refresh cache of container numbers in case has been udpated
    ContainerCache = refreshContainerCache();

    console.log(JSON.stringify(ContainerCache));

    $.each(ContainerCache, function (i, v) {

        // Display attributes of container
        var containerLineHTML = "";
        switch (v.isReefer) {
            case "1":
                containerLineHTML = containerLineHTML + "<img src='images/icon-snowflake.png' width='32' style='margin:.35em -.4em 0 .1em' />";
                containerLineHTML = containerLineHTML + "<span class='containerid reefer'>" + v.operatorCode + " " + (v.is40ftLong == "True" ? "40" : "20") + " <b>" + v.id + "</b></span>";
                break;
            default:
                containerLineHTML = containerLineHTML + "<img src='images/icon-dry.png' width='32' height='32' style='margin:.35em -.4em 0 .1em' />";
                containerLineHTML = containerLineHTML + "<span class='containerid dry'>" + v.operatorCode + " " + (v.is40ftLong == "True" ? "40" : "20") + " <b>" + v.id + "</b></span>";
        }
        selected_containerLength = (v.is40ftLong == "True" ? "40" : "20");
        $("#containernumber").html(containerLineHTML);

        // taskWash
        switch (v.taskWash) {
            case "10":
                $('#taskWash').prop('checked', true);
                $('#taskWash').parent().parent().addClass('added');
                break;
            case "23":
                $('#taskWash').prop('checked', true);
                $('#taskWash').parent().parent().addClass('added');
                $('#taskWash').prop('disabled', true);  // open task from Jade, so can't untick
                break;
            default:
                $('#taskWash').prop('checked', false);
                $('#taskWash').parent().parent().removeClass('added');
        }

        // taskBaseWash
        switch (v.taskBaseWash) {
            case "10":
                $('#taskBaseWash').prop('checked', true);
                $('#taskBaseWash').parent().parent().addClass('added');
                break;
            case "23":
                $('#taskBaseWash').prop('checked', true);
                $('#taskBaseWash').parent().parent().addClass('added');
                $('#taskBaseWash').prop('disabled', true);  // open task from Jade, so can't untick
                break;
            default:
                $('#taskBaseWash').prop('checked', false);
                $('#taskBaseWash').parent().parent().removeClass('added');
        }

        // taskRoofWash
        switch (v.taskRoofWash) {
            case "10":
                $('#taskRoofWash').prop('checked', true);
                $('#taskRoofWash').parent().parent().addClass('added');
                break;
            case "23":
                $('#taskRoofWash').prop('checked', true);
                $('#taskRoofWash').parent().parent().addClass('added');
                $('#taskRoofWash').prop('disabled', true);  // open task from Jade, so can't untick
                break;
            default:
                $('#taskRoofWash').prop('checked', false);
                $('#taskRoofWash').parent().parent().removeClass('added');
        }

        // taskExternalWash
        switch (v.taskExternalWash) {
            case "10":
                $('#taskExternalWash').prop('checked', true);
                $('#taskExternalWash').parent().parent().addClass('added');
                break;
            case "23":
                $('#taskExternalWash').prop('checked', true);
                $('#taskExternalWash').parent().parent().addClass('added');
                $('#taskExternalWash').prop('disabled', true);  // open task from Jade, so can't untick
                break;
            default:
                $('#taskExternalWash').prop('checked', false);
                $('#taskExternalWash').parent().parent().removeClass('added');
        }

        // qualityAlliance
        switch (v.qualityAlliance) {
            // This field is not sourced from any other system, not concerned with status other than 10   
            case "10":
                $('#qualityAlliance').prop('checked', true);
                $('#qualityAlliance').parent().parent().addClass('added');
                break;
            default:
                $('#qualityAlliance').prop('checked', false);
                $('#qualityAlliance').parent().parent().removeClass('added');
        }

        // qualityOxidised  (sequence AFTER qualityAlliance)
        switch (v.qualityOxidised) {
            // This field is not sourced from any other system, not concerned with status other than 10   
            case "10":
                $('#qualityOxidised').prop('checked', true);
                $('#qualityOxidised').parent().parent().addClass('added');
                // Additionally untick and grey out the Alliance Standard button
                $('#qualityAlliance').prop('checked', false);
                $('#qualityAlliance').prop('disabled', true);
                $('#qualityAlliance').parent().parent().children().css('opacity', '.5');
                $('#qualityAlliance').parent().parent().removeClass('added');
                break;
            default:
                $('#qualityOxidised').prop('checked', false);
                $('#qualityOxidised').parent().parent().removeClass('added');
        }

        // taskPhoto
        switch (v.taskPhoto) {
            case "10":
                $('#taskPhoto').prop('checked', true);
                $('#taskPhoto').parent().parent().addClass('added');
                $('#Photos').show();
                break;
            case "23":
                $('#taskPhoto').prop('checked', true);
                $('#taskPhoto').parent().parent().addClass('added');
                $('#taskPhoto').prop('disabled', true);  // open task from Jade, so can't untick
                $('#Photos').show();
                break;
            default:
                $('#taskPhoto').prop('checked', false);
                $('#taskPhoto').parent().parent().removeClass('added');
                $('#Photos').hide();
        }

        // Photos
        thePhotos = getContainerPhotos();
        $.each(thePhotos, function (i, photo) {
            $('#existingphotos').append("<div class='PhotoExisting' id='ExistingPhoto_" + (i + 1) + "' data-photoID='" + photo.PHOTOID + "'><img src='data:image/png;base64," + photo.PHOTO + "'><span class='RemoveExistingPhoto'>X</span></div>");
            existingPhotosIDs.push(photo.PHOTOID);
            return;
        }); // end Photos

        // taskRepair
        switch (v.taskRepair) {
            case "10":
                $('#taskRepair').prop('checked', true);
                $('#taskRepair').parent().parent().addClass('added');
                $('#RepairsList').show();
                break;
            case "23":
                $('#taskRepair').prop('checked', true);
                $('#taskRepair').parent().parent().addClass('added');
                $('#taskRepair').prop('disabled', true);  // open task from Jade, so can't untick
                $('#RepairsList').show();
                break;
            default:
                $('#taskRepair').prop('checked', false);
                $('#taskRepair').parent().parent().removeClass('added');
                $('#RepairsList').hide();
        }

        // Repairs
        theRepairs = getContainerRepairs();
        console.log(JSON.stringify(theRepairs));
        $.each(theRepairs, function (i, repair) {
            $('#existingrepairs').append("<div class='RepairExisting' id='ExistingRepair_" + (i + 1) + "' data-repairID='" + repair.CONTAINERREPAIRID + "'><div class='existingrepairlabel'><b>" + repair.DAMAGELOCATION + "</b>: " + repair.REPAIRDESCRIPTION + " (QTY " + repair.QUANTITY + ")</div><span class='RemoveExistingRepair'>X</span></div>");
            existingRepairIDs.push(repair.CONTAINERREPAIRID);
            return;
        }); // end repairs

        // remarks
        $('#remarks').html(v.remarks);

        // quoteStatus
        var $radios = $('input:radio[name=quoteStatus]');
        switch (v.quoteStatus) {
            case "complete":
                $radios.filter('[value=complete]').prop('checked', true);
                break;
            case "incomplete":
                $radios.filter('[value=incomplete]').prop('checked', true);
                break;
            default:
                // no default behaviour
        }

        // Repair Work Site
        var $radios = $('input:radio[name=RepairWorkSite]');
        switch (v.taskMoveToRepair) {
            case "10":
            case "23":
                $radios.filter('[value=repair]').prop('checked', true);
                break;
            case "5":
                $radios.filter('[value=washpad]').prop('checked', true);
            default: 
                break;
                // no default behaviour
        }

        return;
    });      // end each containersCache

} // end renderContainerState

// --------------------------------------------------------------------------------------------

// ----------------- Popup config ---------------------
$('#InspectionCompletionPopup').dialog({
    autoOpen: false, // Do not open on page load
    width: "100%",
    modal: true, // Freeze the background behind the overlay
    open: function () {
        // ATTEMPTING TO POSITION THE POPUP IN CENTER OF VIEWPORT...
        //$(this).position: { my: "center", at: "center", of: window };
        //$(this).parent().css('position', 'fixed');
        //$('#InspectionCompletionPopup').dialog('option', 'position', 'right top');
        // $('#InspectionCompletionPopup').dialog('widget').position({
        //     my: "center",
        //      at: "center",
        //      of: window
        //  });
        //$('#InspectionCompletionPopup').center();
   
        // close popup if clicked outside
        jQuery('.ui-widget-overlay').bind('click', function () {
            jQuery('#InspectionCompletionPopup').dialog('close');
        })
    }
});


// --------------------------------------------------------------------------------------------
// ----------------------------------- INTERACTION HANDLERS -----------------------------------
// --------------------------------------------------------------------------------------------

$("#repairsbutton").click(function () {
    redirectURL = "repairs.aspx?containerid=" + selected_cid + "&oid=" + selected_oid + "&length=" + selected_containerLength + "&washpad=" + selected_wp;
    saveInspectionState($(this).attr('id'));
});

$('#taskRepair').click(function () {
    if ($(this).prop('checked')) {
        $('#RepairsList').show();
    } else {
        $('#RepairsList').hide();
    }
});

$('#taskPhoto').click(function () {
    if ($(this).prop('checked')) {
        $('#Photos').show();
    } else {
        $('#Photos').hide();
    }
    $("#photoGroup").removeClass('erroring');
    $("#photoError").html('');
});

$('.TakePhotoButton').click(function () {
    $("#photoGroup").removeClass('erroring');
    $("#photoError").html('');
});



// ---------- take photo button ---------------
// Variable to store your files
var files = [];
// Add events
$('input[type=file]').on('change', prepareUpload);
// Grab the files and set them to our variable
function prepareUpload(event) {
    files.push(event.target.files);
    console.log('File added to upload queue');
}

// ---------------- GUI response when ticking/unticking boxes -----------------
$("div.taskline div input").click(function () {
    if ($(this).prop('checked')) {
        $(this).parent().parent().addClass('added');
    } else {
        $(this).parent().parent().removeClass('added');
    }
}); // end ticking/unticking handler


$('#qualityOxidised').click(function () {
    if (this.checked) {
        // oxidised is ticked, so untick and grey out the Alliance Standard button
        $('#qualityAlliance').prop('checked', false);
        $('#qualityAlliance').prop('disabled', true);
        $('#qualityAlliance').parent().parent().children().css('opacity', '.5');
        $('#qualityAlliance').parent().parent().removeClass('added');
    }
    else {
        // enable the Alliance checkbox
        $('#qualityAlliance').prop('disabled', false);
        $('#qualityAlliance').parent().parent().children().css('opacity', '1');
    }

});

// ---------------- Start the completion popup -----------------
$('#initiatecomplete').click(function () {
    var validSubmission = true; // innocent until proven guilty

    // Validate that if a photo task added on app, that a photo has been taken
    //  alert('disabled - ' + $('#taskPhoto').prop('disabled'));
    //  alert('checked - ' + $('#taskPhoto').prop('checked'));
    //  alert('num PhotoExisting - ' + $('.PhotoExisting').length);
    //  alert('num PhotoTaken - ' + $('.PhotoTaken').length);

    if ($('#taskPhoto:checked').prop('checked') == true && $('#taskPhoto:checked').prop('disabled') == false && ($('.PhotoExisting').length + $('.PhotoTaken').length) < 1) {
        // photo task is ticked on app (and not pulled through from Jade) but no photos added
        $("#photoGroup").addClass('erroring');
        $("#photoError").html("You've added a photo task but no photos.  Add photos or untick the task:");
        validSubmission = false;
    }

    // if there's a repair task, validate that a repair work site has been selected
   if($('#taskRepair:checked').prop('checked') == true){
       if ($('input:radio[name=RepairWorkSite]:checked').length > 0) {
        // at least one of the radio buttons was checked
        // do nothing - i.e. if don't falisify sumbmission status
        }
        else {
            // no radio button was checked
            // Highlight quote status and add error message
            $("#RepairWorkSiteError").html("You must select where the repair work will be done:");
            $("#RepairWorkLocator").addClass('erroring');
            validSubmission = false;
        }
   }

    // validate that a quote status has been selected
    if ($('input:radio[name=quoteStatus]:checked').length > 0) {
        // at least one of the radio buttons was checked
        // do nothing - i.e. if don't falisify sumbmission status
    }
    else {
        // no radio button was checked
        // Highlight quote status and add error message
        $("#quotestatuserror").html("You must select whether the quote is complete or incomplete:");
        $("#quotestatus").addClass('erroring');
        validSubmission = false;
    }

    // if all valid open popup, else highlight errors
    if (validSubmission) {
        $('#initiatecomplete span').addClass('clicked');
        $('#InspectionCompletionPopup').dialog('open');
    } else {
        // scroll to first error on screen
        $('#initiatecomplete span').removeClass('clicked');
        $('html, body').animate({
            scrollTop: ($('.erroring').first().offset().top)
        }, 500);
    }
});

$('#cancel').click(function () {
    $('#InspectionCompletionPopup').dialog('close');
    $('#initiatecomplete span').removeClass('clicked');
});

// ----------- Radio buttons - when clicked clear error status and message ------------
$('#quotestatus .RadioOption input').click(function () {
    $("#quotestatus").removeClass('erroring');
    $("#quotestatuserror").html("");
});
$('#RepairWorkLocator .RadioOption input').click(function () {
    $("#RepairWorkLocator").removeClass('erroring');
    $("#RepairWorkSiteError").html("");
});

// -------------- Save Inspection and Complete Inspection Buttons --------------------
$("#saveinspection,#inspectioncomplete").click(function () {
    saveInspectionState($(this).attr('id'));
}); // end saveinspection handler

// ------------------------- Photo Input Repeating Controls handling ----------------------
$('.TakePhotoButton').click(function () {
    var photoRowNum = $(this).attr('id').split('_').pop();
    $('#Photo_' + photoRowNum).click();
    return false;
});

// create repeated field when photo is added
$('.PhotoInput input').change(function () {
    // duplicate Take Photo button
    var lastRepeatingGroup = $('.PhotoInput').last();
    var cloned = lastRepeatingGroup.clone(true)
    cloned.insertAfter(lastRepeatingGroup).hide().fadeIn();
    cloned.find("input").val("");
    //cloned.find("input");
    resetAttributeNames(cloned)

    // update GUI with filename
    var photoRowNum = $(this).attr('id').split('_').pop();
    $('#PhotoBrowse_' + photoRowNum).before("<div class='PhotoTaken'><span class='PhotoDesc'>" + $(this).val().split('\\').pop() + "</span><span class='RemovePhoto'>X</span></div>");
    $('#PhotoBrowse_' + photoRowNum).remove();
});

// Delete a new photo
// note different selector syntax because photo lines dynamically generated
$('body').on('click', '.RemovePhoto', function () {
    var current_photo = $(this).parent('div').parent('div');
    var other_photos = current_photo.siblings('.PhotoInput');
    current_photo.fadeOut('', function () {
        current_photo.remove();
        // reset photo indexes
        other_photos.each(function () {
            resetAttributeNames($(this));
        })
        // reset the indexed on the Take a photo button
    })
});

// Delete an existing photo
// note different selector syntax because photo lines dynamically generated
$('body').on('click', '.RemoveExistingPhoto', function () {
    var current_photo = $(this).parent('div');
    photosToDelete.push($(this).parent('div').attr('data-photoid'));
    var other_photos = current_photo.siblings('.PhotoExisting');
    current_photo.fadeOut('', function () {
        current_photo.remove();
        // Reset photo indexes in GUI
        other_photos.each(function () {
            resetAttributeNames($(this));
        })
    })
});

// Delete a repair (note this handler only adds deletions to a deletion queue - the queue is actioned when saving the inspection)
// note different selector syntax because repair lines dynamically generated
$('body').on('click', '.RemoveExistingRepair', function () {
    var current_repair = $(this).parent('div');
    repairLinesToDelete.push($(this).parent('div').attr('data-repairid'));
    var other_repairs = current_repair.siblings('.RepairExisting');
    current_repair.fadeOut('', function () {
        current_repair.remove();
        // Reset photo indexes in GUI
        other_repairs.each(function () {
            resetAttributeNames($(this));
        })
    })
});

// ---------------------------- Add 'clicked' class to any elements that could go slowly...
// note different selector syntax because navigation dynamically generated
$('#inspectioncomplete, .AddRepairButton').click(function () {
    $(this).addClass('clicked');
});
$('.PhotoInput, #saveinspection').click(function () {
    $(this).children().addClass('clicked');
});

// ----------------------------------------------------------------------------------------------------
// ----------------------------------- End INTERACTION HANDLERS ---------------------------------------


// ----------------------------------------------------------------------------------------------------
// ----------------------------------------- Initialise App -------------------------------------------
// ----------------------------------------------------------------------------------------------------
$(document).ready(function () {
    // initialise from querystring parameters
    washpadId = getParameterByName("washpad");
    if (washpadId < 1 || washpadId > 4) {
        washpadId = 1;
    }
    $('#selectedwashpad').html("<a href=\"containerlist.aspx?washpad=" + washpadId + "\">< W" + washpadId + "</a>");
    $('#containernumber2').html(selected_cid);

    // tick or untick boxes based on containerstate table
    renderContainerState();

    // display state messages
    if (getParameterByName("msgtype") == "Success") {
        StatusFlash(getParameterByName("msgtext"));
    }
});
