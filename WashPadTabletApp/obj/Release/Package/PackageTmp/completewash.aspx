﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="completewash.aspx.cs" Inherits="WashPadTabletApp.completewash" %>

<script runat="server">
  void Page_Load(object sender, EventArgs e)
  {
    Submit1.Text = "Logout " + Context.User.Identity.Name;
    // set the environment label
    EnvironmentLabel.InnerHtml = ConfigurationManager.AppSettings["EnvironmentLabel"].ToString();
  }

  void Signout_Click(object sender, EventArgs e)
  {
    FormsAuthentication.SignOut();
    Response.Redirect("Logon.aspx");
  }
</script>

<html>
<head>
    <title>WashPad Tablet App</title> 
    <!-- fix for jQuery 2.x on IE11 -->
    <meta http-equiv="X-UA-Compatible" content="IE=10;" />

    <!-- javascript libraries -->
    <script src="js/jquery.v2.1.4.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui.v1.11.4.min.js" type="text/javascript"></script>
    <script src="js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="js/underscore-min.js" type="text/javascript"></script>

    <!-- style -->
    <link rel="stylesheet" type="text/css" href="css/POL-TabletStyle.css?version=<% Response.Write(typeof(Washpad).Assembly.GetName().Version.ToString());%>" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />

    <!-- mobile icons -->
    <link rel="icon" sizes="128x128" href="images/launch-icon-128.png" />
    <link rel="apple-touch-startup-image" href="images/launch-icon-128.png" />
    <link rel="apple-touch-icon" sizes="128x128" href="images/launch-icon-128.png">

    <!-- mobile web app flags -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-title" content="Port Chalmers Washpad">

</head>

<body>
    <div id="main">
        <div id="breadcrumb"><span id="selectedwashpad">__</span> / <span id="containernumber">__</span></div>
        <div id="StatusContainer">
            <div id="StatusBar"></div>
        </div>

        <!-- -------------------------------------- -->
        <div id="tasklist">
            <div class="repairseparator">Tick completed wash tasks:</div>
            <div class="taskline" id="taskWashLine" style="display:none;">
                <div class="taskaction"><input type="checkbox" id="taskWash"/></div>
                <div class="tasklabel"><label for="taskWash">Wash task</label></div>
            </div>
            <div class="taskline" id="taskBaseWashLine" style="display:none;">
                <div class="taskaction"><input type="checkbox" id="taskBaseWash"/></div>
                <div class="tasklabel"><label for="taskBaseWash">Base wash task</label></div>
            </div>
            <div class="taskline" id="taskRoofWashLine" style="display:none;">
                <div class="taskaction"><input type="checkbox" id="taskRoofWash"/></div>
                <div class="tasklabel"><label for="taskRoofWash">Roof wash task</label></div>
            </div>
            <div class="taskline" id="taskExternalWashLine" style="display:none;">
                <div class="taskaction"><input type="checkbox" id="taskExternalWash"/></div>
                <div class="tasklabel"><label for="taskExternalWash">External wash task</label></div>
            </div>
            <div class="taskline" id="taskSteamCleanLine" style="display:none;">
                <div class="taskaction"><input type="checkbox" id="taskSteamClean"/></div>
                <div class="tasklabel"><label for="taskSteamClean">Steam clean task</label></div>
            </div>
            <div class="taskline" id="taskSteamCleanFQLine" style="display:none;">
                <div class="taskaction"><input type="checkbox" id="taskSteamCleanFQ"/></div>
                <div class="tasklabel"><label for="taskSteamCleanFQ">Steam clean FQ task</label></div>
            </div>
        </div><!-- end #tasklist -->

        <div id="grade" style="display:none;">
            <div id="gradeerror"></div>
            <div id="gradecontainer">
                <div id="gradeprompt">Container grade:</div>
                <div id="gradecontrol"><select id="containergrade"><option value="">Select...</option><option value="1">G1 - Dairy Standard</option><option value="2">G2 - Upgradeable</option><option value="3">G3 - General</option><option value="4">G4 - Unavailable</option><option value="14">G14 - MSK SCRAP</option><option value="21">G21 - Hides / Pelts / Scrap</option><option value="52">G52 - Sanitised & Sealed</option><option value="74">G74 - DB Drying</option></select></div>
            </div>
        </div>

        <!-- -------------------------------------- -->
        <div style="padding:.5em 0;">
            <div id="savemessage">Saving will complete the relevant wash tasks in Jade</div>
            <div id="savewashcompletion" class="fauxbutton"><span>Save and complete wash tasks ></span></div>
        </div><!-- end #saveinspection -->

    </div><!-- end #main -->
    
    <!-- -------------------------------------- -->
    <div id="footer">
        <div id="environmentinfo">
            <div>Washpad App<br /><span id="EnvironmentLabel" runat="server"></span> <span id="AppVersionLabel">v<% Response.Write(typeof(Washpad).Assembly.GetName().Version.ToString()); %></span></div>
            <img src="images/logo.png" alt="POL Logo" />
        </div><!-- end #environmentinfo -->
        <div id="usermanagement">
            <form id="logout" runat="server">
                <asp:Button ID="Submit1" OnClick="Signout_Click" Text="Log Out" runat="server" /><p>
            </form>
        </div><!-- end #usermanagement -->
    </div><!-- end #footer -->
    <div id="StyleStub" runat="server"><% Response.Write("<style>#main{background: #" + ConfigurationManager.AppSettings["BackgroundColorHex"].ToString() + " !important;}</style>");%></div>

</body>

<script src="js/POL-completewash.js?version=<% Response.Write(typeof(Washpad).Assembly.GetName().Version.ToString());%>" type="text/javascript"></script>

</html>