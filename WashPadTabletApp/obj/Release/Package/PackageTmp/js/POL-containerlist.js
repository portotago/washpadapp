﻿// --------------------------------------------------------------------------------------------
// --------------------------------------------- CONFIG ---------------------------------------
// --------------------------------------------------------------------------------------------
var containersListURL = "data/getcontainers.aspx";
var submissionURL = "data/clearwashpad.aspx";
var redirectURL = "default.aspx";
var heatbeatFileURL = "heartbeat/alive.txt";
var heartBeatFreq = 10  // seconds delay in checking connectivity
var statusFlashDurationMS = 1111;
var refreshListInterval = 15; // seconds delay before checking the list of containers and status in WashDB


// --------------------------------------------------------------------------------------------
// ------------------------------ PRELIMINARY CACHING OF CONTAINER NOS ------------------------
// --------------------------------------------------------------------------------------------

// ------------------ cache containers list --------------------------
//var ContainersCache = refreshContainerCache();
var ContainersCache = "";

// ------------------- initiate heartbeat ----------------------------
setInterval(function () {
    $.ajax({
        url: heatbeatFileURL + '?random=' + Math.random().toString(36).substring(7),
        type: 'HEAD',
        error: function () {
            //file can't be retrieved, assume connection problem
            $.blockUI({
                message: '<h1 style="margin:.5em">Error with connection to POL, check 3G connection and/or VPN.</h1><p style="font-size:16pt;">Retrying in ' + heartBeatFreq + ' seconds...</p>',
                title: "In-house moves app",
                css: {
                    padding: 0,
                    margin: 0,
                    width: '70%',
                    top: '20%',
                    left: '15%',
                    textAlign: 'center',
                    color: '#f00',
                    border: '3px solid #aaa',
                    backgroundColor: '#fff',
                    cursor: 'wait'
                }
            });
        },
        success: function () {
            //file exists
            $.unblockUI();
        }
    });
}, heartBeatFreq * 1000);



// --------------------------------------------------------------------------------------------
// ---------------------------------------- FUNCTIONS -----------------------------------------
// --------------------------------------------------------------------------------------------

function StatusFlash(message) {
    $("#StatusContainer").show();
    $("#StatusBar").html(message);
    $("#StatusBar").hide().fadeIn(500).delay(statusFlashDurationMS).fadeOut(1000).hide(function () {
        $("#StatusContainer").hide();
    });
}  // end StatusFlash


// function to retrieve querystring parameters
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function refreshContainerCache() {
    //console.log("Refreshing cache of containers...");
    var json = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': containersListURL + "?random=" + Math.random().toString(36).substring(7),
        'dataType': "json",
        'success': function (data) {
            console.log("Successfully refreshed containers cache");
            //console.log(JSON.stringify(data));
            json = data;
        }
    });
    return json;
}

function submitClearWashpad() {
    console.log('Sending washpad clear...');
    var posting = $.post(submissionURL, { washpad: getParameterByName("washpad") });
    posting.done(function (data) {
        // Redirect to the relevant washpad list
        window.location = redirectURL + "?msgtype=Success&msgtext=Washpad " + getParameterByName("washpad") + " strip request sent";
    });
    posting.fail(function () {
        //failure of posting to database
        alert("Error: Sorry I couldn't clear the washpad, pick up the phone!");
    });
} // end function submitClearWashpad


// --------------------------------------------------------------------------------------------
function renderContainersList (){
    //refresh cache of container numbers in case has been udpated
    ContainersCache = refreshContainerCache();

    $('#containerlist').html("");

    // get washpad number from querystring (default to 1 if parameter not present or valid)
    washpadId = getParameterByName("washpad");
    if (washpadId < 1 || washpadId > 4) {
        washpadId = 1;
    }

    washpadName = "W" + washpadId;
    $('#selectedwashpad').html(washpadName);
    $('#selectedwashpad2').html(washpadName);
    $('#selectedwashpad3').html(washpadName);
    console.log(washpadName);

    $.each(ContainersCache, function (i, v) {
        if (v.Location == washpadName) {
            // Determine whether inspection/wash/repair is applicable and/or required and display buttons accordingly

            // Determine state of wash - check applicable wash task statuses
            function isTodo(taskStatus) {
                if (taskStatus > 19 && taskStatus < 25) {
                    return true;
                } else { 
                    return false
                }
            }

            // Determine the state of the wash button
            var washState = "0";
            if ((v.taskInspect > 30) || (v.isReefer == 0)) {
                if (isTodo(v.taskWash) || isTodo(v.taskBaseWash) || isTodo(v.taskRoofWash) || isTodo(v.taskExternalWash) || isTodo(v.taskSteamClean) || isTodo(v.taskSteamCleanFQ)) {
                    // if any wash tasks are identified but not done,  status is TO WASH 20
                    washState = "20";
                }
                if (v.taskWash >= "30" || v.taskBaseWash >= "30" || v.taskRoofWash >= "30" || v.taskExternalWash >= "30" || v.taskSteamClean >= "30" || v.taskSteamCleanFQ >= "30") {
                    // if any wash tasks have been completed (30 or 40),  status is WASHING 25
                    washState = "25";
                }
                if ((v.taskWash >= "30" || v.taskWash == 0) && (v.taskBaseWash >= "30" || v.taskBaseWash == 0) && (v.taskRoofWash >= "30" || v.taskRoofWash == 0) && (v.taskExternalWash >= "30" || v.taskExternalWash == 0) && (v.taskSteamClean >= "30" || v.taskSteamClean == 0) && (v.taskSteamCleanFQ >= "30" || v.taskSteamCleanFQ == 0)) {
                    // if ALL wash tasks have either been completed 30/40, or are zero, then status is COMPELTED 40
                    washState = "40";
                }
                if (v.taskWash == 0 && v.taskBaseWash == 0 && v.taskRoofWash == 0 && v.taskExternalWash == 0 && v.taskSteamClean == 0 && v.taskSteamCleanFQ == 0) {
                    //if ALL wash tasks are zero, status is NOT APPLICABLE 0
                    washState = "0";
                }
            }

            // build up HTML for container line
            if (v.quoteStatus == "incomplete" || v.taskMoveToRepair > 0) {
                // a submitted estimate is failing to go to DepotPRO, so indicate this in GUI
                containerLineHTML = "<div id=\"" + v.id + "\" class=\"container incompleteQuote\">";
            } else {
                containerLineHTML = "<div id=\"" + v.id + "\" class=\"container\">";
            }

            if (v.taskInspect == 30) {
                // Task is in submission state, display this in GUI
                if (v.isReefer == 1) {
                    containerLineHTML = containerLineHTML + "<img src='images/icon-snowflake.png' width='32' style='float:left;margin:.35em -.2em 0 .2em' />";
                    containerLineHTML = containerLineHTML + "<span class='containerid reefer'>" + v.id + "</span>";
                } else {
                    containerLineHTML = containerLineHTML + "<img src='images/icon-dry.png' width='32' height='32' style='float:left;margin:.35em -.2em 0 .2em' />";
                    containerLineHTML = containerLineHTML + "<span class='containerid dry'>" + v.id + "</span>";
                }
                containerLineHTML = containerLineHTML + "<span><img src='images/loading.png' style='padding:0 .5em;opacity:.5;' class='ProgressIcon'/></span><span style='margin-top:.3em;display:block;opacity:.5;'>Submitting inspection</span></div>";
            }
            else {
                // Construct full line with controls for Inspect / Repair / Wash
                if (v.isReefer == 1) {
                    containerLineHTML = containerLineHTML + "<img src='images/icon-snowflake.png' width='32' style='float:left;margin:.35em -.2em 0 .2em' />";
                    containerLineHTML = containerLineHTML + "<span class='containerid reefer'>" + v.id + "</span>";
                } else {
                    containerLineHTML = containerLineHTML + "<img src='images/icon-dry.png' width='32' height='32' style='float:left;margin:.35em -.2em 0 .2em' />";
                    containerLineHTML = containerLineHTML + "<span class='containerid dry'>" + v.id + "</span>";
                }

                // Add inspect button, linked or not depending on taskInspection status
                if (v.isReefer == 0)  {
                    containerLineHTML = containerLineHTML + "<span class='ButtonNA'>N/A</span>"; // Drys can't be inspected with this app
                }
                else if (v.taskInspect == "0" || v.taskInspect == "10" || v.taskInspect == "30" || v.taskInspect == "40")  {
                    containerLineHTML = containerLineHTML + "<span class='ButtonInspect _" + v.taskInspect + "'>Inspect</span>";
                } else {
                    containerLineHTML = containerLineHTML + "<a href=\"inspect.aspx?containerid=" + v.id + "&oid=" + v.oid + "&washpad=" + washpadId + "\"><span class='ButtonInspect _" + v.taskInspect + "'>Inspect</span></a>";
                }

                // Add repair button, linked or not depending on taskInspect status, and taskRepair status
                if (v.isReefer == 0) {
                    containerLineHTML = containerLineHTML + "<span class='ButtonNA'>N/A</span>"; // Drys repairs can't be done with this app
                }
                else if (parseInt(v.taskInspect) <= 30 || v.taskRepair == "0" || v.taskRepair == "10" || v.taskRepair == "30" || v.taskRepair == "40") {
                    containerLineHTML = containerLineHTML + "<span class='ButtonRepair _" + v.taskRepair + "'>Repair</span>";
                } else {
                    containerLineHTML = containerLineHTML + "<a href=\"completerepair.aspx?containerid=" + v.id + "&oid=" + v.oid + "&washpad=" + washpadId + "\"><span class='ButtonRepair _" + v.taskRepair + "'>Repair</span></a>";
                }

                // Add wash button, linked or not depending on wash status (value already incorporates taskInspect status)
                if (washState == "0" || washState == "10" || washState == "30" || washState == "40") {
                    containerLineHTML = containerLineHTML + "<span class='ButtonWash _" + washState + "'>Wash</span>";
                } else {
                    containerLineHTML = containerLineHTML + "<a href=\"completewash.aspx?containerid=" + v.id + "&oid=" + v.oid + "&washpad=" + washpadId + "\"><span class='ButtonWash _" + washState + "'>Wash</span></a>";
                }

            }

            containerLineHTML = containerLineHTML + "</div>";
            // render line to page
            $('#containerlist').append(containerLineHTML);

        } // end if container match
        return;
    });                    // end each containersCache

} // end function renderContainersList




// ---------------------------------------- Selection filters -------------------------------------
$("#containerstatusselection ul li").click(function () {
    console.log('filtering container list...');

    // if all
    if ($(this).attr('id') == "all") {
        $('#containerlist .container').css("display", "block");
    } 
    
    // if toinspect  
    if ($(this).attr('id') == "toinspect") {
        $('#containerlist .container').css("display", "none");
        // Show any containers that have inspections to start or are in progress
        $('#containerlist .ButtonInspect._20').parent().parent().css("display", "block");
        $('#containerlist .ButtonInspect._23').parent().parent().css("display", "block");
        $('#containerlist .ButtonInspect._25').parent().parent().css("display", "block");
    }

    // if towash
    if ($(this).attr('id') == "towash") {
        $('#containerlist .container').css("display", "none");
        $('#containerlist .ButtonWash._20').parent().parent().css("display", "block");
        $('#containerlist .ButtonWash._23').parent().parent().css("display", "block");
        $('#containerlist .ButtonWash._25').parent().parent().css("display", "block");
    }

    // if torepair
    if ($(this).attr('id') == "torepair") {
        $('#containerlist .container').css("display", "none");
        $('#containerlist .ButtonRepair._20').parent().parent().css("display", "block");
        $('#containerlist .ButtonRepair._23').parent().parent().css("display", "block");
        $('#containerlist .ButtonRepair._25').parent().parent().css("display", "block");
    }

    // now highlight the selected button
    $('#containerstatusselection ul li').removeClass();
    $('#containerstatusselection ul li').addClass('unselectedstatus');
    $(this).removeClass();
    $(this).addClass('selectedstatus');

});


// ---------------------------- Add 'clicked' class to any elements that could go slowly...
// note different selector syntax because navigation dynamically generated
$('body').on('click', '.ButtonInspect,.ButtonRepair,.ButtonWash,#washpadclearconfirmed', function () {
    $(this).addClass('clicked');
});



// ---------------------------- Clear washpad button
$("#clearPad").click(function () {
    console.log('clearing washpad...');
    $('#clearwashpadconfirmation').dialog('open');
});
    
// ----------------- Popup config ---------------------
$('#clearwashpadconfirmation').dialog({
    autoOpen: false, // Do not open on page load
    width: "100%",
    modal: true, // Freeze the background behind the overlay
    open: function () {
        // close popup if clicked outside
        jQuery('.ui-widget-overlay').bind('click', function () {
            jQuery('#clearwashpadconfirmation').dialog('close');
        })
    }
});

$('#cancel').click(function () {
    $('#clearwashpadconfirmation').dialog('close');
});

$('#washpadclearconfirmed').click(function () {
    submitClearWashpad();
});
// ---------------- Start the completion popup -----------------



// ----------------------------------------------------------------------------------------------------
// ----------------------------------------- Initialise App -------------------------------------------
// ----------------------------------------------------------------------------------------------------
$(document).ready(function () {

    // Check for containers in WashDB on init, and then every refreshListInterval seconds
    renderContainersList();
    setInterval(renderContainersList, refreshListInterval * 1000);

    if (getParameterByName("msgtype") == "Success") {
        StatusFlash(getParameterByName("msgtext"));
    }
});
