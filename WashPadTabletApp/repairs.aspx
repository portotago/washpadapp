﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="repairs.aspx.cs" Inherits="WashPadTabletApp.repairs" %>

<script runat="server">
  void Page_Load(object sender, EventArgs e)
    {
        Submit1.Text = "Logout " + Context.User.Identity.Name;
        // set the environment label
        EnvironmentLabel.InnerHtml = ConfigurationManager.AppSettings["EnvironmentLabel"].ToString();
    }

  void Signout_Click(object sender, EventArgs e)
  {
    FormsAuthentication.SignOut();
    Response.Redirect("Logon.aspx");
  }
</script>

<html>
<head>
    <title>WashPad Tablet App</title>
    <!-- fix for jQuery 2.x on IE11 -->
    <meta http-equiv="X-UA-Compatible" content="IE=10;" />

    <!-- javascript libraries -->
    <script src="js/jquery.v2.1.4.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui.v1.11.4.min.js" type="text/javascript"></script>
    <script src="js/jquery.blockUI.js" type="text/javascript"></script>
    <script src="js/underscore-min.js" type="text/javascript"></script>

    <!-- style -->
    <link rel="stylesheet" type="text/css" href="css/POL-TabletStyle.css?version=<% Response.Write(typeof(Washpad).Assembly.GetName().Version.ToString());%>" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />

    <!-- mobile icons -->
    <link rel="icon" sizes="128x128" href="images/launch-icon-128.png" />
    <link rel="apple-touch-startup-image" href="images/launch-icon-128.png" />
    <link rel="apple-touch-icon" sizes="128x128" href="images/launch-icon-128.png">

    <!-- mobile web app flags -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-title" content="Port Chalmers Washpad">
        
</head>

<body>

<!-- Image maps for damage location selectors -->
<div id="LocationMap">
    <p>Select location:</p>
    <img src="images/locations_Door_20.jpg" usemap="#locations_Door" id="locationSelectionImage" width="650" height="650" alt="location selection"/>
</div>

<map name="locations_Door_20">
    <area shape="rect" class="LocationZone" coords="50,0,150,200" id="DT1N" alt="DT1N" />
    <area shape="rect" class="LocationZone" coords="150,0,350,200" id="DT2N" alt="DT2N" />
    <area shape="rect" class="LocationZone" coords="350,0,550,200" id="DT3N" alt="DT3N" />
    <area shape="rect" class="LocationZone" coords="550,0,650,200" id="DT4N" alt="DT4N" />
    <area shape="rect" class="LocationZone" coords="50,200,150,400" id="DX1N" alt="DX1N" />
    <area shape="rect" class="LocationZone" coords="150,200,350,400" id="DX2N" alt="DX2N" />
    <area shape="rect" class="LocationZone" coords="350,200,550,400" id="DX3N" alt="DX3N" />
    <area shape="rect" class="LocationZone" coords="550,200,650,400" id="DX4N" alt="DX4N" />
    <area shape="rect" class="LocationZone" coords="50,400,150,600" id="DB1N" alt="DB1N" />
    <area shape="rect" class="LocationZone" coords="150,400,350,600" id="DB2N" alt="DB2N" />
    <area shape="rect" class="LocationZone" coords="350,400,550,600" id="DB3N" alt="DB3N" />
    <area shape="rect" class="LocationZone" coords="550,400,650,600" id="DB4N" alt="DB4N" />
</map>
<map name="locations_FrontInterior_20">
    <area shape="rect" class="LocationZone" coords="50,0,150,200" id="FT4N" alt="FT4N" />
    <area shape="rect" class="LocationZone" coords="150,0,350,200" id="FT3N" alt="FT3N" />
    <area shape="rect" class="LocationZone" coords="350,0,550,200" id="FT2N" alt="FT2N" />
    <area shape="rect" class="LocationZone" coords="550,0,650,200" id="FT1N" alt="FT1N" />
    <area shape="rect" class="LocationZone" coords="50,200,150,400" id="FX4N" alt="FX4N" />
    <area shape="rect" class="LocationZone" coords="150,200,350,400" id="FX3N" alt="FX3N" />
    <area shape="rect" class="LocationZone" coords="350,200,550,400" id="FX2N" alt="FX2N" />
    <area shape="rect" class="LocationZone" coords="550,200,650,400" id="FX1N" alt="FX1N" />
    <area shape="rect" class="LocationZone" coords="50,400,150,600" id="FB4N" alt="FB4N" />
    <area shape="rect" class="LocationZone" coords="150,400,350,600" id="FB3N" alt="FB3N" />
    <area shape="rect" class="LocationZone" coords="350,400,550,600" id="FB2N" alt="FB2N" />
    <area shape="rect" class="LocationZone" coords="550,400,650,600" id="FB1N" alt="FB1N" />
</map>
<map name="locations_SideLeft_20">
    <area shape="rect" class="LocationZone" coords="50,0,170,84" id="LT5N" alt="LT5N" />
    <area shape="rect" class="LocationZone" coords="170,0,290,84" id="LT4N" alt="LT4N" />
    <area shape="rect" class="LocationZone" coords="290,0,410,84" id="LT3N" alt="LT3N" />
    <area shape="rect" class="LocationZone" coords="410,0,530,84" id="LT2N" alt="LT2N" />
    <area shape="rect" class="LocationZone" coords="530,0,650,84" id="LT1N" alt="LT1N" />
    <area shape="rect" class="LocationZone" coords="50,84,170,168" id="LX5N" alt="LX5N" />
    <area shape="rect" class="LocationZone" coords="170,84,290,168" id="LX4N" alt="LX4N" />
    <area shape="rect" class="LocationZone" coords="290,84,410,168" id="LX3N" alt="LX3N" />
    <area shape="rect" class="LocationZone" coords="410,84,530,168" id="LX2N" alt="LX2N" />
    <area shape="rect" class="LocationZone" coords="530,84,650,168" id="LX1N" alt="LX1N" />
    <area shape="rect" class="LocationZone" coords="50,168,170,250" id="LB5N" alt="LB5N" />
    <area shape="rect" class="LocationZone" coords="170,168,290,250" id="LB4N" alt="LB4N" />
    <area shape="rect" class="LocationZone" coords="290,168,410,250" id="LB3N" alt="LB3N" />
    <area shape="rect" class="LocationZone" coords="410,168,530,250" id="LB2N" alt="LB2N" />
    <area shape="rect" class="LocationZone" coords="530,168,650,250" id="LB1N" alt="LB1N" />
</map>
<map name="locations_SideRight_20">
    <area shape="rect" class="LocationZone" coords="50,0,170,84" id="RT1N" alt="RT1N" />
    <area shape="rect" class="LocationZone" coords="170,0,290,84" id="RT2N" alt="RT2N" />
    <area shape="rect" class="LocationZone" coords="290,0,410,84" id="RT3N" alt="RT3N" />
    <area shape="rect" class="LocationZone" coords="410,0,530,84" id="RT4N" alt="RT4N" />
    <area shape="rect" class="LocationZone" coords="530,0,650,84" id="RT5N" alt="RT5N" />
    <area shape="rect" class="LocationZone" coords="50,84,170,168" id="RX1N" alt="RX1N" />
    <area shape="rect" class="LocationZone" coords="170,84,290,168" id="RX2N" alt="RX2N" />
    <area shape="rect" class="LocationZone" coords="290,84,410,168" id="RX3N" alt="RX3N" />
    <area shape="rect" class="LocationZone" coords="410,84,530,168" id="RX4N" alt="RX4N" />
    <area shape="rect" class="LocationZone" coords="530,84,650,168" id="RX5N" alt="RX5N" />
    <area shape="rect" class="LocationZone" coords="50,168,170,250" id="RB1N" alt="RB1N" />
    <area shape="rect" class="LocationZone" coords="170,168,290,250" id="RB2N" alt="RB2N" />
    <area shape="rect" class="LocationZone" coords="290,168,410,250" id="RB3N" alt="RB3N" />
    <area shape="rect" class="LocationZone" coords="410,168,530,250" id="RB4N" alt="RB4N" />
    <area shape="rect" class="LocationZone" coords="530,168,650,250" id="RB5N" alt="RB5N" />
</map>
<map name="locations_RoofInterior_20">
    <area shape="rect" class="LocationZone" coords="50,0,170,125" id="TL1N" alt="TL1N" />
    <area shape="rect" class="LocationZone" coords="170,0,290,125" id="TL2N" alt="TL2N" />
    <area shape="rect" class="LocationZone" coords="290,0,410,125" id="TL3N" alt="TL3N" />
    <area shape="rect" class="LocationZone" coords="410,0,530,125" id="TL4N" alt="TL4N" />
    <area shape="rect" class="LocationZone" coords="530,0,650,125" id="TL5N" alt="TL5N" />
    <area shape="rect" class="LocationZone" coords="50,126,170,250" id="TR1N" alt="TR1N" />
    <area shape="rect" class="LocationZone" coords="170,126,290,250" id="TR2N" alt="TR2N" />
    <area shape="rect" class="LocationZone" coords="290,126,410,250" id="TR3N" alt="TR3N" />
    <area shape="rect" class="LocationZone" coords="410,126,530,250" id="TR4N" alt="TR4N" />
    <area shape="rect" class="LocationZone" coords="530,126,650,250" id="TR5N" alt="TR5N" />
</map>
<map name="locations_RoofExterior_20">
    <area shape="rect" class="LocationZone" coords="50,0,170,125" id="_TL1N" alt="TL1N" />
    <area shape="rect" class="LocationZone" coords="170,0,290,125" id="_TL2N" alt="TL2N" />
    <area shape="rect" class="LocationZone" coords="290,0,410,125" id="_TL3N" alt="TL3N" />
    <area shape="rect" class="LocationZone" coords="410,0,530,125" id="_TL4N" alt="TL4N" />
    <area shape="rect" class="LocationZone" coords="530,0,650,125" id="_TL5N" alt="TL5N" />
    <area shape="rect" class="LocationZone" coords="50,126,170,250" id="_TR1N" alt="TR1N" />
    <area shape="rect" class="LocationZone" coords="170,126,290,250" id="_TR2N" alt="TR2N" />
    <area shape="rect" class="LocationZone" coords="290,126,410,250" id="_TR3N" alt="TR3N" />
    <area shape="rect" class="LocationZone" coords="410,126,530,250" id="_TR4N" alt="TR4N" />
    <area shape="rect" class="LocationZone" coords="530,126,650,250" id="_TR5N" alt="TR5N" />
</map>
<map name="locations_Floor_20">
    <area shape="rect" class="LocationZone" coords="50,0,170,125" id="BL1N" alt="BL1N" />
    <area shape="rect" class="LocationZone" coords="170,0,290,125" id="BL2N" alt="BL2N" />
    <area shape="rect" class="LocationZone" coords="290,0,410,125" id="BL3N" alt="BL3N" />
    <area shape="rect" class="LocationZone" coords="410,0,530,125" id="BL4N" alt="BL4N" />
    <area shape="rect" class="LocationZone" coords="530,0,650,125" id="BL5N" alt="BL5N" />
    <area shape="rect" class="LocationZone" coords="50,126,170,250" id="BR1N" alt="BR1N" />
    <area shape="rect" class="LocationZone" coords="170,126,290,250" id="BR2N" alt="BR2N" />
    <area shape="rect" class="LocationZone" coords="290,126,410,250" id="BR3N" alt="BR3N" />
    <area shape="rect" class="LocationZone" coords="410,126,530,250" id="BR4N" alt="BR4N" />
    <area shape="rect" class="LocationZone" coords="530,126,650,250" id="BR5N" alt="BR5N" />
</map>
<map name="locations_Door_40">
    <area shape="rect" class="LocationZone" coords="50,0,150,200" id="_DT1N" alt="_DT1N" />
    <area shape="rect" class="LocationZone" coords="150,0,350,200" id="_DT2N" alt="_DT2N" />
    <area shape="rect" class="LocationZone" coords="350,0,550,200" id="_DT3N" alt="_DT3N" />
    <area shape="rect" class="LocationZone" coords="550,0,650,200" id="_DT4N" alt="_DT4N" />
    <area shape="rect" class="LocationZone" coords="50,200,150,400" id="_DX1N" alt="_DX1N" />
    <area shape="rect" class="LocationZone" coords="150,200,350,400" id="_DX2N" alt="_DX2N" />
    <area shape="rect" class="LocationZone" coords="350,200,550,400" id="_DX3N" alt="_DX3N" />
    <area shape="rect" class="LocationZone" coords="550,200,650,400" id="_DX4N" alt="_DX4N" />
    <area shape="rect" class="LocationZone" coords="50,400,150,600" id="_DB1N" alt="_DB1N" />
    <area shape="rect" class="LocationZone" coords="150,400,350,600" id="_DB2N" alt="_DB2N" />
    <area shape="rect" class="LocationZone" coords="350,400,550,600" id="_DB3N" alt="_DB3N" />
    <area shape="rect" class="LocationZone" coords="550,400,650,600" id="_DB4N" alt="_DB4N" />
</map>
<map name="locations_FrontInterior_40">
    <area shape="rect" class="LocationZone" coords="50,0,150,200" id="_FT4N" alt="_FT4N" />
    <area shape="rect" class="LocationZone" coords="150,0,350,200" id="_FT3N" alt="_FT3N" />
    <area shape="rect" class="LocationZone" coords="350,0,550,200" id="_FT2N" alt="_FT2N" />
    <area shape="rect" class="LocationZone" coords="550,0,650,200" id="_FT1N" alt="_FT1N" />
    <area shape="rect" class="LocationZone" coords="50,200,150,400" id="_FX4N" alt="_FX4N" />
    <area shape="rect" class="LocationZone" coords="150,200,350,400" id="_FX3N" alt="_FX3N" />
    <area shape="rect" class="LocationZone" coords="350,200,550,400" id="_FX2N" alt="_FX2N" />
    <area shape="rect" class="LocationZone" coords="550,200,650,400" id="_FX1N" alt="_FX1N" />
    <area shape="rect" class="LocationZone" coords="50,400,150,600" id="_FB4N" alt="_FB4N" />
    <area shape="rect" class="LocationZone" coords="150,400,350,600" id="_FB3N" alt="_FB3N" />
    <area shape="rect" class="LocationZone" coords="350,400,550,600" id="_FB2N" alt="_FB2N" />
    <area shape="rect" class="LocationZone" coords="550,400,650,600" id="_FB1N" alt="_FB1N" />
</map>
<map name="locations_SideLeft_40">
    <area shape="rect" class="LocationZone" coords="50,0,110,84" id="_LT0N" alt="_LT0N" />
    <area shape="rect" class="LocationZone" coords="110,0,170,84" id="_LT9N" alt="_LT9N" />
    <area shape="rect" class="LocationZone" coords="170,0,230,84" id="_LT8N" alt="_LT8N" />
    <area shape="rect" class="LocationZone" coords="230,0,290,84" id="_LT7N" alt="_LT7N" />
    <area shape="rect" class="LocationZone" coords="290,0,350,84" id="_LT6N" alt="_LT6N" />
    <area shape="rect" class="LocationZone" coords="350,0,410,84" id="_LT5N" alt="_LT5N" />
    <area shape="rect" class="LocationZone" coords="410,0,470,84" id="_LT4N" alt="_LT4N" />
    <area shape="rect" class="LocationZone" coords="470,0,530,84" id="_LT3N" alt="_LT3N" />
    <area shape="rect" class="LocationZone" coords="530,0,590,84" id="_LT2N" alt="_LT2N" />
    <area shape="rect" class="LocationZone" coords="590,0,650,84" id="_LT1N" alt="_LT1N" />
    <area shape="rect" class="LocationZone" coords="50,84,110,168" id="_LX0N" alt="_LX0N" />
    <area shape="rect" class="LocationZone" coords="110,84,170,168" id="_LX9N" alt="_LX9N" />
    <area shape="rect" class="LocationZone" coords="170,84,230,168" id="_LX8N" alt="_LX8N" />
    <area shape="rect" class="LocationZone" coords="230,84,290,168" id="_LX7N" alt="_LX7N" />
    <area shape="rect" class="LocationZone" coords="290,84,350,168" id="_LX6N" alt="_LX6N" />
    <area shape="rect" class="LocationZone" coords="350,84,410,168" id="_LX5N" alt="_LX5N" />
    <area shape="rect" class="LocationZone" coords="410,84,470,168" id="_LX4N" alt="_LX4N" />
    <area shape="rect" class="LocationZone" coords="470,84,530,168" id="_LX3N" alt="_LX3N" />
    <area shape="rect" class="LocationZone" coords="530,84,590,168" id="_LX2N" alt="_LX2N" />
    <area shape="rect" class="LocationZone" coords="590,84,650,168" id="_LX1N" alt="_LX1N" />
    <area shape="rect" class="LocationZone" coords="50,168,110,250" id="_LB0N" alt="_LB0N" />
    <area shape="rect" class="LocationZone" coords="110,168,170,250" id="_LB9N" alt="_LB9N" />
    <area shape="rect" class="LocationZone" coords="170,168,230,250" id="_LB8N" alt="_LB8N" />
    <area shape="rect" class="LocationZone" coords="230,168,290,250" id="_LB7N" alt="_LB7N" />
    <area shape="rect" class="LocationZone" coords="290,168,350,250" id="_LB6N" alt="_LB6N" />
    <area shape="rect" class="LocationZone" coords="350,168,410,250" id="_LB5N" alt="_LB5N" />
    <area shape="rect" class="LocationZone" coords="410,168,470,250" id="_LB4N" alt="_LB4N" />
    <area shape="rect" class="LocationZone" coords="470,168,530,250" id="_LB3N" alt="_LB3N" />
    <area shape="rect" class="LocationZone" coords="530,168,590,250" id="_LB2N" alt="_LB2N" />
    <area shape="rect" class="LocationZone" coords="590,168,650,250" id="_LB1N" alt="_LB1N" />
</map>
<map name="locations_SideRight_40">
    <area shape="rect" class="LocationZone" coords="50,0,110,84" id="_RT1N" alt="_RT1N" />
    <area shape="rect" class="LocationZone" coords="110,0,170,84" id="_RT2N" alt="_RT2N" />
    <area shape="rect" class="LocationZone" coords="170,0,230,84" id="_RT3N" alt="_RT3N" />
    <area shape="rect" class="LocationZone" coords="230,0,290,84" id="_RT4N" alt="_RT4N" />
    <area shape="rect" class="LocationZone" coords="290,0,350,84" id="_RT5N" alt="_RT5N" />
    <area shape="rect" class="LocationZone" coords="350,0,410,84" id="_RT6N" alt="_RT6N" />
    <area shape="rect" class="LocationZone" coords="410,0,470,84" id="_RT7N" alt="_RT7N" />
    <area shape="rect" class="LocationZone" coords="470,0,530,84" id="_RT8N" alt="_RT8N" />
    <area shape="rect" class="LocationZone" coords="530,0,590,84" id="_RT9N" alt="_RT9N" />
    <area shape="rect" class="LocationZone" coords="590,0,650,84" id="_RT0N" alt="_RT0N" />
    <area shape="rect" class="LocationZone" coords="50,84,110,168" id="_RX1N" alt="_RX1N" />
    <area shape="rect" class="LocationZone" coords="110,84,170,168" id="_RX2N" alt="_RX2N" />
    <area shape="rect" class="LocationZone" coords="170,84,230,168" id="_RX3N" alt="_RX3N" />
    <area shape="rect" class="LocationZone" coords="230,84,290,168" id="_RX4N" alt="_RX4N" />
    <area shape="rect" class="LocationZone" coords="290,84,350,168" id="_RX5N" alt="_RX5N" />
    <area shape="rect" class="LocationZone" coords="350,84,410,168" id="_RX6N" alt="_RX6N" />
    <area shape="rect" class="LocationZone" coords="410,84,470,168" id="_RX7N" alt="_RX7N" />
    <area shape="rect" class="LocationZone" coords="470,84,530,168" id="_RX8N" alt="_RX8N" />
    <area shape="rect" class="LocationZone" coords="530,84,590,168" id="_RX9N" alt="_RX9N" />
    <area shape="rect" class="LocationZone" coords="590,84,650,168" id="_RX0N" alt="_RX0N" />
    <area shape="rect" class="LocationZone" coords="50,168,110,250" id="_RB1N" alt="_RB1N" />
    <area shape="rect" class="LocationZone" coords="110,168,170,250" id="_RB2N" alt="_RB2N" />
    <area shape="rect" class="LocationZone" coords="170,168,230,250" id="_RB3N" alt="_RB3N" />
    <area shape="rect" class="LocationZone" coords="230,168,290,250" id="_RB4N" alt="_RB4N" />
    <area shape="rect" class="LocationZone" coords="290,168,350,250" id="_RB5N" alt="_RB5N" />
    <area shape="rect" class="LocationZone" coords="350,168,410,250" id="_RB6N" alt="_RB6N" />
    <area shape="rect" class="LocationZone" coords="410,168,470,250" id="_RB7N" alt="_RB7N" />
    <area shape="rect" class="LocationZone" coords="470,168,530,250" id="_RB8N" alt="_RB8N" />
    <area shape="rect" class="LocationZone" coords="530,168,590,250" id="_RB9N" alt="_RB9N" />
    <area shape="rect" class="LocationZone" coords="590,168,650,250" id="_RB0N" alt="_RB0N" />
</map>
<map name="locations_RoofInterior_40">
<area shape="rect" class="LocationZone" coords="50,0,110,125" id="___TL1N" alt="___TL1N" />
<area shape="rect" class="LocationZone" coords="110,0,170,125" id="___TL2N" alt="___TL2N" />
<area shape="rect" class="LocationZone" coords="170,0,230,125" id="___TL3N" alt="___TL3N" />
<area shape="rect" class="LocationZone" coords="230,0,290,125" id="___TL4N" alt="___TL4N" />
<area shape="rect" class="LocationZone" coords="290,0,350,125" id="___TL5N" alt="___TL5N" />
<area shape="rect" class="LocationZone" coords="350,0,410,125" id="___TL6N" alt="___TL6N" />
<area shape="rect" class="LocationZone" coords="410,0,470,125" id="___TL7N" alt="___TL7N" />
<area shape="rect" class="LocationZone" coords="470,0,530,125" id="___TL8N" alt="___TL8N" />
<area shape="rect" class="LocationZone" coords="530,0,590,125" id="___TL9N" alt="___TL9N" />
<area shape="rect" class="LocationZone" coords="590,0,650,125" id="___TL0N" alt="___TL0N" />
<area shape="rect" class="LocationZone" coords="50,126,110,250" id="___TR1N" alt="___TR1N" />
<area shape="rect" class="LocationZone" coords="110,126,170,250" id="___TR2N" alt="___TR2N" />
<area shape="rect" class="LocationZone" coords="170,126,230,250" id="___TR3N" alt="___TR3N" />
<area shape="rect" class="LocationZone" coords="230,126,290,250" id="___TR4N" alt="___TR4N" />
<area shape="rect" class="LocationZone" coords="290,126,350,250" id="___TR5N" alt="___TR5N" />
<area shape="rect" class="LocationZone" coords="350,126,410,250" id="___TR6N" alt="___TR6N" />
<area shape="rect" class="LocationZone" coords="410,126,470,250" id="___TR7N" alt="___TR7N" />
<area shape="rect" class="LocationZone" coords="470,126,530,250" id="___TR8N" alt="___TR8N" />
<area shape="rect" class="LocationZone" coords="530,126,590,250" id="___TR9N" alt="___TR9N" />
<area shape="rect" class="LocationZone" coords="590,126,650,250" id="___TR0N" alt="___TR0N" />
</map>

<map name="locations_Floor_40">
    <area shape="rect" class="LocationZone" coords="50,0,110,125" id="_BL1N" alt="_BL1N" />
    <area shape="rect" class="LocationZone" coords="110,0,170,125" id="_BL2N" alt="_BL2N" />
    <area shape="rect" class="LocationZone" coords="170,0,230,125" id="_BL3N" alt="_BL3N" />
    <area shape="rect" class="LocationZone" coords="230,0,290,125" id="_BL4N" alt="_BL4N" />
    <area shape="rect" class="LocationZone" coords="290,0,350,125" id="_BL5N" alt="_BL5N" />
    <area shape="rect" class="LocationZone" coords="350,0,410,125" id="_BL6N" alt="_BL6N" />
    <area shape="rect" class="LocationZone" coords="410,0,470,125" id="_BL7N" alt="_BL7N" />
    <area shape="rect" class="LocationZone" coords="470,0,530,125" id="_BL8N" alt="_BL8N" />
    <area shape="rect" class="LocationZone" coords="530,0,590,125" id="_BL9N" alt="_BL9N" />
    <area shape="rect" class="LocationZone" coords="590,0,650,125" id="_BL0N" alt="_BL0N" />
    <area shape="rect" class="LocationZone" coords="50,126,110,250" id="_BR1N" alt="_BR1N" />
    <area shape="rect" class="LocationZone" coords="110,126,170,250" id="_BR2N" alt="_BR2N" />
    <area shape="rect" class="LocationZone" coords="170,126,230,250" id="_BR3N" alt="_BR3N" />
    <area shape="rect" class="LocationZone" coords="230,126,290,250" id="_BR4N" alt="_BR4N" />
    <area shape="rect" class="LocationZone" coords="290,126,350,250" id="_BR5N" alt="_BR5N" />
    <area shape="rect" class="LocationZone" coords="350,126,410,250" id="_BR6N" alt="_BR6N" />
    <area shape="rect" class="LocationZone" coords="410,126,470,250" id="_BR7N" alt="_BR7N" />
    <area shape="rect" class="LocationZone" coords="470,126,530,250" id="_BR8N" alt="_BR8N" />
    <area shape="rect" class="LocationZone" coords="530,126,590,250" id="_BR9N" alt="_BR9N" />
    <area shape="rect" class="LocationZone" coords="590,126,650,250" id="_BR0N" alt="_BR0N" />
</map>
<map name="locations_RoofExterior_40">
    <area shape="rect" class="LocationZone" coords="50,0,110,125" id="__TL1N" alt="__TL1N" />
    <area shape="rect" class="LocationZone" coords="110,0,170,125" id="__TL2N" alt="__TL2N" />
    <area shape="rect" class="LocationZone" coords="170,0,230,125" id="__TL3N" alt="__TL3N" />
    <area shape="rect" class="LocationZone" coords="230,0,290,125" id="__TL4N" alt="__TL4N" />
    <area shape="rect" class="LocationZone" coords="290,0,350,125" id="__TL5N" alt="__TL5N" />
    <area shape="rect" class="LocationZone" coords="350,0,410,125" id="__TL6N" alt="__TL6N" />
    <area shape="rect" class="LocationZone" coords="410,0,470,125" id="__TL7N" alt="__TL7N" />
    <area shape="rect" class="LocationZone" coords="470,0,530,125" id="__TL8N" alt="__TL8N" />
    <area shape="rect" class="LocationZone" coords="530,0,590,125" id="__TL9N" alt="__TL9N" />
    <area shape="rect" class="LocationZone" coords="590,0,650,125" id="__TL0N" alt="__TL0N" />
    <area shape="rect" class="LocationZone" coords="50,126,110,250" id="__TR1N" alt="__TR1N" />
    <area shape="rect" class="LocationZone" coords="110,126,170,250" id="__TR2N" alt="__TR2N" />
    <area shape="rect" class="LocationZone" coords="170,126,230,250" id="__TR3N" alt="__TR3N" />
    <area shape="rect" class="LocationZone" coords="230,126,290,250" id="__TR4N" alt="__TR4N" />
    <area shape="rect" class="LocationZone" coords="290,126,350,250" id="__TR5N" alt="__TR5N" />
    <area shape="rect" class="LocationZone" coords="350,126,410,250" id="__TR6N" alt="__TR6N" />
    <area shape="rect" class="LocationZone" coords="410,126,470,250" id="__TR7N" alt="__TR7N" />
    <area shape="rect" class="LocationZone" coords="470,126,530,250" id="__TR8N" alt="__TR8N" />
    <area shape="rect" class="LocationZone" coords="530,126,590,250" id="__TR9N" alt="__TR9N" />
    <area shape="rect" class="LocationZone" coords="590,126,650,250" id="__TR0N" alt="__TR0N" />
</map>


    <div id="main">
        <div id="breadcrumb"><span id="selectedwashpad">__</span> / <span id="containernumber">__</span></div>
        <div id="StatusContainer">
            <div id="StatusBar"></div>
        </div>

        <div class="repairentry">
            <div class="repairheading">
                <div class="repairentrylabel">Add a repair:</div>
                <input type="hidden" id="Repair_ID_1" value="" />
            </div>
            <div id="repair_location" class="repair" >
                <div class="repairlabel"><span>Location</span></div>
                <div class="repairselection">
                    <select name="Repair_Category_1" id="Repair_Category_1" class="RepairLineCategorySelector" data-native="true">
                        <option value="null" id="locationdefault">Choose...</option>
                        <option value="Door">Door</option>
                        <option value="FrontInterior">FrontInterior</option>
                        <option value="Floor">Floor</option>
                        <option value="RoofInterior">RoofInterior</option>
                        <option value="RoofExterior">RoofExterior</option>
                        <option value="SideLeft">SideLeft</option>
                        <option value="SideRight">SideRight</option>
                    </select>
                    <input type="text" id="Repair_Location_1" class="repairLocationLabel" value="TBA" style="width:150px !important;" readonly>
                </div>
            </div>
            <div id="task_basewash" class="repair" >
                <div class="repairlabel"><span>Repair</span></div>
                <div class="repairselection">
                    <span id="repairselectorplaceholder_1" class="repairselectorplaceholder">select location first</span>
                    <select name="Repair_Code_1" id="Repair_Code_1" style="width:450px !important;display:none;" class="repairselector" data-native="true"><option id="chooser">Choose...</option><!-- options populated by jQuery --></select>
                </div>
            </div>
            <div id="task_roofwash" class="repair">
                <div class="repairlabel"><span>Quantity</span></div>
                <div class="repairselection">
                    <span id="qtyplaceholder">select repair first</span>
                    <select name="Repair_QTY_1" id="Repair_QTY_1" data-native="true" style="display:none;">
                        <option>1</option>
                    </select>
                    <!-- quantity options populated by jQuery -->
                </div>
            </div>

        </div><!-- end #repairentry -->

     
       <!--<div id="AddRepairLine"><span><b>+</b> Add another repair</span></div>-->
       
       <div id="saverepairs"><span>< Cancel</span></div>

    </div><!-- end #main -->

    <!-- -------------------------------------- -->
    <div id="footer">
        <div id="environmentinfo">
            <div>Washpad App<br /><span id="EnvironmentLabel" runat="server"></span> <span id="AppVersionLabel">v<% Response.Write(typeof(Washpad).Assembly.GetName().Version.ToString()); %></span></div>
            <img src="images/logo.png" alt="POL Logo" />
        </div><!-- end #environmentinfo -->
        <div id="usermanagement">
            <form id="logout" runat="server">
                <asp:Button ID="Submit1" OnClick="Signout_Click" Text="Log Out" runat="server" /><p>
            </form>
        </div><!-- end #usermanagement -->
    </div><!-- end #footer -->
    <div id="StyleStub" runat="server"><% Response.Write("<style>#main{background: #" + ConfigurationManager.AppSettings["BackgroundColorHex"].ToString() + " !important;}</style>");%></div>

</body>

<script src="js/POL-repairs.js?version=<% Response.Write(typeof(Washpad).Assembly.GetName().Version.ToString());%>" type="text/javascript"></script>

</html>


