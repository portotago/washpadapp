USE WashDB

ALTER TABLE dbo.containerStates ADD
completedBaseWash dateTime,
completedExternalWash dateTime,
completedRoofWash dateTime,
completedSteamClean dateTime,
completedSteamCleanFQ dateTime
