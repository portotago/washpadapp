DECLARE @pad VARCHAR(MAX)
SET @pad = 'W4'

/* REPAIR TIMINGS FOR A WASHPAD */
SELECT * FROM [WashDB].[dbo].[containerRepairs] where containerID in (SELECT containerID FROM [WashDB].[dbo].[containerStates] where (exitedWashpad IS NULL OR exitedWashpad = 0) and location = @pad) ORDER BY completedRepair ASC

/* INSPECTION AND WASH COMPLETION TIMINGS FOR WASHPAD */
--SELECT containerId, completedInspection,completedWash FROM [WashDB].[dbo].[containerStates] where (exitedWashpad IS NULL OR exitedWashpad = 0) and location = @pad Order by completedInspection

/* REPAIRS COUNT FOR WASHPAD */
--SELECT COUNT (*) FROM [WashDB].[dbo].[containerRepairs] where containerID in (SELECT containerID FROM [WashDB].[dbo].[containerStates] where (exitedWashpad IS NULL OR exitedWashpad = 0) and location = @pad)