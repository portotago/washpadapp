USE WashDB;   
GRANT EXECUTE ON OBJECT::dbo.spAddContainerToWashpad
    TO "WashpadUser";  
GRANT EXECUTE ON OBJECT::dbo.spGetContainersForInspectionCompletion
    TO "WashpadUser";   
GRANT EXECUTE ON OBJECT::dbo.spGetContainerState
    TO "WashpadUser";   
GRANT EXECUTE ON OBJECT::dbo.spGetContainerStates
    TO "WashpadUser";   
GRANT EXECUTE ON OBJECT::dbo.spGetRerepairsForContainer
    TO "WashpadUser";   
GRANT EXECUTE ON OBJECT::dbo.spInsertContainerRepair
    TO "WashpadUser";   
GRANT EXECUTE ON OBJECT::dbo.spRemoveContainersFromWash
    TO "WashpadUser";   
GRANT EXECUTE ON OBJECT::dbo.spSetContainerState
    TO "WashpadUser";   
GRANT EXECUTE ON OBJECT::dbo.spSetContainerTaskCompletion
    TO "WashpadUser";  
GRANT EXECUTE ON OBJECT::dbo.spUnremoveContainerFromWash
    TO "WashpadUser";  
GO
