USE [Statsdb]
GO
/****** Object:  StoredProcedure [dbo].[spGetContainersToWeigh]    Script Date: 11/18/2016 10:44:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID(N'spGetContainersOnWash') is not null 
	DROP procedure dbo.spGetContainersOnWash
GO

CREATE PROCEDURE [dbo].spGetContainersOnWash
AS
/*
	This stored procedure is used by the Washpad Tablet app to find out what containers are on Wash at the start of the day
*/
	SELECT ti.id
		, ti.oid
		, ti.operatorCode 
		, i.isReefer
		, i.is40ftLong 
		, tila.Location
		, tila.EffectiveDT WentToWash
		, tm.description as RecTransportMode
		, CASE WHEN ti.unpackedDT is null THEN 'YES' ELSE 'NO' END as RecMT
		, CASE WHEN ti.inspectDT < ti.receivedDT THEN 'YES' ELSE 'NO' END as PT_lessthan_Rec
		, wash.toDoTaskDescShort as Wash
		, base.toDoTaskDescShort as BaseWash
		, roof.toDoTaskDescShort as RoofWash
		, extc.toDoTaskDescShort as ExternalClean
		, phot.toDoTaskDescShort as Photo
		, repa.toDoTaskDescShort as Repair
		, stmc.toDoTaskDescShort as SteamClean
		, scfq.toDoTaskDescShort as SteamCleanFQ
		, reqi.toDoTaskDescShort as RequiresInspection
		, dbu3.toDoTaskDescShort as DBUpgrade3
		, dbca.toDoTaskDescShort as DBCertAdmin
	FROM statsdb.dbo.trackeditem ti
	INNER JOIN Statsdb.dbo.isotype i on i.code = ti.isoType 
	INNER JOIN Statsdb.dbo.tblTrackedItemLocationAssign tila on tila.TrackedItem = ti.oid 
	INNER JOIN Statsdb.dbo.transportmodes tm on tm.mode = ti.receiptTransportMode 
	LEFT JOIN Statsdb.dbo.todotask wash on wash.myTrackedItem = ti.oid and wash.status = 0 and wash.toDoTaskDescShort = 'WASH'
	LEFT JOIN Statsdb.dbo.todotask base on base.myTrackedItem = ti.oid and base.status = 0 and base.toDoTaskDescShort = 'BASE WASH (pol)'
	LEFT JOIN Statsdb.dbo.todotask roof on roof.myTrackedItem = ti.oid and roof.status = 0 and roof.toDoTaskDescShort = 'Roof Wash'
	LEFT JOIN Statsdb.dbo.todotask extc on extc.myTrackedItem = ti.oid and extc.status = 0 and extc.toDoTaskDescShort = 'EXTCLEAN'
	LEFT JOIN Statsdb.dbo.todotask phot on phot.myTrackedItem = ti.oid and phot.status = 0 and phot.toDoTaskDescShort = 'Photo'
	LEFT JOIN Statsdb.dbo.todotask repa on repa.myTrackedItem = ti.oid and repa.status = 0 and repa.toDoTaskDescShort = 'Repair'
	LEFT JOIN Statsdb.dbo.todotask stmc on stmc.myTrackedItem = ti.oid and stmc.status = 0 and stmc.toDoTaskDescShort = 'STEAM.C'
	LEFT JOIN Statsdb.dbo.todotask scfq on scfq.myTrackedItem = ti.oid and scfq.status = 0 and scfq.toDoTaskDescShort = 'STEAM.C FQ'
	LEFT JOIN Statsdb.dbo.todotask reqi on reqi.myTrackedItem = ti.oid and reqi.status = 0 and reqi.toDoTaskDescShort = 'REQ INSP'
	LEFT JOIN Statsdb.dbo.todotask dbu3 on dbu3.myTrackedItem = ti.oid and dbu3.status = 0 and dbu3.toDoTaskDescShort = 'DB Upgrade 3'
	LEFT JOIN Statsdb.dbo.todotask dbca on dbca.myTrackedItem = ti.oid and dbca.status = 0 and dbca.toDoTaskDescShort = 'DB Cert Admin'
	WHERE tila.InEffectiveDT is null
	and ti.releasedDT is null
	and ti.myTerminal = 0x092F00000001 -- POE
	and tila.Location like 'W_'
	ORDER BY tila.Location , tila.EffectiveDT , ti.id

GO -- exec [dbo].[spGetContainersOnWash] 

GRANT EXECUTE ON [dbo].[spGetContainersOnWash] to WashpadUser