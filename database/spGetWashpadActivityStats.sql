USE [WashDB]
GO
/****** Object:  StoredProcedure [dbo].[spGetWashpadActivityStats]    Script Date: 05/03/2017 11:55:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spGetWashpadActivityStats]
	@day datetime
	
AS
	SET NOCOUNT ON
	
BEGIN
	DECLARE @BoxesInspectedWXDay1st int = 0
	DECLARE @BoxesInspectedWXDay2nd int = 0
	DECLARE @BoxesInspectedWXDay24hrs int = 0
	
	DECLARE @BoxesRepairedWXDay1st int = 0
	DECLARE @BoxesRepairedWXDay2nd int = 0
	DECLARE @BoxesRepairedWXDay24hrs int = 0
	
	DECLARE @IndividualRepairsWXDay1st int = 0
	DECLARE @IndividualRepairsWXDay2nd int = 0
	DECLARE @IndividualRepairsWXDay24hrs int = 0

	DECLARE @BoxesWashedWXDay1st int = 0
	DECLARE @BoxesWashedWXDay2nd int = 0
	DECLARE @BoxesWashedWXDay24hrs int = 0
	
	DECLARE @IndividualWashesWXDay1st int = 0
	DECLARE @IndividualWashesWXDay2nd int = 0
	DECLARE @IndividualWashesWXDay24hrs int = 0

	DECLARE @BoxesNeedRepairWXDay1st int = 0
	DECLARE @BoxesNeedRepairWXDay2nd int = 0
	DECLARE @BoxesNeedRepairWXDay24hrs int = 0
	
	DECLARE @BoxesNeedRepairW1Day1st int = 0
	DECLARE @BoxesNeedRepairW1Day2nd int = 0
	DECLARE @BoxesNeedRepairW1Day24hrs int = 0
	
	DECLARE @BoxesNeedRepairW2Day1st int = 0
	DECLARE @BoxesNeedRepairW2Day2nd int = 0
	DECLARE @BoxesNeedRepairW2Day24hrs int = 0
	
	DECLARE @BoxesNeedRepairW3Day1st int = 0
	DECLARE @BoxesNeedRepairW3Day2nd int = 0
	DECLARE @BoxesNeedRepairW3Day24hrs int = 0
	
	DECLARE @BoxesNeedRepairW4Day1st int = 0
	DECLARE @BoxesNeedRepairW4Day2nd int = 0
	DECLARE @BoxesNeedRepairW4Day24hrs int = 0
	
	DECLARE @BoxesInspectedWXDayPrev1st int = 0
	DECLARE @BoxesInspectedWXDayPrev2nd int = 0
	DECLARE @BoxesInspectedWXDayPrev24hrs int = 0
	
	DECLARE @BoxesRepairedWXDayPrev1st int = 0
	DECLARE @BoxesRepairedWXDayPrev2nd int = 0
	DECLARE @BoxesRepairedWXDayPrev24hrs int = 0
	
	DECLARE @IndividualRepairsWXDayPrev1st int = 0
	DECLARE @IndividualRepairsWXDayPrev2nd int = 0
	DECLARE @IndividualRepairsWXDayPrev24hrs int = 0

	DECLARE @BoxesWashedWXDayPrev1st int = 0
	DECLARE @BoxesWashedWXDayPrev2nd int = 0
	DECLARE @BoxesWashedWXDayPrev24hrs int = 0
	
	DECLARE @IndividualWashesWXDayPrev1st int = 0
	DECLARE @IndividualWashesWXDayPrev2nd int = 0
	DECLARE @IndividualWashesWXDayPrev24hrs int = 0

	DECLARE @BoxesNeedRepairWXDayPrev1st int = 0
	DECLARE @BoxesNeedRepairWXDayPrev2nd int = 0
	DECLARE @BoxesNeedRepairWXDayPrev24hrs int = 0
	
	DECLARE @BoxesNeedRepairW1DayPrev1st int = 0
	DECLARE @BoxesNeedRepairW1DayPrev2nd int = 0
	DECLARE @BoxesNeedRepairW1DayPrev24hrs int = 0
	
	DECLARE @BoxesNeedRepairW2DayPrev1st int = 0
	DECLARE @BoxesNeedRepairW2DayPrev2nd int = 0
	DECLARE @BoxesNeedRepairW2DayPrev24hrs int = 0
	
	DECLARE @BoxesNeedRepairW3DayPrev1st int = 0
	DECLARE @BoxesNeedRepairW3DayPrev2nd int = 0
	DECLARE @BoxesNeedRepairW3DayPrev24hrs int = 0
	
	DECLARE @BoxesNeedRepairW4DayPrev1st int = 0
	DECLARE @BoxesNeedRepairW4DayPrev2nd int = 0
	DECLARE @BoxesNeedRepairW4DayPrev24hrs int = 0

	DECLARE @day1stShiftStart datetime = @day
	DECLARE @day2ndShiftStart datetime = DATEADD("HH",15,@day)
	DECLARE @day2ndShiftEnd datetime = DATEADD("HH",24,@day)
	
	DECLARE @dayPrev1stShiftStart datetime = DATEADD("d",-1,@day)
	DECLARE @dayPrev2ndShiftStart datetime = DATEADD("HH",15,DATEADD("d",-1,@day))
	DECLARE @dayPrev2ndShiftEnd datetime = @day

	/****** Boxes Inspected - ALL PADS ******/
	SELECT @BoxesInspectedWXDay1st = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @day1stShiftStart AND completedInspection < @day2ndShiftStart

	SELECT @BoxesInspectedWXDay2nd = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @day2ndShiftStart AND completedInspection < @day2ndShiftEnd

	SELECT @BoxesInspectedWXDay24hrs = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @day1stShiftStart AND completedInspection < @day2ndShiftEnd
	
	
	/****** Boxes Repaired - ALL PADS ******/
	SELECT  @BoxesRepairedWXDay1st = COUNT(DISTINCT containerID)
	FROM [WashDB].[dbo].[containerRepairs]
	WHERE completedRepair >= @day1stShiftStart AND completedRepair < @day2ndShiftStart 
	
	SELECT  @BoxesRepairedWXDay2nd = COUNT(DISTINCT containerID)
	FROM [WashDB].[dbo].[containerRepairs]
	WHERE completedRepair >= @day2ndShiftStart AND completedRepair < @day2ndShiftEnd
	
	SELECT  @BoxesRepairedWXDay24hrs = COUNT(DISTINCT containerID)
	FROM [WashDB].[dbo].[containerRepairs]
	WHERE completedRepair >= @day1stShiftStart AND completedRepair < @day2ndShiftEnd


	/****** Individual Repairs - ALL PADS ******/
	SELECT  @IndividualRepairsWXDay1st = COUNT(*)
	FROM [WashDB].[dbo].[containerRepairs]
	WHERE completedRepair >= @day1stShiftStart AND completedRepair < @day2ndShiftStart 
	
	SELECT  @IndividualRepairsWXDay2nd = COUNT(*)
	FROM [WashDB].[dbo].[containerRepairs]
	WHERE completedRepair >= @day2ndShiftStart AND completedRepair < @day2ndShiftEnd
	
	SELECT  @IndividualRepairsWXDay24hrs = COUNT(*)
	FROM [WashDB].[dbo].[containerRepairs]
	WHERE completedRepair >= @day1stShiftStart AND completedRepair < @day2ndShiftEnd
	
	
	/****** Boxes Washed - ALL PADS ******/
	SELECT  @BoxesWashedWXDay1st = COUNT(DISTINCT containerID)
	FROM [WashDB].[dbo].[containerStates]
	WHERE	(completedWash >= @day1stShiftStart AND completedWash < @day2ndShiftStart) 
	OR		(completedBaseWash >= @day1stShiftStart AND completedBaseWash < @day2ndShiftStart)
	OR		(completedRoofWash >= @day1stShiftStart AND completedRoofWash < @day2ndShiftStart)
	OR		(completedExternalWash >= @day1stShiftStart AND completedExternalWash < @day2ndShiftStart)
	OR		(completedSteamClean >= @day1stShiftStart AND completedSteamClean < @day2ndShiftStart)
	OR		(completedSteamCleanFQ >= @day1stShiftStart AND completedSteamCleanFQ < @day2ndShiftStart)

	SELECT  @BoxesWashedWXDay2nd = COUNT(DISTINCT containerID)
	FROM [WashDB].[dbo].[containerStates]
	WHERE (completedWash >= @day2ndShiftStart AND completedWash < @day2ndShiftEnd)
	OR		(completedBaseWash >= @day2ndShiftStart AND completedBaseWash < @day2ndShiftEnd)
	OR		(completedRoofWash >= @day2ndShiftStart AND completedRoofWash < @day2ndShiftEnd)
	OR		(completedExternalWash >= @day2ndShiftStart AND completedExternalWash < @day2ndShiftEnd)
	OR		(completedSteamClean >= @day2ndShiftStart AND completedSteamClean < @day2ndShiftEnd)
	OR		(completedSteamCleanFQ >= @day2ndShiftStart AND completedSteamCleanFQ < @day2ndShiftEnd)
	
	SELECT  @BoxesWashedWXDay24hrs = COUNT(DISTINCT containerID)
	FROM [WashDB].[dbo].[containerStates]
	WHERE (completedWash >= @day1stShiftStart AND completedWash < @day2ndShiftEnd)
	OR		(completedBaseWash >= @day1stShiftStart AND completedBaseWash < @day2ndShiftEnd)
	OR		(completedRoofWash >= @day1stShiftStart AND completedRoofWash < @day2ndShiftEnd)
	OR		(completedExternalWash >= @day1stShiftStart AND completedExternalWash < @day2ndShiftEnd)
	OR		(completedSteamClean >= @day1stShiftStart AND completedSteamClean < @day2ndShiftEnd)
	OR		(completedSteamCleanFQ >= @day1stShiftStart AND completedSteamCleanFQ < @day2ndShiftEnd)

	
	/****** Individual Washes - ALL PADS ******/
	SELECT  @IndividualWashesWXDay1st = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedWash >= @day1stShiftStart AND completedWash < @day2ndShiftStart 
		
	SELECT  @IndividualWashesWXDay2nd = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedWash >= @day2ndShiftStart AND completedWash < @day2ndShiftEnd
	
	SELECT  @IndividualWashesWXDay24hrs = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedWash >= @day1stShiftStart AND completedWash < @day2ndShiftEnd
	
	
	/****** Boxes Need Repair - ALL PADS ******/
	SELECT @BoxesNeedRepairWXDay1st = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @day1stShiftStart AND completedInspection < @day2ndShiftStart AND taskRepair > 0

	SELECT @BoxesNeedRepairWXDay2nd = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @day2ndShiftStart AND completedInspection < @day2ndShiftEnd AND taskRepair > 0
	
	SELECT @BoxesNeedRepairWXDay24hrs = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @day1stShiftStart AND completedInspection < @day2ndShiftEnd AND taskRepair > 0
	
	
	/****** Boxes Need Repair - W1 ******/
	SELECT @BoxesNeedRepairW1Day1st = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @day1stShiftStart AND completedInspection < @day2ndShiftStart AND location = 'W1' AND taskRepair > 0

	SELECT @BoxesNeedRepairW1Day2nd = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @day2ndShiftStart AND completedInspection < @day2ndShiftEnd AND location = 'W1' AND taskRepair > 0
	
	SELECT @BoxesNeedRepairW1Day24hrs = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @day1stShiftStart AND completedInspection < @day2ndShiftEnd AND location = 'W1' AND taskRepair > 0
	
	
	/****** Boxes Need Repair - W2 ******/
	SELECT @BoxesNeedRepairW2Day1st = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @day1stShiftStart AND completedInspection < @day2ndShiftStart AND location = 'W2' AND taskRepair > 0
	
	SELECT @BoxesNeedRepairW2Day2nd = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @day2ndShiftStart AND completedInspection < @day2ndShiftEnd AND location = 'W2' AND taskRepair > 0
	
	SELECT @BoxesNeedRepairW2Day24hrs = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @day1stShiftStart AND completedInspection < @day2ndShiftEnd AND location = 'W2' AND taskRepair > 0


	/****** Boxes Need Repair - W3 ******/
	SELECT @BoxesNeedRepairW3Day1st = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @day1stShiftStart AND completedInspection < @day2ndShiftStart AND location = 'W3' AND taskRepair > 0

	SELECT @BoxesNeedRepairW3Day2nd = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @day2ndShiftStart AND completedInspection < @day2ndShiftEnd AND location = 'W3' AND taskRepair > 0
	
	SELECT @BoxesNeedRepairW3Day24hrs = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @day1stShiftStart AND completedInspection < @day2ndShiftEnd AND location = 'W3' AND taskRepair > 0


	/****** Boxes Need Repair - W4 ******/
	SELECT @BoxesNeedRepairW4Day1st = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @day1stShiftStart AND completedInspection < @day2ndShiftStart AND location = 'W4' AND taskRepair > 0

	SELECT @BoxesNeedRepairW4Day2nd = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @day2ndShiftStart AND completedInspection < @day2ndShiftEnd AND location = 'W4' AND taskRepair > 0

	SELECT @BoxesNeedRepairW4Day24hrs = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @day1stShiftStart AND completedInspection < @day2ndShiftEnd AND location = 'W4' AND taskRepair > 0



	/****** Boxes Inspected - ALL PADS ******/
	SELECT @BoxesInspectedWXDayPrev1st = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @dayPrev1stShiftStart AND completedInspection < @dayPrev2ndShiftStart

	SELECT @BoxesInspectedWXDayPrev2nd = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @dayPrev2ndShiftStart AND completedInspection < @dayPrev2ndShiftEnd

	SELECT @BoxesInspectedWXDayPrev24hrs = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @dayPrev1stShiftStart AND completedInspection < @dayPrev2ndShiftEnd
	
	
	/****** Boxes Repaired - ALL PADS ******/
	SELECT  @BoxesRepairedWXDayPrev1st = COUNT(DISTINCT containerID)
	FROM [WashDB].[dbo].[containerRepairs]
	WHERE completedRepair >= @dayPrev1stShiftStart AND completedRepair < @dayPrev2ndShiftStart 
	
	SELECT  @BoxesRepairedWXDayPrev2nd = COUNT(DISTINCT containerID)
	FROM [WashDB].[dbo].[containerRepairs]
	WHERE completedRepair >= @dayPrev2ndShiftStart AND completedRepair < @dayPrev2ndShiftEnd
	
	SELECT  @BoxesRepairedWXDayPrev24hrs = COUNT(DISTINCT containerID)
	FROM [WashDB].[dbo].[containerRepairs]
	WHERE completedRepair >= @dayPrev1stShiftStart AND completedRepair < @dayPrev2ndShiftEnd


	/****** Individual Repairs - ALL PADS ******/
	SELECT  @IndividualRepairsWXDayPrev1st = COUNT(*)
	FROM [WashDB].[dbo].[containerRepairs]
	WHERE completedRepair >= @dayPrev1stShiftStart AND completedRepair < @dayPrev2ndShiftStart 
	
	SELECT  @IndividualRepairsWXDayPrev2nd = COUNT(*)
	FROM [WashDB].[dbo].[containerRepairs]
	WHERE completedRepair >= @dayPrev2ndShiftStart AND completedRepair < @dayPrev2ndShiftEnd
	
	SELECT  @IndividualRepairsWXDayPrev24hrs = COUNT(*)
	FROM [WashDB].[dbo].[containerRepairs]
	WHERE completedRepair >= @dayPrev1stShiftStart AND completedRepair < @dayPrev2ndShiftEnd
	
	
	/****** Boxes Washed - ALL PADS ******/
	SELECT  @BoxesWashedWXDayPrev1st = COUNT(DISTINCT containerID)
	FROM [WashDB].[dbo].[containerStates]
	WHERE (completedWash >= @dayPrev1stShiftStart AND completedWash < @dayPrev2ndShiftStart)
	OR		(completedBaseWash >= @dayPrev1stShiftStart AND completedBaseWash < @dayPrev2ndShiftStart)
	OR		(completedRoofWash >= @dayPrev1stShiftStart AND completedRoofWash < @dayPrev2ndShiftStart)
	OR		(completedExternalWash >= @dayPrev1stShiftStart AND completedExternalWash < @dayPrev2ndShiftStart)
	OR		(completedSteamClean >= @dayPrev1stShiftStart AND completedSteamClean < @dayPrev2ndShiftStart)
	OR		(completedSteamCleanFQ >= @dayPrev1stShiftStart AND completedSteamCleanFQ < @dayPrev2ndShiftStart)

	SELECT  @BoxesWashedWXDayPrev2nd = COUNT(DISTINCT containerID)
	FROM [WashDB].[dbo].[containerStates]
	WHERE (completedWash >= @dayPrev2ndShiftStart AND completedWash < @dayPrev2ndShiftEnd)
	OR		(completedBaseWash >= @dayPrev2ndShiftStart AND completedBaseWash < @dayPrev2ndShiftEnd)
	OR		(completedRoofWash >= @dayPrev2ndShiftStart AND completedRoofWash < @dayPrev2ndShiftEnd)
	OR		(completedExternalWash >= @dayPrev2ndShiftStart AND completedExternalWash < @dayPrev2ndShiftEnd)
	OR		(completedSteamClean >= @dayPrev2ndShiftStart AND completedSteamClean < @dayPrev2ndShiftEnd)
	OR		(completedSteamCleanFQ >= @dayPrev2ndShiftStart AND completedSteamCleanFQ < @dayPrev2ndShiftEnd)
	
	SELECT  @BoxesWashedWXDayPrev24hrs = COUNT(DISTINCT containerID)
	FROM [WashDB].[dbo].[containerStates]
	WHERE (completedWash >= @dayPrev1stShiftStart AND completedWash < @dayPrev2ndShiftEnd)
	OR		(completedBaseWash >= @dayPrev1stShiftStart AND completedBaseWash < @dayPrev2ndShiftEnd)
	OR		(completedRoofWash >= @dayPrev1stShiftStart AND completedRoofWash < @dayPrev2ndShiftEnd)
	OR		(completedExternalWash >= @dayPrev1stShiftStart AND completedExternalWash < @dayPrev2ndShiftEnd)
	OR		(completedSteamClean >= @dayPrev1stShiftStart AND completedSteamClean < @dayPrev2ndShiftEnd)
	OR		(completedSteamCleanFQ >= @dayPrev1stShiftStart AND completedSteamCleanFQ < @dayPrev2ndShiftEnd)

	
	/****** Individual Washes - ALL PADS ******/
	SELECT  @IndividualWashesWXDayPrev1st = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedWash >= @dayPrev1stShiftStart AND completedWash < @dayPrev2ndShiftStart 
		
	SELECT  @IndividualWashesWXDayPrev2nd = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedWash >= @dayPrev2ndShiftStart AND completedWash < @dayPrev2ndShiftEnd
	
	SELECT  @IndividualWashesWXDayPrev24hrs = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedWash >= @dayPrev1stShiftStart AND completedWash < @dayPrev2ndShiftEnd
	
	
	/****** Boxes Need Repair - ALL PADS ******/
	SELECT @BoxesNeedRepairWXDayPrev1st = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @dayPrev1stShiftStart AND completedInspection < @dayPrev2ndShiftStart AND taskRepair > 0

	SELECT @BoxesNeedRepairWXDayPrev2nd = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @dayPrev2ndShiftStart AND completedInspection < @dayPrev2ndShiftEnd AND taskRepair > 0
	
	SELECT @BoxesNeedRepairWXDayPrev24hrs = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @dayPrev1stShiftStart AND completedInspection < @dayPrev2ndShiftEnd AND taskRepair > 0
	
	
	/****** Boxes Need Repair - W1 ******/
	SELECT @BoxesNeedRepairW1DayPrev1st = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @dayPrev1stShiftStart AND completedInspection < @dayPrev2ndShiftStart AND location = 'W1' AND taskRepair > 0

	SELECT @BoxesNeedRepairW1DayPrev2nd = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @dayPrev2ndShiftStart AND completedInspection < @dayPrev2ndShiftEnd AND location = 'W1' AND taskRepair > 0
	
	SELECT @BoxesNeedRepairW1DayPrev24hrs = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @dayPrev1stShiftStart AND completedInspection < @dayPrev2ndShiftEnd AND location = 'W1' AND taskRepair > 0
	
	
	/****** Boxes Need Repair - W2 ******/
	SELECT @BoxesNeedRepairW2DayPrev1st = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @dayPrev1stShiftStart AND completedInspection < @dayPrev2ndShiftStart AND location = 'W2' AND taskRepair > 0
	
	SELECT @BoxesNeedRepairW2DayPrev2nd = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @dayPrev2ndShiftStart AND completedInspection < @dayPrev2ndShiftEnd AND location = 'W2' AND taskRepair > 0
	
	SELECT @BoxesNeedRepairW2DayPrev24hrs = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @dayPrev1stShiftStart AND completedInspection < @dayPrev2ndShiftEnd AND location = 'W2' AND taskRepair > 0


	/****** Boxes Need Repair - W3 ******/
	SELECT @BoxesNeedRepairW3DayPrev1st = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @dayPrev1stShiftStart AND completedInspection < @dayPrev2ndShiftStart AND location = 'W3' AND taskRepair > 0

	SELECT @BoxesNeedRepairW3DayPrev2nd = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @dayPrev2ndShiftStart AND completedInspection < @dayPrev2ndShiftEnd AND location = 'W3' AND taskRepair > 0
	
	SELECT @BoxesNeedRepairW3DayPrev24hrs = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @dayPrev1stShiftStart AND completedInspection < @dayPrev2ndShiftEnd AND location = 'W3' AND taskRepair > 0


	/****** Boxes Need Repair - W4 ******/
	SELECT @BoxesNeedRepairW4DayPrev1st = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @dayPrev1stShiftStart AND completedInspection < @dayPrev2ndShiftStart AND location = 'W4' AND taskRepair > 0

	SELECT @BoxesNeedRepairW4DayPrev2nd = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @dayPrev2ndShiftStart AND completedInspection < @dayPrev2ndShiftEnd AND location = 'W4' AND taskRepair > 0

	SELECT @BoxesNeedRepairW4DayPrev24hrs = COUNT(*)
	FROM [WashDB].[dbo].[containerStates]
	WHERE completedInspection >= @dayPrev1stShiftStart AND completedInspection < @dayPrev2ndShiftEnd AND location = 'W4' AND taskRepair > 0

/*********** OUTPUT *********************/
SELECT
	@BoxesInspectedWXDay1st AS BoxesInspectedWXDay1st,
	@BoxesInspectedWXDay2nd AS BoxesInspectedWXDay2nd,
	@BoxesInspectedWXDay24hrs AS BoxesInspectedWXDay24hrs,
	@BoxesRepairedWXDay1st AS BoxesRepairedWXDay1st,
	@BoxesRepairedWXDay2nd AS BoxesRepairedWXDay2nd,
	@BoxesRepairedWXDay24hrs AS BoxesRepairedWXDay24hrs,
	@IndividualRepairsWXDay1st AS IndividualRepairsWXDay1st,
	@IndividualRepairsWXDay2nd AS IndividualRepairsWXDay2nd,
	@IndividualRepairsWXDay24hrs AS IndividualRepairsWXDay24hrs,
	@BoxesWashedWXDay1st AS BoxesWashedWXDay1st,
	@BoxesWashedWXDay2nd AS BoxesWashedWXDay2nd,
	@BoxesWashedWXDay24hrs AS BoxesWashedWXDay24hrs,
	@IndividualWashesWXDay1st AS IndividualWashesWXDay1st,
	@IndividualWashesWXDay2nd AS IndividualWashesWXDay2nd,
	@IndividualWashesWXDay24hrs AS IndividualWashesWXDay24hrs,
	@BoxesNeedRepairWXDay1st AS BoxesNeedRepairWXDay1st,
	@BoxesNeedRepairWXDay2nd AS BoxesNeedRepairWXDay2nd,
	@BoxesNeedRepairWXDay24hrs AS BoxesNeedRepairWXDay24hrs,
	@BoxesNeedRepairW1Day1st AS BoxesNeedRepairW1Day1st,
	@BoxesNeedRepairW1Day2nd AS BoxesNeedRepairW1Day2nd,
	@BoxesNeedRepairW1Day24hrs AS BoxesNeedRepairW1Day24hrs,
	@BoxesNeedRepairW2Day1st AS BoxesNeedRepairW2Day1st,
	@BoxesNeedRepairW2Day2nd AS BoxesNeedRepairW2Day2nd,
	@BoxesNeedRepairW2Day24hrs AS BoxesNeedRepairW2Day24hrs,
	@BoxesNeedRepairW3Day1st AS BoxesNeedRepairW3Day1st,
	@BoxesNeedRepairW3Day2nd AS BoxesNeedRepairW3Day2nd,
	@BoxesNeedRepairW3Day24hrs AS BoxesNeedRepairW3Day24hrs,
	@BoxesNeedRepairW4Day1st AS BoxesNeedRepairW4Day1st,
	@BoxesNeedRepairW4Day2nd AS BoxesNeedRepairW4Day2nd,
	@BoxesNeedRepairW4Day24hrs AS BoxesNeedRepairW4Day24hrs,
	@BoxesInspectedWXDayPrev1st AS BoxesInspectedWXDayPrev1st,
	@BoxesInspectedWXDayPrev2nd AS BoxesInspectedWXDayPrev2nd,
	@BoxesInspectedWXDayPrev24hrs AS BoxesInspectedWXDayPrev24hrs,
	@BoxesRepairedWXDayPrev1st AS BoxesRepairedWXDayPrev1st,
	@BoxesRepairedWXDayPrev2nd AS BoxesRepairedWXDayPrev2nd,
	@BoxesRepairedWXDayPrev24hrs AS BoxesRepairedWXDayPrev24hrs,
	@IndividualRepairsWXDayPrev1st AS IndividualRepairsWXDayPrev1st,
	@IndividualRepairsWXDayPrev2nd AS IndividualRepairsWXDayPrev2nd,
	@IndividualRepairsWXDayPrev24hrs AS IndividualRepairsWXDayPrev24hrs,
	@BoxesWashedWXDayPrev1st AS BoxesWashedWXDayPrev1st,
	@BoxesWashedWXDayPrev2nd AS BoxesWashedWXDayPrev2nd,
	@BoxesWashedWXDayPrev24hrs AS BoxesWashedWXDayPrev24hrs,
	@IndividualWashesWXDayPrev1st AS IndividualWashesWXDayPrev1st,
	@IndividualWashesWXDayPrev2nd AS IndividualWashesWXDayPrev2nd,
	@IndividualWashesWXDayPrev24hrs AS IndividualWashesWXDayPrev24hrs,
	@BoxesNeedRepairWXDayPrev1st AS BoxesNeedRepairWXDayPrev1st,
	@BoxesNeedRepairWXDayPrev2nd AS BoxesNeedRepairWXDayPrev2nd,
	@BoxesNeedRepairWXDayPrev24hrs AS BoxesNeedRepairWXDayPrev24hrs,
	@BoxesNeedRepairW1DayPrev1st AS BoxesNeedRepairW1DayPrev1st,
	@BoxesNeedRepairW1DayPrev2nd AS BoxesNeedRepairW1DayPrev2nd,
	@BoxesNeedRepairW1DayPrev24hrs AS BoxesNeedRepairW1DayPrev24hrs,
	@BoxesNeedRepairW2DayPrev1st AS BoxesNeedRepairW2DayPrev1st,
	@BoxesNeedRepairW2DayPrev2nd AS BoxesNeedRepairW2DayPrev2nd,
	@BoxesNeedRepairW2DayPrev24hrs AS BoxesNeedRepairW2DayPrev24hrs,
	@BoxesNeedRepairW3DayPrev1st AS BoxesNeedRepairW3DayPrev1st,
	@BoxesNeedRepairW3DayPrev2nd AS BoxesNeedRepairW3DayPrev2nd,
	@BoxesNeedRepairW3DayPrev24hrs AS BoxesNeedRepairW3DayPrev24hrs,
	@BoxesNeedRepairW4DayPrev1st AS BoxesNeedRepairW4DayPrev1st,
	@BoxesNeedRepairW4DayPrev2nd AS BoxesNeedRepairW4DayPrev2nd,
	@BoxesNeedRepairW4DayPrev24hrs AS BoxesNeedRepairW4DayPrev24hrs

END

RETURN