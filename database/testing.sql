USE [DepotPro_POLTablet_TEST]
GO

BEGIN TRANSACTION
DECLARE @ContainerID varchar(max)
DECLARE	@return_value int

SET @ContainerID = ',CAXU6927597,CGMU3041691,CGMU3041834,'

SELECT 'before', *
FROM dbo.containerStates
WHERE ','+@ContainerID+',' like '%,'+ containerID +',%'


EXEC @return_value = [dbo].[spRemoveContainersFromWash] @ContainerIDs = @ContainerID

SELECT	'Return Value' = @return_value

SELECT 'after', *
FROM dbo.containerStates
WHERE ','+@ContainerID+',' like '%,'+ containerID +',%'


GO

ROLLBACK TRANSACTION